@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('add_invoice') !!}
@endsection

@section('content')
 <div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{$headTitle}}</span>
                </div>
            </div>
            <div class="portlet-body">
                <form id="frminvoice" class="form-horizontal" role="form" method="POST" action="{{ route('invoice.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                        <label for="user_id" class="col-md-2 control-label">Select Counsumer{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <select name="user_id" id="user_id" class="form-control">
                                <option value="">Select a Consumer</option>
                                @foreach ($users as $user)
                                    <option value="{{$user->id}}" {{ (old('user_id') == $user->id) ? 'selected' : '' }}>{{$user->first_name .' '.$user->last_name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('user_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('user_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('property_id') ? ' has-error' : '' }}">
                        <label for="property_id" class="col-md-2 control-label">Select Property{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <select name="property_id" id="property_id" class="form-control" >
                                <option value=''>Select Property</option>
                            </select>
                            @if ($errors->has('property_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('property_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('reference') ? ' has-error' : '' }}">
                        <label for="reference" class="col-md-2 control-label">Reference{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-envelope"></i>
                                <input type="text" placeholder="Enter Reference" name="reference" id="reference" class="form-control" value="{{old('reference','')}}" maxlength="150" />
                                @if ($errors->has('reference'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reference') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                        <label for="value" class="col-md-2 control-label">Value{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-gbp"></i>
                                <input type="text" placeholder="Enter Value" name="value" id="value" class="form-control" value="{{old('value','')}}" maxlength="150" />
                                @if ($errors->has('value'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                        <label for="duedate" class="col-md-2 control-label">Due Date{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-calendar"></i>
                                <input class="form-control form-control-inline date-picker" type="text" name="duedate" id="duedate" placeholder="Select Due Date" readonly />
                                @if ($errors->has('duedate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('duedate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('payment_type') ? ' has-error' : '' }}">
                        <label for="payment_type" class="col-md-2 control-label">Select Payment Type{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <select name="payment_type" id="payment_type" class="form-control" >
                                <option value=''>Select Payment</option>
                                <option value='online'>Online</option>
                                <option value='offline'>Offline</option>
                            </select>
                            @if ($errors->has('payment_type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('payment_type') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn green">Submit</button>
                            <a href="{{route('invoice.index')}}" class="btn red">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: SAMPLE FORM PORTLET -->
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {

    var dateToday = new Date();

    if (jQuery().datepicker) {
        $('.date-picker').datepicker({
            rtl: false,
            orientation: "right bottom",
            autoclose: true,
            startDate: dateToday
        });
    }

    $("#frminvoice").validate({
        rules: {
            user_id:{required:true},
            property_id:{required:true},
            reference:{required:true},
            value:{required:true,number:true},
            duedate:{required:true},
            payment_type:{required:true}
        },
        messages: {
            user_id:{
                required:"@lang('validation.required',['attribute'=>'user'])"
            },
            property_id:{
                required:"@lang('validation.required',['attribute'=>'property'])"
            },
            reference:{
                required:"@lang('validation.required',['attribute'=>'reference'])"
            },
            value:{
                required:"@lang('validation.required',['attribute'=>'value'])",
                number:"@lang('validation.numeric',['attribute'=>'value'])"
            },
            duedate:{
                required:"@lang('validation.required',['attribute'=>'duedate'])"
            },
            payment_type:{
                required:"@lang('validation.required',['attribute'=>'payment type'])"
            }
        },
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function (element) {
           $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
           $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                  error.appendTo('.a');
            }else{
                if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            }
        }
    });
    $(document).on('submit','#frminvoice',function(){
        if($("#frminvoice").valid()){
            return true;
        }else{
            return false;
        }
    });

    $(document).on('change', '#user_id', function(event) {
        event.preventDefault();
        var user_id = $(this).val();
        if (user_id) {
            $.ajax({
                url: '{{ route('getuserdata') }}',
                type: 'post',
                dataType: 'html',
                data: {"_token": "{{ csrf_token() }}",id: user_id},
            })
            .done(function(data) {
                data = JSON.parse(data);
                $('#property_id').html(data.data).attr('disabled',false);
            })
            .fail(function() {
                $('#property_id').html('<option>Select User</option>');
            });
        }else {
            $('#property_id').html("").attr('disabled',true);
        }
    });
});

</script>

@endpush
