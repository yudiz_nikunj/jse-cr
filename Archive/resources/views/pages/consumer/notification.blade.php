<div class="modal fade" id="ajaxModal" role="dialog">
    <div class="modal-dialog">
  
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Send Notification</h4>
            </div>
            <div class="modal-body">
                <form method="post" role="form" action="{{ route('notificationForm') }}" id="frmnotification">
                    {{ csrf_field() }}
                    <div id="userid"></div> {{-- hidden field --}}
                    <div class="form-group">
                        <label for="title">Title{!! $mend_sign !!}</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                    </div>
                
                    <div class="form-group">
                        <label for="title">Description{!! $mend_sign !!}</label>
                        <textarea class="form-control" id="description" name="description" placeholder="Description" style="resize: vertical;"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Send</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="float: right;">Close</button>
                </form>
            </div>
        </div>
    </div>
</div>

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function() {

            $("#frmnotification").validate({
                rules: {
                    title:{required:true},
                    description:{required:true}
                },
                messages: {
                    title:{
                        required:"@lang('validation.required',['attribute'=>'title'])"
                    },
                    description:{
                        required:"@lang('validation.required',['attribute'=>'description'])"
                    }
                },
                errorClass: 'help-block',
                errorElement: 'span',
                highlight: function (element) {
                   $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                   $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "radio") {
                          error.appendTo('.a');
                    }else{
                        if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                }
            });
            $(document).on('submit','#frmnotification',function(){
                if($("#frmnotification").valid()){
                    return true;
                }else{
                    return false;
                }
            });
        });
    </script>

@endpush