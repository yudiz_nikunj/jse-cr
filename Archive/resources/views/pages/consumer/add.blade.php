@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('add_consumer') !!}
@endsection

@section('content')
 <div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{$headTitle}}</span>
                </div>
            </div>
            <div class="portlet-body">
                <form id="frmconsumer" class="form-horizontal" role="form" method="POST" action="{{ route('consumer.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group form-md-line-input" style="display: none;">
                        <label class="col-md-2 control-label" for="form_control_1" style="color: #333;">Consumer Type{!! $mend_sign !!}</label>
                        <div class="col-md-10">
                            <div class="md-radio-inline">
                                
                                <div class="md-radio">
                                    <input type="radio" id="radio53_3" data-error-container="#type_error" value="management" name="consumer_type" class="md-radiobtn" checked="checked" {{(old('consumer_type','management')) }}>
                                    <label for="radio53_3">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Management
                                    </label>
                                </div>

                                <div class="md-radio">
                                    <input type="radio" id="radio53_4" data-error-container="#type_error" value="sourcing" name="consumer_type" class="md-radiobtn" {{(old('consumer_type','sourcing')) }} >
                                    <label for="radio53_4">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Sourcing
                                    </label>
                                </div>
                                
                                <span id="type_error"></span>
                            </div>
                        </div>
                    </div>


                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <label for="first_name" class="col-md-2 control-label">First Name{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-user"></i>
                                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name" maxlength="50" value="{{ old('first_name','') }}">
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <label for="last_name" class="col-md-2 control-label">Last Name{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-user"></i>
                                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name" maxlength="50" value="{{ old('last_name','') }}">
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-2 control-label">Email{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-envelope"></i>
                                <input type="email" placeholder="Enter E-mail Address" name="email" id="email" class="form-control" value="{{old('email','')}}" maxlength="150" />
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-2 control-label">Password{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-lock"></i>
                                <input type="password" placeholder="Enter Password" name="password" id="password" class="form-control" value="{{old('password','')}}" maxlength="30" />
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                        <label for="phone_number" class="col-md-2 control-label">Contact Number{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-phone"></i>
                                <input type="text" placeholder="Enter Contact No" name="phone_number" id="phone_number" class="form-control" value="{{old('phone_number','')}}" maxlength="15" />
                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone_number" class="col-md-2 control-label"></label>
                        <div class="col-md-4"> 
                        <span><b>Note:</b> Country code(44) is mandatory in contact number.</span>
                        </div>
                    </div>

                    <div class="cunsumer-panel form-group{{ $errors->has('monthly') ? ' has-error' : '' }}">
                        <label for="monthly" class="col-md-2 control-label">Monthly Package{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-gbp"></i>
                                <input type="text" placeholder="Month Package Price" name="monthly" id="monthly" class="form-control" value="{{old('monthly','')}}" maxlength="15" />
                                @if ($errors->has('monthly'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('monthly') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="cunsumer-panel form-group{{ $errors->has('yearly') ? ' has-error' : '' }}">
                        <label for="yearly" class="col-md-2 control-label">Yearly Package{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-gbp"></i>
                                <input type="text" placeholder="Year Package Price" name="yearly" id="yearly" class="form-control" value="{{old('yearly','')}}" maxlength="15" />
                                @if ($errors->has('yearly'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('yearly') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('profile_photo') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-2 control-label">Profile Picture</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-upload"></i>
                                <input type="file" name="profile_photo" id="profile_photo" class="form-control" />
                                @if ($errors->has('profile_photo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('profile_photo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn green">Submit</button>
                            <a href="{{route('consumer.index')}}" class="btn red">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: SAMPLE FORM PORTLET -->
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">

// function checkCustType(type){
//     if (type == 'management') {
//          $('.cunsumer-panel').show();
//     }else{
//          $('.cunsumer-panel').hide();
//     }
// }

$(document).ready(function() {

    // $('input[type=radio][name=consumer_type]').change(function() {
    //      checkCustType(this.value);
    // });
    
    $("#frmconsumer").validate({
        rules: {
            first_name:{
                required:true,
                maxlength:50
            },
            last_name:{
                required:true,
                maxlength:50
            },
            email:{
                required:true,
                pattern: /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
                remote: {
                    url: "{!! url('checkUniqueEmail')!!}",
                    type: "post",
                    data: {
                        _token: function() {
                            return "{{csrf_token()}}"
                        },
                        email: function(){
                            return $("#email").val();
                        }
                    }
                },
                maxlength:150,
            },
            password:{
                required:true,
                minlength:6
            },
            phone_number:{
                required:true,
                digits:true,
                minlength:9,
                maxlength:15
            },
            monthly:{
                required: "#radio53_3:checked",
                number: true
            },
            yearly:{
                required: "#radio53_3:checked",
                number: true
            },
            profile_photo:{
                accept:'jpg|png|jpeg'
            }
        },
        messages: {
            first_name:{
                required:"@lang('validation.required',['attribute'=>'first name'])",
                maxlength:50
            },
            last_name:{
                required:"@lang('validation.required',['attribute'=>'last name'])",
                maxlength:50
            },
            email:{
                required:"@lang('validation.required',['attribute'=>'email'])",
                remote:"@lang('validation.exists',['attribute'=>'email'])",
                pattern: "Please enter a valid email address.",
                maxlength:"@lang('validation.max.string',['attribute'=>'email','max'=>150])"
            },
            password:{
                required:"@lang('validation.required',['attribute'=>'password'])",
                minlength:"@lang('validation.min.string',['attribute'=>'password','min'=>6])"
            },
            phone_number:{
                required:"@lang('validation.required',['attribute'=>'contact number'])",
                minlength:"@lang('validation.min.string',['attribute'=>'contact number','min'=>9])",
                maxlength:"@lang('validation.max.string',['attribute'=>'contact number','max'=>15])"
            },
            monthly:{
                required:"@lang('validation.required',['attribute'=>'monthly'])",
                number:"@lang('validation.numeric',['attribute'=>'monthly'])"
            },
            yearly:{
                required:"@lang('validation.required',['attribute'=>'yearly'])",
                number:"@lang('validation.numeric',['attribute'=>'yearly'])"
            },
            profile_photo:{
                accept:"@lang('validation.mimetypes',['attribute'=>'profile photo','value'=>'jpg|png|jpeg'])"
            }
        },
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function (element) {
           $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
           $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                  error.appendTo('.a');
            }else{
                if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            }
        }
    });
    $(document).on('submit','#frmconsumer',function(){
        if($("#frmconsumer").valid()){
            return true;
        }else{
            return false;
        }
    });





});

</script>
@endpush
