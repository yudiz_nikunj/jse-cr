@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('change_pass') !!}
@endsection

@section('content')
 <div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-lock font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">Change Password</span>
                </div>
            </div>
            <div class="portlet-body">
                <form class="form-horizontal" id="frmChangepass" role="form" method="POST" action="{{ route('changepass') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                        <label for="old_password" class="col-md-2 control-label">{!! $mend_sign !!}Old Password</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-lock"></i>
                                <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Old Password">
                                @if ($errors->has('old_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                @endif 
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-2 control-label">{!! $mend_sign !!}New Password</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-lock"></i>
                                <input type="password" class="form-control" name="password" id="password" placeholder="New Password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif 
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password_confirmation" class="col-md-2 control-label">{!! $mend_sign !!}Confirm Password</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-lock"></i>
                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password"> 
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn green">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: SAMPLE FORM PORTLET -->
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(function(){
        $('#frmChangepass').validate({
            errorElement: 'span',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                old_password: {
                    required: true,minlength:6,
                },
                password: {
                    required: true,minlength:6,
                },
                password_confirmation:{
                    required: true,minlength:6,equalTo:"#password"
                }
            },
            messages: {
                old_password: {
                    required: "@lang('validation.required',['attribute'=>'old password'])",
                    minlength:"@lang('validation.min.string',['attribute'=>'old password','min'=>6])"
                },
                password: {
                    required: "@lang('validation.required',['attribute'=>'password'])",
                    minlength:"@lang('validation.min.string',['attribute'=>'password','min'=>6])"
                },
                password_confirmation:{
                    required: "@lang('validation.required',['attribute'=>'confirm password'])",
                    minlength:"@lang('validation.min.string',['attribute'=>'confirm password','min'=>6])",
                    equalTo:"@lang('validation.same',['attribute'=>'password','other'=>'confirm password'])"
                }
            },
            errorClass: 'help-block',
            errorElement: 'span',
            highlight: function (element) {
               $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
               $(element).closest('.form-group').removeClass('has-error');
            },
            errorPlacement: function (error, element) {
                if (element.attr("type") == "radio") {
                      error.appendTo('.a');
                }else{
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            },
            submitHandler: function(form) {  
                if($("#frmChangepass").valid()) {
                    $(form).submit();
                }
            }
        }); 
    });
</script>
@endpush