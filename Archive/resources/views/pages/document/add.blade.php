@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('add_document') !!}
@endsection
@push('page_css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.multiselect.css') }}">
@endpush
@section('content')
 <div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{$headTitle}}</span>
                </div>
            </div>
            <div class="portlet-body">
                <form id="frmdocument" class="form-horizontal" role="form" method="POST" action="{{ route('document.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    
                    <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }} user_id">
                        <label for="user_id" class="col-md-2 control-label">Select Counsumer{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <select name="user_id[]" id="user_id"  multiple='multiple' class="3col active"  data-error-container="#user_id-error">
                                    @if($users)
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->first_name.' '.$user->last_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('user_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                @endif
                                <span id="user_id-error"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-2 control-label">Title{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-header"></i>
                                <input type="text" placeholder="Enter Title" name="title" id="title" class="form-control" value="{{old('title','')}}" maxlength="150" />
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                   
                    <div class="form-group{{ $errors->has('document') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-2 control-label">Document{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-upload"></i>
                                <input type="file" name="document" id="document" class="form-control" />
                                @if ($errors->has('document'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('document') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn green">Submit</button>
                            <a href="{{route('document.index')}}" class="btn red">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: SAMPLE FORM PORTLET -->
    </div>
</div>
@endsection

@push('scripts')
<script  src="{{ asset('js/jquery.multiselect.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#user_id').multiselect({
        columns: 1,
        search: true,
        selectAll : true,
        placeholder: 'Select Counsumer',
    });
    $('body').on('change', '#user_id', function(){
        $('#user_id').valid();
    }); 
    $("#frmdocument").validate({
        ignore:[],
        rules: {
            title:{ 
                required:true 
            },
            document:{
                required:true,
                // accept:'pdf,jpge,jpg,png,doc,docx,txt'
            },
            'user_id[]':{ 
                required:true 
            },
            
        },
        messages: {
            title:{
                required:"@lang('validation.required',['attribute'=>'title'])"
            },
            'user_id[]':{
                required:"@lang('validation.required',['attribute'=>'user'])"
            },
            document:{
                required:"@lang('validation.required',['attribute'=>'document'])",
                accept:"@lang('validation.mimetypes',['attribute'=>'document','value'=>' .pdf, .jpge, .jpg, .png, .doc, .docx, .txt, .zip'])"
            }
        },
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function (element) {
           $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
           $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                  error.appendTo('.a');
            }else{
                if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            }
        }
    });
    $(document).on('submit','#frmdocument',function(){
        if($("#frmdocument").valid()){
            if ($(this).valid()) {
                addOverlay();
                $("input[type=submit], input[type=button], button[type=submit]").prop("disabled", "disabled");
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    });
});

</script>

@endpush
