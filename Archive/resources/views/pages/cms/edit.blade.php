@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('edit_cms') !!}
@endsection

@section('content')

<div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-tag font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{$headTitle}}</span>
                </div>
            </div>
            <div class="portlet-body">
                <form id="frmPages" class="form-horizontal" role="form" method="post" action="{{ route('cms.update',$pages->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-2 control-label">{!! $mend_sign !!}Page Title</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="icon-tag"></i>
                                <input type="text" class="form-control" name="title" id="title" placeholder="Enter Page Title" maxlength="50" value="{!! old('title',$pages->title) !!}">
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif 
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="content" class="col-md-2 control-label">{!! $mend_sign !!}Page Content</label>
                        <div class="col-md-8">
                            <div class="input-icon">
                                <textarea type="text" class="form-control" rows="6" name="content" id="myEditor" placeholder="Enter Page Content">{!!  old('content',$pages->content) !!}</textarea>

                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif 
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn green">Submit</button>
                            <a href="{{route('cms.index')}}" class="btn red">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: SAMPLE FORM PORTLET -->
    </div>
</div>
</div>

@endsection

@push('scripts')     
    <!-- Include JS file. -->
    <script type='text/javascript' src='{{ asset('') }}tinymce/tinymce.min.js'></script>

    <script type="text/javascript">
        $(document).ready(function() {
            tinymce.init({ 
                selector:'textarea',
                plugins: [
                  'advlist autolink link image lists charmap print preview hr anchor pagebreak',
                  'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                  'save table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
                menubar: "tools",
                height: 300,
                valid_elements : '*[*]',
                allow_html_in_named_anchor: true,
                allow_unsafe_link_target: true,
                convert_fonts_to_spans : false,
                entity_encoding : "raw",
                cleanup_on_startup: false,
                trim_span_elements: false,
                verify_html: false,
                cleanup: false,
                convert_urls: false,
                allow_script_urls: true,
                relative_urls : true,
                document_base_url : "{{ env('APP_URL') }}/jse",
                valid_children : "body[p,ol,ul]"
                + ",p[a|span|b|i|u|sup|sub|img|hr|#text]"
                + ",span[a|b|i|u|sup|sub|img|#text]"
                + ",a[p|figure|span|b|i|u|sup|sub|img|#text]"
                + ",b[span|a|i|u|sup|sub|img|#text]"
                + ",i[span|a|b|u|sup|sub|img|#text]"
                + ",sup[span|a|i|b|u|sub|img|#text]"
                + ",sub[span|a|i|b|u|sup|img|#text]"
                + ",li[span|a|b|i|u|sup|sub|img|ol|ul|#text]"
                + ",ol[li]"
                + ",ul[li]",
            });
        });
    </script>

    {{-- <script type="text/javascript">
		jQuery(document).ready(function($) {
			CKEDITOR.replace('content').config.allowedContent = {
                $1: {
                    // Use the ability to specify elements as an object.
                    elements: CKEDITOR.dtd,
                    attributes: true,
                    styles: true,
                    classes: true
                }
            };
		});
	</script> --}}
@endpush
