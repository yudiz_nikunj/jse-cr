
@extends('frontend.layout')

@section('header')

    @include('frontend.partials.user_menu')

@endsection

@section('main')

<!--******************* Middle Section Start ******************-->
<section class="common-section paddingb-none">
    <div class="container">
        <div class="section-title wow">
            <h1>INVOICES</h1>
        </div>
        <div class="height-saperator"></div>
        <p>Here is where you can view and pay all your invoices. Please note we operate on seven day payment terms upon completion of each job. You will be alerted by text and email once the invoice is available to pay through our secure client portal.</p>
        <div class="height-saperator"></div>
        <div class="portal-data-wrap">
            <div class="portal-heading-bar">
                <div class="row">
                    <div class="col-sm-7">
                        <h2><img src="{{ asset('frontend/images/LogoIcon-YELLLOW.png') }}" alt="jse logo" class="wow animatedslow flipInY"/> INVOICE MANAGER</h2>
                    </div>
                    <div class="col-sm-5 header-select">
                        <select class="custom-select" name="sort_by" id="sort_by">
                            <option value="duedate" {{ ($sort_by == "duedate") ? 'selected' : '' }} >Date</option>
                            <option value="postcode" {{ ($sort_by == "postcode") ? 'selected' : '' }} >Property</option>
                            <option value="id" {{ ($sort_by == "id") ? 'selected' : '' }} >Invoice</option>
                            <option value="status" {{ ($sort_by == "status") ? 'selected' : '' }} >Status</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="portal-invoice-block">
                <div class="row">
                    @if(count($invoices))
                        @foreach($invoices as $invoice)
                            <div class="col-sm-6 col-md-4 wow animatedslow fadeIn">
                                <div class="invoice-box">
                                    <ul>
                                        <li>
                                            <div class="col-xs-6 invoice-title">PROPERTY</div>
                                            <div class="col-xs-6 invoice-data">{{ $invoice->postcode }}</div>
                                        </li>
                                        {{-- <li>
                                            <div class="col-xs-6 invoice-title">ADDRESS</div>
                                            <div class="col-xs-6 invoice-data">{{ $invoice->address }}</div>
                                        </li> --}}
                                        <li>
                                            <div class="col-xs-6 invoice-title">INVOICE #</div>
                                            <div class="col-xs-6 invoice-data">{{ $invoice->id }}</div>
                                        </li>
                                        <li>
                                            <div class="col-xs-6 invoice-title">REFERENCE</div>
                                            <div class="col-xs-6 invoice-data">{{ $invoice->reference }}</div>
                                        </li>
                                        <li>
                                            <div class="col-xs-6 invoice-title">ISSUE DATE</div>
                                            <div class="col-xs-6 invoice-data">{{ date('d/m/Y',strtotime($invoice->duedate)) }}</div>
                                        </li>
                                        <li>
                                            <div class="col-xs-6 invoice-title">VALUE</div>
                                            <div class="col-xs-6 invoice-data">£{{ $invoice->value }}</div>
                                        </li>
                                        <li class="invoice-payment">
                                            <div class="col-xs-6 invoice-title">STATUS</div>
                                            <div class="col-xs-6 invoice-data">
                                                @if ($invoice->status == "pending")
                                                        <a href="{{ route('invoice.pay',$invoice->id) }}" class="pay-invoice">pay now</a>
                                                @else
                                                    PAID
                                                @endif
                                            </div>
                                        </li>
                                        <li class="invoice-action">
                                            <div class="col-xs-6 invoice-title">VIEW/PRINT</div>
                                            <div class="col-xs-6 invoice-data"><a href="{{ route('view_invoice',$invoice->id) }}" target="_blank"><img src="{{ asset('frontend/images/invoice-view.png') }}" alt="invoice view" /></a><a href="{{ route('download_invoice',$invoice->id) }}"><img src="{{ asset('frontend/images/invoice-print.png') }}" alt="invoice print" /></a></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-sm-12 col-md-12 wow animatedslow fadeIn">
                            <div class="note note-danger">
                                <strong>There are no invoices available.</strong>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="portal-footer-bar">
                {{ $invoices->links('frontend.partials.pagination',['paginator' => $invoices]) }}
            </div>
        </div>
    </div>
</section>
<!--******************* Middle Section End ******************-->

@endsection

@push('scripts')

<script>
        jQuery(document).ready(function($) {
            var url = "{{ Request::url() }}";
            $(document).on('change', '#sort_by', function(event) {
                event.preventDefault();
                url+="?sort="+$(this).val();
                window.location=url;
            });
        });
</script>

@endpush
