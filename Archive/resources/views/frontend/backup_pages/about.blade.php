@extends('frontend.layout')

@section('header')
<section class="main-banner page-banner about-banner">
        
    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="banner-desc">
        <h4 class="wow animatedslow fadeIn">{!! $page->title !!}</h4>
    </div>
    <!--******************* Banner Section End ******************-->
    
</section>
@endsection


@section('main')

<!--******************* Middle Section Start ******************-->

    {!! $page->content !!}

<!--******************* Middle Section End ******************-->

@endsection