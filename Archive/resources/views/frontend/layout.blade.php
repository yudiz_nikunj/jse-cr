<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>JSE Property Management {{ !empty($title) ? " - ".$title : "" }} </title>
    <noscript>
        <meta http-equiv="refresh" content="0; url={{ route('noscript') }}" />
    </noscript>
    <!--Favicon Included-->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('') }}frontend/images/fevicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('') }}frontend/images/fevicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('') }}frontend/images/fevicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('') }}frontend/images/fevicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('') }}frontend/images/fevicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('') }}frontend/images/fevicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('') }}frontend/images/fevicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('') }}frontend/images/fevicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('') }}frontend/images/fevicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('') }}frontend/images/fevicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('') }}frontend/images/fevicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('') }}frontend/images/fevicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('') }}frontend/images/fevicon/favicon-16x16.png">
    <link rel="manifest" href="{{ asset('') }}frontend/images/fevicon/manifest.json">

    <!-- lato font -->
    <!-- <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap" rel="stylesheet"> -->
    <!--Font Awsome Included-->
    <link href="{{ asset('') }}frontend/css/fontawesome.css" type="text/css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link href="{{ asset('') }}frontend/css/bootstrap.css" rel="stylesheet">

    <link href="{{ asset('') }}frontend/css/jcf.css" type="text/css" rel="stylesheet" />

    
    <!--Extra Style Included-->
    <link type="text/css" href="{{ asset('') }}frontend/css/jquery.mmenu.all.css" rel="stylesheet" />
    <link type="text/css" href="{{ asset('') }}frontend/css/jquery-ui.css" rel="stylesheet" />
    <link type="text/css" href="{{ asset('') }}frontend/css/jquery-ui.css" rel="stylesheet" />
    <link type="text/css" rel="stylesheet" href="{{ asset('') }}frontend/css/slick.css" />
    <link type="text/css" rel="stylesheet" href="{{ asset('') }}frontend/css/slick-theme.css" />
    <!-- fancybox V3.5 css added -->
    <link type="text/css" rel="stylesheet" href="{{ asset('') }}frontend/css/jquery.fancybox.css" />

    

    <!--Main Style Included-->
    <link href="{{ asset('') }}frontend/css/style.css" type="text/css" rel="stylesheet" />

    <!--Main Js Included-->
    <script src="{{ asset('') }}frontend/js/lib/jquery.js" type="text/javascript"></script>
    <script src="{{ asset('') }}frontend/js/modernizr.js" type="text/javascript"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('') }}frontend/js/bootstrap.js"></script>
    <link href="{{ asset('plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
    <!--Extra Js Included-->
    <script type="text/javascript" src="{{ asset('') }}frontend/js/jquery.mmenu.all.js"></script>
    <script src="{{ asset('') }}frontend/js/jquery.vide.js"></script>
    <script src="{{ asset('') }}frontend/js/jquery-ui.min.js"></script>
    <script src="{{ asset('') }}frontend/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
    <script>
        var SITE_URL = '{{ env('APP_URL').'/images/' }}';
    </script>
    <script src="{{ asset('') }}frontend/js/jcf.js" ></script>
    <script src="{{ asset('') }}frontend/js/jcf.radio.js" ></script>
    <script src="{{ asset('') }}frontend/js/jcf.select.js" ></script>
   
    <script src="{{ asset('') }}frontend/js/slick.min.js" ></script>
    <!-- fancybox js added -->
    <script src="{{ asset('') }}frontend/js/jquery.fancybox.js" ></script>



    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="{{ asset('') }}frontend/js/html5shiv.min.js"></script>
      <script src="{{ asset('') }}frontend/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .note.note-danger{
            background-color: #f56e74;
            border-color: #f02651;
            color: white;
        }

        .note {
            border-radius: 2px;
            border: 0;
            margin: 0 0 20px 0;
            padding: 15px 30px 15px 15px;
        }
    </style>
    <script type="text/javascript">
    	<!--
    	if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
      	  var msViewportStyle = document.createElement('style');
      	  msViewportStyle.appendChild(
      	    document.createTextNode(
      	      '@-ms-viewport{width:auto!important}'
      	    )
      	  );
      	  document.querySelector('head').appendChild(msViewportStyle);
      	}
    	-->
    </script>
    <!-- MM Menu -->
    <script type="text/javascript">
       jQuery(document).ready(function( $ ) {
            var themeColor = "theme-black";
            var bodyClass = $("body").attr('class');
            if (bodyClass.trim() == "template-portal") {
                themeColor = "theme-white";
            }
            $("#menu").mmenu({
              extensions 	: [ "border-none", themeColor],
            	offCanvas	: {
            		zposition 	: "front",
            		position 	: "bottom"
            	}
            });
            var API = $("#menu").data( "mmenu" );
            API.close();
            $("#my-button").click(function() {
                API.open();
            });
            $('.menu-item').click(function(){
                API.close();
            });
            jcf.replaceAll();
       });
    </script>

    @stack('styles')

    <script type="text/javascript">
        var browser = '';
        var browserVersion = 0;

        if (/Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
            browser = 'Opera';
        } else if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
            browser = 'MSIE';
        } else if (/Navigator[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
            browser = 'Netscape';
        } else if (/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
            browser = 'Chrome';
        } else if (/Safari[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
            browser = 'Safari';
            /Version[\/\s](\d+\.\d+)/.test(navigator.userAgent);
            browserVersion = new Number(RegExp.$1);
        } else if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
            browser = 'Firefox';
        }
        if(browserVersion === 0){
            browserVersion = parseFloat(new Number(RegExp.$1));
        }
       

        jQuery(document).ready(function( $ ) {
             if(browser == 'Safari'){
                jQuery("body").addClass('safari');
            }
        });   
    </script>
</head>
<body class="@yield('class') ">
    <div class="page">
        <!--******************* Header Section Start *********************-->

            {{-- @include('frontend.partials.header') --}}
            @yield('header')

        <!--******************* Header Section End *********************-->

        <!--******************* Middle Section Start ******************-->

            @yield('main')

        <!--******************* Middle Section End ******************-->

        <!--******************* Footer Section Start ******************-->

            @include('frontend.partials.footer')

        <!--******************* Footer Section End ******************-->
    </div>
    <!--*********************** All End ************************-->

    @include('flash::message')

    @stack('scripts')

    <!--Parallex Background Start-->
    <!--Parallex Background End-->
    <!--Wow Animation Start-->
     <!-- Start of jsepropertymanagement Zendesk Widget script -->
        <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=4e9053c9-644b-47d8-a97e-f34349dc5c29"> </script>
        <!-- End of jsepropertymanagement Zendesk Widget script -->
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}frontend/css/animate.css"/>
    <script src="{{ asset('') }}frontend/js/wow.js" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">

        var BASE_URL = "{{ url('') }}";

        $(document).ready(function(){

            $(".desktop-menu > a").click(function(){
                $(".menu-box").addClass("open fadeIn");
                $(".main-menu").addClass("fadeInDown animated");
            });
            $("a.close-menu").click(function(){
                $(".menu-box").removeClass("open");
                $(".main-menu").removeClass("fadeInDown animated");
            });
            var $win = $(window);
            $(".bg-parallex").each(function(){
                var scroll_speed = 3;
                var $this = $(this);
                $(window).scroll(function() {
                    var bgScroll = -(($win.scrollTop() - $this.offset().top)/ scroll_speed);
                    var bgPosition = 'center '+ bgScroll + 'px';
                    $this.css('background-position', bgPosition);
                });
            });
            $('a[data-toggle="collapse"]').on('click', function () {
              if($(this).hasClass('active')){
                 $(this).removeClass('active');
              }
              else{
                  $('.panel .panel-title a').removeClass('active');
                  $(this).addClass('active');
              }
            })
        });

        wow = new WOW(
            {
                animateClass: 'animated',
                offset:       100,
                callback:     function(box) {
                }
            }
        );
        wow.init();

        toastr.options = {
          "closeButton": true,
          "debug": false,
          "positionClass": "toast-top-right",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
        $(document).ready(function(){
            var cookieEnabled = (navigator.cookieEnabled) ? true : false;

            if (!cookieEnabled)
            {
                document.cookie="testcookie";
                checkCookie = (document.cookie.indexOf("testcookie") != -1) ? true : false;
                if(!checkCookie){
                    window.location.href = '{{ route('nocookie') }}'
                }
            }
        });
    </script>
    <script>
        var el = $('.slideCount span');
        var slickEl = $('.slider-wrapper');

        slickEl.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
            //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
            var i = (currentSlide ? currentSlide : 0) + 1;
            el.text(i + '/' + slick.slideCount);
        });
        //  $('.slider-wrapper').slick({
        //     infinite: true,
        //     loop: false,
        //     slidesToShow: 1,
        //     slidesToScroll: 1,
        //     dots:false,
        //     arrows:false,
        //     asNavFor: '.slider-wrapper-vertical'
        // });
        // $('.slider-wrapper-vertical').slick({
        //     infinite: true,
        //     loop: false,
        //     vertical: true,
        //     verticalSwiping: true,
        //     initialSlide: 1,
        //     slidesToShow: 2,
        //     slidesToScroll: 1,
        //     dots:false,
        //     arrows:false,
        //     rows:0,            
        //     asNavFor: '.slider-wrapper'
        // });
        // $('.left-arrow').click(function(){
        //     $('.slider-wrapper').slick("prev");
        // });
        // $('.right-arrow').click(function(){
        //     $('.slider-wrapper').slick("next");
        // });

        // $('.slider-wrapper').on('afterChange', function(event, slick, currentSlide){
        //         $('.slider-wrapper-vertical').slick('next');
        // });

        // var slider = document.getElementById("myRange");
        // var output = document.getElementById("demo");
        // output.innerHTML = slider.value;

        // slider.oninput = function() { output.innerHTML = this.value; }        
        
        // $('[data-fancybox="images"]').fancybox({
        //     thumbs : {
        //         autoStart : true,
        //         axis      : 'x'
        //     }
        // })
       
    </script>
    <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>

</body>
</html>
