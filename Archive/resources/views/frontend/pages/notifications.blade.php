
@extends('frontend.layout')

@section('header')

    @include('frontend.partials.user_menu')

@endsection

@section('main')

<!--******************* Middle Section Start ******************-->
<section class="common-section paddingb-none">
    <div class="container">
        <div class="section-title wow">
            <h1>NOTIFICATIONS</h1>
        </div>
        <div class="height-saperator"></div>
        <p>Here is where you can view all your notifications. We will use this space to keep you up to date about any jobs being carried out for you. We will also notify you about invoices being raised and any other relevant news.</p>
        <div class="height-saperator"></div>
        <div class="portal-data-wrap">
            <div class="portal-heading-bar">
                <div class="row">
                    <div class="col-sm-7">
                        <h2><img src="{{ asset('frontend/images/JSE-Icon-vector.svg') }}" alt="jse logo" class="wow animatedslow flipInY"/> NOTIFICATIONS</h2>
                    </div>
                    <div class="col-sm-5 header-select">
                        <select class="custom-select" name="sort_by" id="sort_by">
                            <option value="{{ encrypt('created_at') }}" {{ ($sort_by == "created_at") ? 'selected' : '' }} >Date</option>
                            <option value="{{ encrypt('property') }}" {{ ($sort_by == "property") ? 'selected' : '' }} >Property</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <ul class="notifications-list">
                @foreach($notifications as $notification)
                    <li class="wow animatedslow fadeIn {{ ($notification->status==1) ? 'new' : '' }}">
                        <span class="n-date">
                            {{ $notification->created_at->format('d/m/Y') }}
                        </span>

                        <span class="n-title">
                            @if(!empty($notification->property))
                                <b>{{  $notification->property }}:</b>
                            @endif
                            {{ $notification->title }}
                        </span>

                        <span class="n-desc">{{ $notification->description }}</span>
                    </li>
                @endforeach
            </ul>
            <div class="portal-footer-bar">
                {{ $notifications->links('frontend.partials.pagination',['paginator' => $notifications]) }}
            </div>
        </div>
    </div>
</section>
<!--******************* Middle Section End ******************-->

@endsection


@push('scripts')

<script>
        jQuery(document).ready(function($) {
            var url = "{{ Request::url() }}";
            $(document).on('change', '#sort_by', function(event) {
                event.preventDefault();
                url+=window.location.search+"&sort="+$(this).val();
                window.location=url;
            });
        });
</script>

@endpush
