@extends('frontend.layout')

@section('header')

<section class="video-banner main-banner" data-vide-bg="{{ asset('') }}frontend/media/video">

    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="banner-desc">
        <div class="container">
            <h1>{!! $page->title !!}</h1>
            <a href="about" class="wow animatedslow fadeIn">Find Out More</a>
        </div>
    </div>
    <!--******************* Banner Section End ******************-->
</section>

@endsection

@section('main')

    {!! $page->content !!}

    {{-- @if($property->count() > 0 )
        <section class="latest-properties-wrapper common-section">
            <div class="container">
                <div class="section-title wow animated" style="visibility: visible;">
                    <h1 class="text-uppercase">LATEST PROPERTIES</h1>
                </div>
                <div class="height-saperator"></div>
                <div class="row">

                    @foreach($property as $prop)
                        <div class="col-sm-4">
                            <div class="card mb-4 box-shadow">
                                <a class="box-link" href="{{ route('property-detail',$prop->id) }}"></a>
                                <img class="card-img-top" alt="card-image" style="height: 225px; width: 100%; display: block;" src="{{ $prop->propertyimg() }}" data-holder-rendered="true">
                                <div class="card-body">
                                    <div class="card-text">
                                        <p>{{ $prop->name }}</p>
                                        <p>£{{ $prop->price }}</p>
                                    </div>
                                    <div class="property-type-box">
                                        <p class="text-capitalize">{{ $prop->location }}</p>
                                        <p class="text-capitalize">{{ $prop->property_type }}</p>
                                    </div>
                                    <div class="card-icon-box">
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-4 d-flex">
                                                <div class="align-items-center"><figure><img src="{{ asset('frontend/images/shower-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure>{{ $prop->bathroom }}</div>
                                            </div>
                                            <div class="col-sm-4 col-xs-4 d-flex">
                                                <div class="align-items-center"><figure><img src="{{ asset('frontend/images/bed-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure>{{ $prop->bedroom }}</div>
                                            </div>
                                            <div class="col-sm-4 col-xs-4 d-flex">
                                                <div class="align-items-center"><figure><img src="{{ asset('frontend/images/sofa-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure>{{ $prop->livingroom }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    
                    <div class="property-btn text-center">
                        <a class="theme-btn wow slow fadeIn animated" href="new-property" style="visibility: visible; animation-name: fadeIn;">view more</a>
                    </div>
                </div> <!-- row ends -->
            </div> <!-- container ends -->
        </section>
    @endif --}}

    @include('frontend.partials.get_in_touch')
    @include('frontend.partials.map')

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.services-section').before(`

                @if($property->count() > 0 )
                <section class="latest-properties-wrapper common-section">
                    <div class="container">
                        <div class="section-title wow animated" style="visibility: visible;">
                            <h1 class="text-uppercase">LATEST PROPERTIES</h1>
                        </div>
                        <div class="height-saperator"></div>
                        <div class="row">

                            @foreach($property as $prop)
                                <div class="col-sm-4">
                                    <div class="card mb-4 box-shadow">
                                        <a class="box-link" href="{{ route('property-detail',$prop->slug) }}"></a>
                                        <img class="card-img-top" alt="card-image" style="height: 225px; width: 100%; display: block;" src="{{ $prop->propertyimg() }}" data-holder-rendered="true">
                                        <div class="card-body">
                                            <div class="card-text">
                                                <p> <span> {{ str_limit($prop->name, 55, '...')  }} </span></p>
                                                <p>£{{ number_format($prop->price,2) }}</p>
                                            </div>
                                            <div class="property-type-box">
                                                <p class="text-capitalize">{{ $prop->location }}</p>
                                                <p class="text-capitalize">{{ $prop->property_type }}</p>
                                            </div>
                                            <div class="card-icon-box">
                                                <div class="row">
                                                    @if($prop->bathroom>0)
                                                    <div class="col-sm-4 col-xs-4 text-left">
                                                        <div class="icon-holder"><figure><img src="{{ asset('frontend/images/shower-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure> <span> {{ $prop->bathroom }} </span> </div>
                                                    </div>
                                                    @endif
                                                    @if($prop->bedroom>0)
                                                    <div class="col-sm-4 col-xs-4 text-center">
                                                        <div class="icon-holder"><figure><img src="{{ asset('frontend/images/bed-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure> <span> {{ $prop->bedroom }}</span></div>
                                                    </div>
                                                    @endif
                                                    @if($prop->livingroom>0)
                                                    <div class="col-sm-4 col-xs-4 text-right">
                                                        <div class="icon-holder"><figure><img src="{{ asset('frontend/images/sofa-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure> <span>  {{ $prop->livingroom }}</span></div>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            
                            <div class="property-btn text-center col-xs-12">
                                <a class="theme-btn wow slow fadeIn animated" href="new-property" style="visibility: visible; animation-name: fadeIn;">view more</a>
                            </div>
                        </div> <!-- row ends -->
                    </div> <!-- container ends -->
                </section>
                @endif
            `)
        })
    </script>    
@endpush


