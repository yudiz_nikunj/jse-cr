@extends('frontend.layout')


@section('header')
<section class="main-banner page-banner service-overview-banner">
        
    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="banner-desc">
        <h4 class="wow animatedslow fadeIn"> {!! $page->title !!} </h4>
    </div>
    <!--******************* Banner Section End ******************-->
    
</section>
@endsection

@section('main')

<!--******************* Middle Section Start ******************-->
    {!! $page->content !!}
    
    @include('frontend.partials.get_in_touch')
<!--******************* Middle Section End ******************-->

@endsection