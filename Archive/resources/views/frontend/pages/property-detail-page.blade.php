@extends('frontend.layout')

@section('header')
<section class="main-banner page-banner about-banner">
        
    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="banner-desc">
        <h4 class="wow animatedslow fadeIn">property details</h4>
    </div>
    <!--******************* Banner Section End ******************-->
    
</section>
@endsection


@section('main')

    <html>
       <section class="property-details-page-wrapper">
           <div class="container">
                <div class="slider-title-wrap">
                    <div>
                        <a href="{{ route('newproperty') }}"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back to Search</a>
                    </div>
                    <ul class="property-nav">
                        @if(isset($previous->slug))
                            <li><a href="{{ route('property-detail',$previous->slug) }}"><figure><img src="{{ asset('frontend/images/left-arrow.svg') }}" alt="arrow"/></figure></a></li>
                        @endif

                        @if(isset($next->slug))
                            <li><a href="{{ route('property-detail',$next->slug) }}"><figure><img src="{{ asset('frontend/images/right-arrow.svg') }}" alt="arrow"/></figure></a></li>
                        @endif
                    </ul>
                   
                </div>
                <div class="slider-box">
                   
                    <div class="parent">
                        @foreach($property->images->take(3) as $img)
                            <div class="div{{ $loop->iteration }}" style="background-image:url('{{ Storage::url( $img->name ) }}')">
                                <a data-fancybox="gallery" href="{{  Storage::url( $img->name ) }}" data-thumbs='{"autoStart": true, "axis": "x"}'>
                                </a>   
                            </div> 
                        @endforeach                    
                        
                        <div style="display: none;">
                            @foreach($property->images as $key => $img)
                                @if($key >= 3)
                                    <a data-fancybox="gallery" href="{{ Storage::url( $img->name ) }}" data-thumbs="{{ Storage::url( $img->name ) }}">
                                        <img src="{{ Storage::url( $img->name ) }}" alt="slider-image" />
                                    </a> 
                                @endif
                            @endforeach
                        </div>
                    </div>
                    @if($property->images->count() > 0)
                    <div class="slide-count" style="cursor: pointer;">
                        <figure><img src="{{ asset('frontend/images/image.svg') }}" alt="image"/></figure>                        
                        <span>{{ $property->images->count() >= 3 ? 3 : $property->images->count() }}/{{$property->images->count()}}</span>
                        
                    </div>
                    @endif
                </div>
               
                <div class="title-box">
                    <h3>{{ $property->name}}</h3> 
                    <h1>£{{  number_format($property->price,2)}}</h1>
                </div>
                <div class="property-details-counter-wrap">
                    <div class="row">
                        @if($property->bathroom>0)
                        <div class="col-lg-3 col-sm-4 col-xs-6">
                            <div class="bathroom-box room-box">
                                <div class="circle-box">
                                    <figure><img src="{{ asset('frontend/images/shower-big-icon.svg') }}" alt="jse management icon" class="wow slow flipInY animated" style="visibility: visible; animation-name: flipInY;"></figure>
                                </div>
                                <div class="counter-box">
                                    <p class="counter-title title">Bathrooms</p>
                                    <p>{{ $property->bathroom}}</p>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($property->bedroom>0)
                        <div class="col-lg-3 col-sm-4 col-xs-6">
                            <div class="bedroom-box room-box">
                                <div class="circle-box">
                                    <figure><img src="{{ asset('frontend/images/bed-big-icon.svg') }}" alt="jse management icon" class="wow slow flipInY animated" style="visibility: visible; animation-name: flipInY;"></figure>
                                </div>
                                <div class="counter-box">
                                    <p class="counter-title title">Bedrooms</p>
                                    <p>{{ $property->bedroom }}</p>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($property->livingroom>0)
                        <div class="col-lg-3 col-sm-4 col-xs-6">
                            <div class="livingroom-box room-box">
                                <div class="circle-box">
                                    <figure><img src="{{ asset('frontend/images/sofa-big-icon.svg') }}" alt="jse management icon" class="wow slow flipInY animated" style="visibility: visible; animation-name: flipInY;"></figure>
                                </div>
                                <div class="counter-box">
                                    <p class="counter-title title">Living Rooms</p>
                                    <p>{{ $property->livingroom }}</p>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div> 
                    <div class="row">
                        <div class="col-lg-3 col-sm-4">
                            <div class="property-detail-box">
                                <p class="title-box title"> Location  </p>
                                <p> {{ $property->location}} </p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-4">
                            <div class="property-detail-box">
                                <p class="title-box title"> Property Type  </p>
                                <p> {{ $property->property_type }} </p>
                            </div>
                        </div>
                    </div>
                </div> <!-- property details counter wrap closed -->
                <div class="description-box">
                        <p class="title">Property Description</p>
                        {!! $property->description !!}
                </div>
                
                <div class="amenities-wrap section-title wow animated">
                    @if($property->amenities->count())
                    <h1>Amenities</h1>
                    <div class="d-flex">
                        @foreach($property->amenities as $aminity)
                        <div class="amenities-box text-center">
                            <figure><img src="{{ asset($aminity->icon) }}" alt="jse management icon" class="wow slow flipInY animated" style="visibility: visible; animation-name: flipInY;"></figure>
                            <p>{{ $aminity->title }}</p>
                        </div>
                        @endforeach
                    </div>
                    @endif
                    <a class="theme-btn wow slow fadeIn  animated inquiry" href="javascript:" style="visibility: visible; animation-name: fadeIn;">Enquire Now</a>
                </div>
                
            </div>
       </section>
    </html>
    @include('frontend.partials.propertymap',['latitude'=>$property->latitude,'longitude'=>$property->longitude])
    @include('frontend.partials.inquiry',['property_name'=>$property->name])   

@endsection
@push('scripts')
<script type="text/javascript">
    $(document).on('click','.inquiry',function(){
        $('html, body').animate({
            scrollTop: $("#inquiry").last().offset().top
        }, 1000);
    })

    $(document).on('click','.slide-count',function(){
       $(".parent .div1 a").click()
    })
    
</script>
@endpush

