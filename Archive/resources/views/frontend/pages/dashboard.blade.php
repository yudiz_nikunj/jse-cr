@extends('frontend.layout')


@include('frontend.partials.user_menu')
<!--******************* Middle Section Start ******************-->
<section class="common-section">
    <div class="container">
        <div class="saperator wow animatedslow fadeIn"></div>
        <p><b>Manage your properties, receive invoices and pay securely using our Client Portal. We will send you notifications once actions have been completed.</b></p>
        <p>Out system is easy to use, but feel free to get in touch should you require further assistance.</p>
    </div>
</section>
<section class="services-section">
    <div class="row">
        <div class="col-sm-6">
            <a href="{{ route('properties') }}" class="services-block properties">
                <p>PROPERTIES</p>
                <figure>
                    <img src="{{ asset('frontend/images/JSE-Icon-vector.svg') }}" alt="jse logo" class="wow animatedslow flipInY"/>
                </figure>
            </a>
        </div>
        <div class="col-sm-6">
            <a href="{{ route('invoices') }}" class="services-block invoices">
                <p>INVOICES</p>
                <figure>
                    <img src="{{ asset('frontend/images/JSE-Icon-vector.svg') }}" alt="jse logo" class="wow animatedslow flipInY"/>
                </figure>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <a href="{{ route('notifications') }}" class="services-block notifications">
                <p>NOTIFICATIONS</p>
                <figure>
                    <img src="{{ asset('frontend/images/JSE-Icon-vector.svg') }}" alt="jse logo" class="wow animatedslow flipInY"/>
                </figure>
            </a>
        </div>

        <div class="col-sm-6">
            <a href="{{ route('documents') }}" class="services-block invoices">
                <p>DOCUMENTS</p>
                <figure>
                    <img src="{{ asset('frontend/images/JSE-Icon-vector.svg') }}" alt="jse logo" class="wow animatedslow flipInY"/>
                </figure>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
</section>
<section class="common-section signup-section bg-parallex">
    <div class="container">
        <div class="section-title wow">
            <h1>TALK WITH US</h1>
        </div>
        <div class="height-saperator"></div>
        <p>Click on the 'Request Call Back' button to send request to speak to us. We'll call back within 24 hours.</p>
        <a href="{{ route('request_callback') }}" class="theme-btn dark-btn">Request Call Back</a>
    </div>
</section>
<!--******************* Middle Section End ******************-->
