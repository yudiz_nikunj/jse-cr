
@extends('frontend.layout')

@section('header')

    @include('frontend.partials.user_menu')

@endsection

@section('main')

<!--******************* Middle Section Start ******************-->
<section class="common-section paddingb-none">
    <div class="container">
        <div class="section-title wow">
            <h1>DOCUMENTS</h1>
        </div>
        <div class="height-saperator"></div>
        <p>Here is where you can view all your notifications. We will use this space to keep you up to date about any jobs being carried out for you. We will also notify you about invoices being raised and any other relevant news.</p>
        <div class="height-saperator"></div>
        <div class="portal-data-wrap">
            <div class="portal-heading-bar">
                <div class="row">
                    <div class="col-sm-7">
                        <h2><img src="{{ asset('frontend/images/JSE-Icon-vector.svg') }}" alt="jse logo" class="wow animatedslow flipInY"/> DOCUMENTS</h2>
                    </div>
    
                    <div class="clearfix"></div>
                </div>
            </div>
            <ul class="notifications-list documents-list">
                @foreach($documents as $document)
                    <li class="wow animatedslow fadeIn">
                        <div class="document-inner-block">
                            <div class="left-doc-content">
                                <img src="https://webdevprojects.cloud/php/laravel/jse/frontend/images/docs-icon.svg">
                                <p class="n-title" >{{ $document->title }}</p>
                            </div>
                            <div class="right-doc-content">
                                <span class="n-size">{{ formatBytes($document->size) }}</span>
                                <img src="https://webdevprojects.cloud/php/laravel/jse/frontend/images/download-black-icon.svg">
                            </div>
                            <a href="{{ asset('storage/app/public/'.$document->path) }}" target="_blank"></a>
                        </div>
                    </li>
                @endforeach
            </ul>
            <div class="portal-footer-bar">
                {{ $documents->links('frontend.partials.pagination',['paginator' => $documents]) }}
            </div>
        </div>
    </div>
</section>
<!--******************* Middle Section End ******************-->

@endsection


@push('scripts')

<script>
        jQuery(document).ready(function($) {
            var url = "{{ Request::url() }}";
            $(document).on('change', '#sort_by', function(event) {
                event.preventDefault();
                url+=window.location.search+"&sort="+$(this).val();
                window.location=url;
            });
        });
</script>

@endpush
