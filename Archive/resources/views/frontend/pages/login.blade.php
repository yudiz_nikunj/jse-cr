@extends('frontend.layout')

@section('header')

@push('styles')

    <style type="text/css">
        .help-block{
            color: #c31e1e !important;
            font-size: 18px !important;
        }
    </style>

@endpush

<section class="main-banner video-banner bg-parallex" data-vide-bg="{{ asset('') }}frontend/media/video">
        
    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="login-block">
        <div class="container">
            <span>CLIENT PORTAL</span>
            <form method="post" action="{{  url('login') }}" id="frmuserlogin">
                {{ csrf_field() }}
                <div class="col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
                    <div class="row">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control" placeholder="Email" name="email" id="email">
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-default">Login</button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </form>
            <a href="{{ url('password/reset') }}" class="wow animatedslow fadeIn">Lost your Password</a>
        </div>
    </div> 
    <!--******************* Banner Section End ******************-->
</section>

@endsection


@push('scripts')
<script type="text/javascript">
$(document).ready(function() {

    $("#frmuserlogin").validate({
        rules: {
            email: {
                required: true,
                pattern: /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
                email: true
            },
            password: {
                required: true
            }
        },
        messages: {
            email: {
                required: "Email is required.",
                email: "Enter valid email address.",
                pattern: "Enter valid email address."
            },
            password: {
                required: "Password is required."
            }
        },
        errorClass: 'help-block',
        errorElement: 'p',
        highlight: function (element) {
           $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
           $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                  error.appendTo('.a');
            }else{
                if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            }
        }
    });
    $(document).on('submit','#frmuserlogin',function(){
        if($("#frmuserlogin").valid()){
            return true;
        }else{
            return false;
        }
    });
});

</script>
@endpush

