@extends('frontend.layout')

@section('header')
<section class="main-banner page-banner service-development-banner">

    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="banner-desc">
        <h4 class="wow animatedslow fadeIn">{!! $page->title !!}</h4>
    </div>
    <!--******************* Banner Section End ******************-->

</section>
@endsection


@section('main')

<!--******************* Middle Section Start ******************-->

    {!! $page->content !!}

	<section class="common-section signup-section bg-parallex" style="background-position: center -16.6667px;">
	    <div class="container">

	        <div class="signup-form">
	            <figure><img src="{{ asset('/') }}frontend/images/LogoIcon-BLACK.png" alt="jse-logo" class="wow animatedslow flipInY"/></figure>
	            <h3>SIGN UP NOW!</h3>
	            <p>Enter your details below and we'll contact you shortly to provide you with a quotation based on the size of your property</p>



	    		@include('frontend.partials.signup_form')



	    		<div class="clearfix"></div>
	        </div>
	    </div>
	</section>

<!--******************* Middle Section End ******************-->

@endsection
