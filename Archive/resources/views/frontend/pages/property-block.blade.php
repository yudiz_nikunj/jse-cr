@foreach($property as $prop)
    <div class="col-sm-4 prop-block">
        <div class="card mb-4 box-shadow">
            <a class="box-link" href="{{ route('property-detail',$prop->slug) }}"></a>
            <img class="card-img-top" alt="card-image" style="height: 225px; width: 100%; display: block;" src="{{ $prop->propertyimg() }}" data-holder-rendered="true">
            <div class="card-body">
                <div class="card-text">
                    <p>{{ $prop->name }}</p>
                    <p>£{{ number_format($prop->price,2) }}</p>
                </div>
                <div class="property-type-box">
                    <p class="text-capitalize">{{ $prop->location }}</p>
                    <p class="text-capitalize">{{ $prop->property_type }}</p>
                </div>
                <div class="card-icon-box">
                    <div class="row">
                        @if($prop->bathroom>0)
                        <div class="col-sm-4 col-xs-4 text-left">
                            <div class="icon-holder"><figure><img src="{{ asset('frontend/images/shower-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure> <span> {{ $prop->bathroom }} </span> </div>
                        </div>
                        @endif
                        @if($prop->bedroom>0)
                        <div class="col-sm-4 col-xs-4 text-center">
                            <div class="icon-holder"><figure><img src="{{ asset('frontend/images/bed-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure> <span> {{ $prop->bedroom }}</span></div>
                        </div>
                        @endif
                        @if($prop->livingroom>0)
                        <div class="col-sm-4 col-xs-4 text-right">
                            <div class="icon-holder"><figure><img src="{{ asset('frontend/images/sofa-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure> <span>  {{ $prop->livingroom }}</span></div>
                        </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
@endforeach

@if($property->total() == 0)
    <h1 class="col-xs-12 text-center" >No property found.</h1>
@endif

@if($property->total() > $paginate)
    <div class="property-btn text-center col-xs-12 loadmore">
        <a class="theme-btn wow slow fadeIn animated" onclick="loadmore()" href="javascript:" style="visibility: visible; animation-name: fadeIn;">Load more</a>
    </div>
@endif
