<div class="col-md-8 col-md-offset-2">
    <div class="row">
        <form method="post" id="frmsignup" method="post" action="{{ route('get_in_touch') }}">
        	{{ csrf_field() }}
            <input type="hidden" name="type" value="sign-up">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Name" name="name" id="name">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email" name="email" id="email">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Telephone" name="telephone" id="telephone">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Property Address" name="property_address" id="property_address">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Number of Rooms" name="rooms" id="rooms">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default">Send</button>
                </div>
            <div class="clearfix"></div>
        </form>
    </div>
</div>



@push('scripts')

    <script type="text/javascript">
        $(function(){
            $('#frmsignup').validate({
                errorElement: 'span',
                errorClass: 'help-block',
                focusInvalid: false,
                rules: {
                    name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    telephone:{
                        required: true,
                        number: true
                    }
                },
                messages: {
                    name: {
                        required: "@lang('validation.required',['attribute'=>'Name'])"
                    },
                    email: {
                        required: "@lang('validation.required',['attribute'=>'Email'])",
                        email: "@lang('validation.email',['attribute'=>'Email'])"
                    },
                    telephone:{
                        required: "@lang('validation.required',['attribute'=>'Telephone'])",
                        number: "@lang('validation.numeric',['attribute'=>'Telephone'])",
                    }
                },
                errorClass: 'help-block',
                errorElement: 'span',
                highlight: function (element) {
                   $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                   $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "radio") {
                          error.appendTo('.a');
                    }else{
                        if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                },
                submitHandler: function(form) {  
                    if($("#frmsignup").valid()) {
                        $(form).submit();
                    }
                }
            }); 
        });
    </script>

@endpush

