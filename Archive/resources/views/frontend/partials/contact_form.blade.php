<section class="common-section signup-section bg-parallex">
    <div class="container">
        <div class="section-title wow">
            <h1>WRITE TO US</h1>
        </div>
        <div class="height-saperator"></div>
        <p>Enter your details below and let us know about the nature of your enquiry. We'll be back in touch to assist you shortly.</p>
        <div class="signup-form">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <form id="frmcontact" action="{{ route('contact_submit') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Name" id="name" name="name">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email" id="email" name="email">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Telephone" id="tel" name="tel">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="6" placeholder="Enquiry" id="enquiry" name="enquiry"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Send</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>