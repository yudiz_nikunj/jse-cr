@if (count($errors))
	
	<div class="alert alert-danger">
	
		@foreach ($errors->all() as $error)
			
			<strong>{{ $error }}</strong>

		@endforeach

	</div>

@endif