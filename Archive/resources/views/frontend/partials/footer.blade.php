<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 foot-contact"><a href="tel:"{{ Config::get('settings.footer_contact') }}>{{ Config::get('settings.footer_contact') }}</a><br><a href="mailto:{{ Config::get('settings.footer_email') }}">{{ Config::get('settings.footer_email') }}</a></div>
            <div class="col-sm-4 logo-block hidden-xs"><a href="{{ route('home') }}"><img src="{{ asset('') }}frontend/images/JSE_Logo_Trans.svg" alt="jse logo" /></a></div>
            <div class="col-sm-4 foot-address">{!! Config::get('settings.footer_address') !!}</div>
            <div class="col-sm-4 logo-block visible-xs"><a href="{{ route('home') }}"><img src="{{ asset('') }}frontend/images/JSE_Logo_Trans.svg" alt="jse logo" /></a></div>
            <div class="clearfix"></div>
        </div>
        <div class="siteinfo">
            <div class="row">
                <div class="col-sm-6 copyright-block "> <?php echo str_replace('{{YEAR}}', date('Y'), Config::get('settings.footer_text')); ?> </div>
                <div class="col-sm-6 powered-block">Designed by <a href="http://www.jcwebdesign.co" target="_blank">JC Web Design.</a></div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</footer>
