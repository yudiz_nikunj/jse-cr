@extends('frontend.layout')

@section('header')

<section class="main-banner video-banner bg-parallex" data-vide-bg="{{ asset('') }}frontend/media/video">
        
    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="login-block">
        <div class="container">
            <span>Forgot Password</span>
            <form method="post" action="{{  url('password/reset') }}" id="frmuserreset">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
                    <div class="row">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email" name="email" id="email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" id="password_confirmation">
                        </div>
                        <button type="submit" class="btn btn-default">Reset</button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
    <!--******************* Banner Section End ******************-->
</section>

@endsection


@push('scripts')

    <script type="text/javascript">
        $(function(){
            $('#frmuserreset').validate({
                errorElement: 'p',
                errorClass: 'help-block',
                focusInvalid: false,
                rules: {
                    email:{
                        required:true,
                        email:true,
                        remote: {
                            url: "{!! url('checkAvailableEmail')!!}",
                            type: "post",
                            data: {
                                _token: function() {
                                    return "{{csrf_token()}}"
                                },
                                email: function(){
                                    return $("#email").val();
                                }
                            }
                        },
                        maxlength:150,
                    },
                    password: {
                        required: true,minlength:6
                    },
                    password_confirmation:{
                        required: true,minlength:6,
                        equalTo:"#password"
                    }
                },
                messages: {
                    email: {
                        required: "@lang('validation.required',['attribute'=>'email'])",
                        remote:"@lang('validation.notexists',['attribute'=>'email'])",
                    },
                    password: {
                        required: "@lang('validation.required',['attribute'=>'new password'])",
                        minlength:"@lang('validation.min.string',['attribute'=>'new password','min'=>6])"
                    },
                    password_confirmation:{
                        required: "@lang('validation.required',['attribute'=>'confirm password'])",
                        equalTo:"@lang('validation.same',['attribute'=>'new password','other'=>'confirm password'])"
                    }
                },
                errorClass: 'help-block',
                errorElement: 'p',
                highlight: function (element) {
                   $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                   $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "radio") {
                          error.appendTo('.a');
                    }else{
                        if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                },
                submitHandler: function(form) {  
                    if($("#frmuserreset").valid()) {
                        $(form).submit();
                    }
                }
            }); 
        });
    </script>

@endpush