<?php


return [
	'required_all' => 'fill all required value.',
	'not_found' => ':entity not found.',

	'success' => 'success',

	'add' => ':entity added successfully.',
	'update' => ':entity updated successfully.',
	'delete' => ':entity deleted successfully.',
	'list' => ':entity retrived successfully.',

	'edit_profile' => 'profile updated successfully.',
	'exists' => ':entity already exists.',

	'favourite_add'=> 'Added to your favourite list',
	'favourite_remove' => 'Removed from your favourite list'
];