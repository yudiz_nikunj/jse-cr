<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use Illuminate\Support\Facades\Auth;

class PropertyController extends Controller
{
    public function index()
    {
        $properties = Property::where('user_id', Auth::id())->get();
        return view('frontend.pages.properties', compact('properties'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'address' => 'required',
            'postcode' => 'required'
        ]);

        Auth::user()->properties()->create($request->only(['postcode','address']));
        flash('Property Added Successfully.')->success();
        return redirect()->back();
    }

    public function destroy(Property $property)
    {
        if (Auth::id() == $property->user_id) {
            if ($property->invoices()->where('status', 'pending')->count()) {
                flash('Please pay pending invoices before deleting property.')->error();
                return redirect()->back();
            }

            $property->delete();
            flash('Property Deleted Successfully.')->success();
            return redirect()->back();
        }

        flash('Unauthorised user.')->error();
        return redirect()->back();
    }
}
