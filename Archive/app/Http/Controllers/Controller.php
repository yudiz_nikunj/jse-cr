<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Validator;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /* USED FOR API */
    public $response = array();
    public $status = 422;
    public $status_arr = array(
            'success' => 200,
            'unauthorized' => 412,
            'not_found' => 404,
            'already_exists' =>409,
            'validation' => 422,
        );
    /* USED FOR API */

    public function ValidateForm($fields, $rules){
    	$validator = Validator::make($fields, $rules)->validate();
    }

    public function DTFilters($request){
        $filters = array(
            'draw' => $request['draw'],
            'offset' => $request['start'],
            'limit' => $request['length'],
            'sort_column' => $request['columns'][$request['order'][0]['column']]['data'],
            'sort_order' => $request['order'][0]['dir'],
            'search' => $request['search']['value']

        );
        return $filters;
    }


    /* USED FOR API */
    public function ValidateApi($fields,$rules){
        $validate = Validator::make($fields,$rules);

        if($validate->fails()){
            $this->response['message'] = trans('api.required_all');
            $this->response['error'] = $validate->errors();
            return false;
        }

        return true;
    }

    public function validProfilePhoto($user){
        $user->profile_photo_zoom = $user->profile_photo;
        if(filter_var($user->profile_photo, FILTER_VALIDATE_URL) === FALSE){
            $user->profile_photo_zoom = checkImage(1,$user->profile_photo);
            $user->profile_photo = checkImage(6,$user->profile_photo);
        }
        return $user;
    }
    /* USED FOR API */
}
