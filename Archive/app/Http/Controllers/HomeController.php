<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail\Callback;
use App\Mail\Invoicesuccess;
use Stripe\Error\Card;
use App\Models\Invoice;
use App\Models\UserInvoice;
use App\Models\Notification;
use App\Models\Document;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Encryption\DecryptException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function dashboard()
    {
        return view('frontend.pages.dashboard');
    }

    public function invoices(Request $request)
    {
        $sort_by = $request->input('sort', 'duedate');
        $sort_order = "DESC";

        if (! in_array($sort_by, ['duedate','postcode','id','status'])) {
            $sort_by = "duedate";
        }

        if (Auth::user()->consumer_type =="management") {
            $invoices = User::select(DB::raw('invoices.*,properties.postcode,properties.address'))
            // ->join('invoices', 'users.id', '=', 'invoices.user_id')
            ->join('invoices', function ($join) {
                $join->on('users.id', '=', 'invoices.user_id');
                $join->whereRaw('invoices.deleted_at is null');
            })
            ->leftjoin('properties', 'invoices.property_id', '=', 'properties.id')
            ->where('users.id', $request->user()->id)
            ->orderBy($sort_by, $sort_order)->simplePaginate(6);
        } else {
            $invoices = User::select(DB::raw('user_invoices.*'))
                ->join('user_invoices', 'users.id', '=', 'user_invoices.user_id')
                ->where('users.id', $request->user()->id)
                ->orderBy($sort_by, $sort_order)->simplePaginate(6);
        }

        //return $invoices;
        return view('frontend.pages.invoices', compact('invoices', 'sort_by'));
    }

    public function notifications(Request $request)
    {
        $sort_by = "created_at";
        try {
            $sort_by = decrypt($request->input('sort', 'created_at'));
        } catch (DecryptException $e) {
        }

        if (! in_array($sort_by, ['created_at','property'])) {
            $sort_by = "created_at";
        }

        $sort_order = "DESC";

        $notifications = Notification::where('user_id', $request->user()->id)->orderBy($sort_by, $sort_order)->simplePaginate(6);
        $ids = $notifications->pluck('id');
        Notification::where('user_id', $request->user()->id)->whereIn('id', $ids)->update(['status' => 0]);
        return view('frontend.pages.notifications', compact('notifications', 'sort_by'));
    }

    public function documents(Request $request)
    {
        $documents = Document::whereRaw('FIND_IN_SET("'.Auth::id().'",user_id)')->latest('created_at')->simplePaginate(6);
        return view('frontend.pages.document', compact('documents'));
    }

    public function settings()
    {
        return view('frontend.pages.settings');
    }

    public function request_callback(Request $request)
    {
        Mail::to($request->user()->email)->send(new Callback($request->user()));
        flash('Callback Mail has been sent to administrator')->success();
        return redirect()->route('dashboard');
    }

    public function membership(Request $request)
    {
        if ($request->user()->subscribed('main')) {
            return view('frontend.pages.active_plan')->with('user', $request->user());
        }
        return view('frontend.pages.membership')->with('user', $request->user());
    }

    public function payment(Request $request)
    {
        $amount = ($request->package_selection == 'monthly') ? $request->user()->monthly : $request->user()->yearly;
        return view('frontend.pages.payment')->with('package', $request->package_selection)->with('amount', $amount);
    }

    public function pay(Request $request, Invoice $invoice)
    {
        if ($request->user()->id != $invoice->user_id) {
            flash("You can pay only your Invoices.")->error();
            return redirect()->back();
        }

        if ($invoice->status == "paid") {
            flash('Invoice is Already paid')->error();
            return redirect()->back();
        }

        try {
            $charge =  $request->user()->charge(($invoice->value * 100), [
                'description' =>$invoice->reference,
                'metadata' => [
                    'JSE Invoice ID' =>$invoice->id,
                    'JSE Property ID' =>$invoice->property_id,
                ],
            ]);

            if ($charge->paid) {
                $invoice->status = "paid";
                $invoice->save();

                /* Consumner Mail Send */

                Mail::to($request->user()->email)->send(new Invoicesuccess($invoice));

                /* Mail Send */

                flash('Invoice Paid Successfully')->success();
                return redirect()->back();
            }
            flash('payment failed')->error();
            return redirect()->back();
        } catch (\Stripe\Error\InvalidRequest $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        } catch (\Stripe\Error\Authentication $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        } catch (\Stripe\Error\ApiConnection $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        } catch (\Stripe\Error\Base $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        } catch (Exception $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        }
        /* catch (Card $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        }*/
    }

    public function payinvoice(Request $request, UserInvoice $invoice)
    {
        if ($request->user()->id != $invoice->user_id) {
            flash("You can pay only your Invoices.")->error();
            return redirect()->back();
        }

        if ($invoice->status == "paid") {
            flash('Invoice is Already paid')->error();
            return redirect()->back();
        }

        try {
            $charge =  $request->user()->charge(($invoice->value * 100), [
                'description' =>$invoice->reference,
                'metadata' => [
                    'JSE Invoice ID' =>$invoice->id
                ],
            ]);

            if ($charge->paid) {
                $invoice->status = "paid";
                $invoice->save();

                /* Consumner Mail Send */

                Mail::to($request->user()->email)->send(new Invoicesuccess($invoice));

                /* Mail Send */

                flash('Invoice Paid Successfully')->success();
                return redirect()->back();
            }
            flash('payment failed')->error();
            return redirect()->back();
        } catch (\Stripe\Error\InvalidRequest $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        } catch (\Stripe\Error\Authentication $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        } catch (\Stripe\Error\ApiConnection $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        } catch (\Stripe\Error\Base $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        } catch (Exception $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        }
        /* catch (Card $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        }*/
    }

    public function download(Request $request, Invoice $invoice)
    {
        $user = $request->user();
        if ($user->id != $invoice->user_id) {
            return abort(404);
        }

        $pdf = PDF::loadView('frontend.pdf.invoice', compact('user', 'invoice'));
        return $pdf->download('invoice.pdf');
    }

    public function viewPDF(Request $request, Invoice $invoice)
    {
        $user = $request->user();

        if ($user->id != $invoice->user_id) {
            return abort(404);
        }

        $pdf = PDF::loadView('frontend.pdf.invoice', compact('user', 'invoice'));
        return $pdf->stream();
    }

    public function userinvoicedownload(Request $request, UserInvoice $userinvoice)
    {
        $user = $request->user();
        if ($user->id != $userinvoice->user_id) {
            return abort(404);
        }
        $invoice = $userinvoice;
        $pdf = PDF::loadView('frontend.pdf.invoice', compact('user','invoice'));
        return $pdf->download('invoice.pdf');
    }

    public function userinvoiceviewPDF(Request $request, UserInvoice $userinvoice)
    {
        
        $user = $request->user();

        if ($user->id != $userinvoice->user_id) {
            return abort(404);
        }
        $invoice = $userinvoice;
        $pdf = PDF::loadView('frontend.pdf.invoice', compact('user','invoice'));
        return $pdf->stream();
    }
}
