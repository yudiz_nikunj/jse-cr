<?php

namespace App\Http\Controllers;

use File;
use App\User;
use App\Models\Sitesettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    public function noscript(){
        return view('errors.javascript');
    }

    public function enableCookie(){
        return view('errors.cookie_disable');
    }

    public function profile(Request $request)
    {
        $rules = [
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'phone_number' => 'required|digits_between:9,12',
            'postcode' => 'required|max:10',
            'address' => 'required',
            'city' => 'required',
        ];

        $this->validate($request,$rules);

        $consumer = Auth::user();
        $consumer->fill($request->only(['first_name','last_name','phone_number','postcode','address','city']))->save();

        flash('Profile updated successfully.')->success();

        return redirect()->back();
        
    }

    public function changeuserpass(Request $request)
    {
        $rules = [
            'opass'=>'required|min:6',
            'npass'=>'required|min:6',
            'cpass'=>'required|min:6'
        ];
        $this->validateForm($request->all(), $rules);

        if(Hash::check($request->opass, Auth::user()->password)){
            $user = Auth::user();
            $user->password = Hash::make($request->npass);
            flash('Password changed successfully.')->success();
            $user->save();
        } else {
            flash('Invalid old password.')->error();
        }
        return redirect()->route('settings');
    }

    public function check_unique_email(Request $request){
        $id = (isset($request->id) && $request->id > 0) ? $request->id : 0;
        if(User::where('email','=',$request->email)->where('id','!=',$id)->count()>0){
            return "false";
        }
        else { return "true"; }
    }

    public function checkAvailableEmail(Request $request)
    {
        if (User::where('email','=',$request->email)->where('user_type','=','user')->count() > 0) {
            return "true";
        }else { return "false"; }
    }
}




