<?php

namespace App\Http\Controllers;

use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Error\Base;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CardsController extends Controller
{
    public function __construct()
    {
        Stripe::setApiKey(config("services.stripe.secret"));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customer = \Stripe\Customer::retrieve($request->user()->stripe_id);
        /*$cards = \Stripe\Customer::retrieve($request->user()->stripe_id)->sources->all(array(
            "object" => "card",
            "limit"  => 100,
        ));*/
        return view("frontend.pages.cards", compact('customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'card_no' => 'required',
            'card_name' => 'required',
            'cvv' => 'required',
            'expiration' => 'required',
        ]);

        $customer = Customer::retrieve($request->user()->stripe_id);
        $date = explode('/', $request->expiration);
        try {
            $card = $customer->sources->create(
                array(
                    "source" => [
                        'object'    => "card",
                        "exp_month" => $date[0],
                        "exp_year"  => $date[1],
                        "number"    => $request->card_no,
                        "cvc"       => $request->cvv,
                        "name"      => $request->user()->first_name . " " . $request->user()->last_name,
                    ],
                )
            );
            flash("Card Saved Successfully")->success();
            return redirect()->back();
        } catch (Base $e) {
            Log::error("Erorr to Create Card ", ['Message'=>$e->getMessage(),'error'=>$e]);
            flash($e->getMessage())->error();
            return redirect()->back();
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            $customer = \Stripe\Customer::retrieve($request->user()->stripe_id);
            $result = $customer->sources->retrieve($id)->delete();
            if ($result->deleted) {
                return ["message"=>"Card Deleted Successfully","success"=>true];
            }
            return ["message"=>"Internal Server Error","success"=>false];
        } catch (\Stripe\Error\Base $e) {
            Log::error("Erorr to Delete Card ", ['Message'=>$e->getMessage(),'error'=>$e]);
            return ["message"=>$e->getMessage()->error(),"success"=>false];
        } catch (\Exception $e) {
            Log::error("Erorr to Delete Card ", ['Message'=>$e->getMessage(),'error'=>$e]);
            return ["message"=>$e->getMessage()->error(),"success"=>false];
        }
        return ["message"=>"Bad Request","success"=>false];
    }

    public function defaultCard(Request $request)
    {
        $this->validate($request, [
            'card_number' => 'required'
        ]);

        try {
            $customer = \Stripe\Customer::update($request->user()->stripe_id, [
            'default_source' => $request->card_number
            ]);
            flash("Deafault Card Changed Successfully")->success();
            return redirect()->back();
        } catch (\Stripe\Error\Base $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        } catch (Exception $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        }
        return redirect()->back();
    }
}
