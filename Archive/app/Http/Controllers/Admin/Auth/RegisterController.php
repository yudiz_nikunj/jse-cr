<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name' => 'required|max:50',
            'email' => 'required|max:150|unique_email',
            'password'=> 'required|min:6',
            'phone_number' => 'required|max:15|min:9',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {        
        $user = new User;
        $user->name  = $data['name'];
        $user->email  = $data['email'];
        $user->password  = bcrypt($data['password']);
        $user->phone_number  = $data['phone_number'];
        $user->user_type = "admin";
        $user->active = 0;

        if($user->save()) {
            $permissions_array = getPermissions($user->user_type);
            if(!empty($permissions_array))
                $user->roles()->attach($permissions_array);
            flash('You have successfully registered please wait until your account is active.')->success();
        }
        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails())
        {
           $this->throwValidationException(
               $request, $validator
           );
        }

        $this->create($request->all());
        return redirect()->route('register');
    }
}
