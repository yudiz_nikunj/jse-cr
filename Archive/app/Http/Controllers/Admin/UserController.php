<?php

namespace App\Http\Controllers\Admin;

use File;
use App\User;
use App\Models\Sitesettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function noscript(){
        return view('errors.javascript');
    }

    public function enableCookie(){
        return view('errors.cookie_disable');
    }

    public function showChangePassword(){
		return view('pages.change_password')->with('headTitle', 'Change Password');;
	}

    public function changePassword(Request $request){
    	$rules = [
    		'old_password'=>'required|min:6',
    		'password'=>'required|min:6',
    		'password_confirmation'=>'required|min:6'
    	];
    	$this->validateForm($request->all(), $rules);

    	if(Hash::check($request->old_password, Auth::user()->password)){
    		$user = Auth::user();
    		$user->password = Hash::make($request->password);
            flash('Password changed successfully.')->success();
    		$user->save();
    	} else {
            flash('Invalid old password.')->error();
    	}
    	return redirect()->route('changepass');
    }

    public function showProfile(){
        $user = Auth::user();     
        return view('pages.edit_profile',compact('user'))->with('headTitle', 'My Profile');
    }

    public function editProfile(Request $request){

        $user = Auth::user();
        
        $rules = [
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'required|max:150|unique_email:'.$user->id,
            'phone_number' => 'required|max:16|min:10',
            'profile_photo' => 'sometimes|image|mimes:jpeg,jpg,png',
        ];

        $this->validateForm($request->all(), $rules);

        if ($request->hasFile('profile_photo')){
            Storage::delete($user->profile_photo);
            $user->profile_photo = $request->file('profile_photo')->store('users');
        }

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone_number = unMaskPhoneNumber($request->phone_number);

        $user->save();
        flash('Profile changed successfully.')->success();
        return redirect()->route('showProfile');
    }

    public function check_unique_email(Request $request){
        $id = (isset($request->id) && $request->id > 0) ? $request->id : 0;
        if(User::where('email','=',$request->email)->where('id','!=',$id)->count()>0){
            return "false";
        }
        else { return "true"; }
    }

    public function showSetting(){
        $settings = Sitesettings::where('editable','=','y')->get();
        return view('pages.site_setting')->with(compact('settings'))->with('headTitle', 'General Settings');
    }

    public function changeSetting(Request $request){
        $array = array();
    
        foreach ($request->input() as $key => $value) {
            $setting = Sitesettings::find($key);
            if($setting){
                if ($value != "") {
                    if($setting->constant == 'app_timezone'){
                        if($value == 'MST')
                            $value = '-07:00';
                        else if($value == 'PST')
                            $value = '-08:00';
                        else if($value == 'EST')
                            $value = '-05:00';
                        else
                            $value = '-06:00';
                    }
                    $setting->value = $value;
                    $setting->save();
                    $array[$setting->constant] = $value;
                }
            }
        }
        
        //Image Uploading
        foreach ($request->file() as $key => $value) {
            $setting = Sitesettings::find($key);
            if ($request->hasFile($key))
                $file_pre = ($setting->constant == 'site_logo') ? 'logo.' : time().'.';
                $filename = $file_pre . $request->file($key)->getClientOriginalExtension();
                $request->file($key)->move(public_path('images'), $filename);
                $setting->value = $filename;
                $setting->save();
                $array[$setting->constant] = $filename;
        }

        //Non editable value
        $rem_settings = Sitesettings::where('editable','=','n')->get();
        foreach ($rem_settings as $key => $single) {
            $array[$single->constant] = $single->value;
        }

        //File writing/updating
        $str = '<?php '.PHP_EOL.'use Illuminate\Support\Facades\Config;'.PHP_EOL.PHP_EOL.'return '.var_export($array,true).';';
        file_put_contents(config_path('settings.php'), $str);
        
        flash('Settings updated successfully.')->success();        
        return redirect()->route('settings.showSetting');
    }
}
