<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Mail\Invoice as InvoiceMail;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Models\UserInvoice;
use App\Models\Notification;
use Carbon\Carbon;
use App\Notifications\PaymentReminder;

class UserInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.userinvoice.list')->with('headTitle', 'Invoices');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where(['user_type'=>'user','consumer_type'=>'sourcing'])->where('active', 1)->get();
        return view('pages.userinvoice.add')->with('headTitle', 'Invoices')->with(['users'=>$users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'reference' => 'required',
            'value' => 'required',
            'user_id' => 'required',
            'duedate' => 'required',
        ]);

        $user = User::where('id', $request->user_id)->first();

        $result = UserInvoice::create($request->only(['reference','value','duedate','user_id']));

        if ($result) {
            $notification = $user->notification()->create([
                'title' => $request->reference,
                'description' => 'new invoice generated for '.$request->reference. ' of £'.$request->value
            ]);

            $notification->duedate = $request->duedate;
            $notification->value = $request->value;
            $notification->invoice_id = $result->id;
            try{
                Mail::to($user->email)->send(new InvoiceMail($notification));

                $user->notify(new PaymentReminder($notification['description']));
            }catch(\Exception $e){

            }    

            flash('Invoice added successfully.')->success();
            return redirect()->route('userinvoice.index');
        }
        flash('Internal Server Error.')->error();
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(UserInvoice $userinvoice)
    {
        return view('pages.userinvoice.view',compact('userinvoice'))->with('headTitle', 'View User Invoices');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, UserInvoice $userinvoice)
    {
        return view('pages.userinvoice.edit',compact('userinvoice'))->with('headTitle', 'Edit User Invoices');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserInvoice $userinvoice)
    {
        $this->validate($request, [
            'status' => 'required',
        ]);

        if($userinvoice->payment_type == 'offline' && $userinvoice->status == 'pending'){
            $userinvoice->status = $request->status;
            $userinvoice->notes = $request->notes;
            $userinvoice->save();

            flash('Invoice update successfully.')->success();
            return redirect()->route('userinvoice.index');
        }else{
            flash('Internal Server Error.')->error();
            return redirect()->route('userinvoice.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $userinvoice = UserInvoice::has('user')->with(['user']);

        if ($search != '') {
            $count->where(function ($query) use ($search) {
                $query->where("reference", "like", "%{$search}%");
                $query->orWhereHas('user', function ($query) use ($search) {
                    $query->where("first_name", "like", "%{$search}%");
                    $query->orwhere("last_name", "like", "%{$search}%");
                });
                $query->orwhere("value", "like", "%{$search}%");
                $query->orwhere("status", "like", "%{$search}%");
            });
        }
        $count = $userinvoice->count();

        $records["recordsTotal"] = $count;
        $records["recordsFiltered"] = $count;
        $records['data'] = array();

        $userinvoice = $userinvoice->orderBy($sort_column, $sort_order)->offset($offset)->limit($limit)->get();

        foreach ($userinvoice as $invoice) {
            $records['data'][] = [
                'checkbox'=>view('shared.checkbox')->with('id', $invoice->id)->render(),
                'updated_at'=>$invoice->updated_at,
                'first_name'=>$invoice->user->first_name.' '.$invoice->user->last_name,
                'reference'=>$invoice->reference,
                'value'=>$invoice->value ? $invoice->value : '-',
                'status'=>$invoice->status,
                'payment_type'=>$invoice->payment_type,
                'action'=> view('pages.userinvoice.actions')->with('userinvoice', $invoice)->render()
            ];
        }
        return $records;
    }

    public function createInvoice(Request $request)
    {   
        $users = User::where(['user_type'=>'user','consumer_type'=>'sourcing'])->where('active', 1)->get();
        return view('pages.userinvoice.upload')->with('headTitle', 'Upload User Invoices')->with(['users'=>$users]);
    }
    public function uploadInvoice(Request $request)
    {   
        $this->validate($request, [
            'user_id' => 'required',
            'file'    => 'required',
        ]);

        try{
            $user = User::where('id', $request->user_id)->first();
            
            $data['path'] = $request->file('file')->store('userinvoice');
            $data['reference'] = 'upload invoice';
            $data['status'] = 'pending';
            $data['payment_type'] = 'offline';
            $data['user_id'] = $request->user_id;

            UserInvoice::create($data);


            $notification = $user->notification()->create([
                'property' => '-',
                'title' => 'upload invoice',
                'description' => 'New invoice upload'
            ]);

            flash('User Invoice Upload successfully.')->success();
            return redirect()->route('userinvoice.index');

        }catch(Exception $e){
            flash('Internal Server Error.')->error();
            return redirect()->back()->withInput();
        }    
    }
}
