<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\Property;
use Carbon\Carbon;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.property.list')->with('headTitle', 'Properties');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('user_type', 'user')->where('active', 1)->get();
        return view('pages.property.add')->with('headTitle', 'Property')->with(['users'=>$users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'address' => 'required',
            'postcode' => 'required'
        ]);

        Property::create($request->only(['postcode','address','user_id']));
        flash('Property Added Successfully.')->success();
        
        return redirect()->route('property.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Property $property)
    {
        // $this->validate($request, [
        //     'user_id' => 'required',
        //     'address' => 'required',
        //     'postcode' => 'required'
        // ]);

        // Property::create($request->only(['postcode','address','user_id']));
        // flash('Property Added Successfully.')->success();
        
        // return redirect()->route('property.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if(!empty($request->action) && $request->action == 'delete_all'){
            $content = ['status'=>204, 'message'=>"something went wrong"];
            Property::destroy(explode(',',$request->ids));
            $content['status']=200;
            $content['message'] = "Property deleted successfully.";
            return response()->json($content);
        }else{
            Property::destroy($id);
            if(request()->ajax()){
                $content = array('status'=>200, 'message'=>"Property deleted successfully.");
                return response()->json($content);
            }else{
                flash('Property deleted successfully.')->success();
                return redirect()->route('property.index');
            }
        }
    }

    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $count = Property::where('id','>',0);
        //$count = $states = State::with('country');

        if($search != ''){
            $count->where(function($query) use ($search){
                $query->where("properties.postcode", "like", "%{$search}%");
                $query->orwhere("properties.address", "like", "%{$search}%");
                $query->orWhereHas('user', function($query) use ($search){
                    $query->where("first_name", "like", "%{$search}%");
                    $query->orwhere("last_name", "like", "%{$search}%");
                });
            });
        }
        $count = $count->count();

        $records["recordsTotal"] = $count;
        $records["recordsFiltered"] = $count;
        $records['data'] = array();
        //$properties = Property::offset($offset)->limit($limit)->orderBy($sort_column,$sort_order)->with('user');
        $properties = Property::select(DB::raw('properties.*,users.first_name,users.last_name,users.id'))
                            ->join('users','properties.user_id','=','users.id')
                            ->orderBy($sort_column,$sort_order)->offset($offset)->limit($limit);

        if($search != ''){
            $properties->where(function($query) use ($search){
                $query->where("properties.postcode", "like", "%{$search}%");
                $query->orwhere("properties.address", "like", "%{$search}%");
                $query->orWhereHas('user', function($query) use ($search){
                    $query->where("first_name", "like", "%{$search}%");
                    $query->orwhere("last_name", "like", "%{$search}%");
                });
            });
        }
        $properties = $properties->get();

        foreach ($properties as $property) {
            
            $records['data'][] = [
                'checkbox'=>view('shared.checkbox')->with('id',$property->id)->render(),
                'first_name'=>$property->user->first_name.' '.$property->user->last_name,
                'postcode'=>$property->postcode,
                'address'=>$property->address,
                'action'=>view('shared.actions')->with('id', $property->id)->render(),
            ];
        }
        return $records;
    }
}
