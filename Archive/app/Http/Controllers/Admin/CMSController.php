<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Page;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;
use App\User;
use Carbon\Carbon;

class CMSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.cms.list')->with('headTitle','Manage Page');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('pages.cms.edit', ['pages' => $page])->with('headTitle','Manage Page');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $page->title = $request->title;
        $page->content = $request->content;
        $page->save();

        flash('Pages updated successfully.')->success();
        return redirect()->route('cms.edit',$page->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $count = Page::where('id','>',0);

        if($search != ''){
            $count->where(function($query) use ($search){
                $query->where("name", "like", "%{$search}%");
                $query->orwhere("title", "like", "%{$search}%");
            });
        }
        $count = $count->count();

        $records["recordsTotal"] = $count;
        $records["recordsFiltered"] = $count;
        $records['data'] = array();
        $pages = Page::offset($offset)->limit($limit)->orderBy($sort_column,$sort_order);

        if($search != ''){
            $pages->where(function($query) use ($search){
                $query->where("name", "like", "%{$search}%");
                $query->orwhere("title", "like", "%{$search}%");
            });
        }
        $pages = $pages->get();

        foreach ($pages as $page) {

            $records['data'][] = [
                'checkbox'=>view('shared.checkbox')->with('id',$page->id)->render(),
                'name'=>$page->name,
                'title'=>$page->title,
                'action'=>view('shared.actions')->with('id', $page->id)->render(),
            ];
        }

        return $records;        

    }

}
