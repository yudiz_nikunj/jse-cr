<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Mail\Invoice as InvoiceMail;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Models\Invoice;
use App\Models\Property;
use App\Models\Notification;
use Carbon\Carbon;
use App\Notifications\PaymentReminder;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.invoice.list')->with('headTitle', 'Invoices');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where(['user_type'=>'user','consumer_type'=>'management'])->where('active', 1)->get();
        return view('pages.invoice.add')->with('headTitle', 'Invoices')->with(['users'=>$users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'reference' => 'required',
            'value' => 'required',
            'property_id' => 'required',
            'user_id' => 'required',
            'duedate' => 'required',
            'payment_type' => 'required:in:online,offline',
        ]);

        $user = User::where('id', $request->user_id)->first();

        $result = $user->invoices()->create($request->only(['reference','value','property_id','duedate','payment_type']));

        if ($result) {
            $property = $user->properties()->where('id', $request->property_id)->first();

            $notification = $user->notification()->create([
                'property' => $property->postcode,
                'title' => $request->reference,
                'description' => 'new invoice generated on your property '.$property->address.' for '.$request->reference. ' of £'.$request->value
            ]);

            $notification->duedate = $request->duedate;
            $notification->value = $request->value;
            $notification->invoice_id = $result->id;
            try{ 
                Mail::to($user->email)->send(new InvoiceMail($notification));
                // dd($notification['description']);
                $user->notify(new PaymentReminder($notification['description']));
            }catch(\Exception $e){
                
            }    

            flash('Invoice added successfully.')->success();
            return redirect()->route('invoice.index');
        }
        flash('Internal Server Error.')->error();
        return redirect()->back()->withInput();
    }

    public function createInvoice(Request $request)
    {   
        $users = User::where(['user_type'=>'user','consumer_type'=>'management'])->where('active', 1)->get();
        return view('pages.invoice.upload')->with('headTitle', 'Upload Invoices')->with(['users'=>$users]);
    }
    public function uploadInvoice(Request $request)
    {   
        $this->validate($request, [
            'user_id' => 'required',
            'file'    => 'required',
        ]);

        try{
            
            $data['path'] = $request->file('file')->store('invoice');
            $data['reference'] = 'upload invoice';
            $data['status'] = 'pending';
            $data['payment_type'] = 'offline';

            // echo "<pre>";
            // print_r($data);
            // exit();

            
            $user = User::where('id', $request->user_id)->first();    
            $user->invoices()->create($data);

            $notification = $user->notification()->create([
                'property' => '-',
                'title' => 'upload invoice',
                'description' => 'New invoice upload'
            ]);
            
            flash('Invoice Upload successfully.')->success();
            return redirect()->route('invoice.index');

        }catch(Exception $e){
            flash('Internal Server Error.')->error();
            return redirect()->back()->withInput();
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        return view('pages.invoice.view',compact('invoice'))->with('headTitle', 'View Invoices');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        return view('pages.invoice.edit',compact('invoice'))->with('headTitle', 'Edit Invoices');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {

        $this->validate($request, [
            'status' => 'required',
        ]);

        if($invoice->payment_type == 'offline' && $invoice->status == 'pending'){
            $invoice->status = $request->status;
            $invoice->notes = $request->notes;
            $invoice->save();

            flash('Invoice update successfully.')->success();
            return redirect()->route('invoice.index');
        }else{
            flash('Internal Server Error.')->error();
            return redirect()->route('invoice.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $invoices = Invoice::with(['user','property']);
            if ($search != '') {
                $invoices->where(function ($query) use ($search) {
                    $query->where("reference", "like", "%{$search}%");
                    $query->orWhereHas('user', function ($query) use ($search) {
                        $query->where("first_name", "like", "%{$search}%");
                        $query->orwhere("last_name", "like", "%{$search}%");
                    });
                    $query->orWhereHas('property', function ($query) use ($search) {
                        $query->where("address", "like", "%{$search}%");
                    });
                    $query->orwhere("value", "like", "%{$search}%");
                    $query->orwhere("status", "like", "%{$search}%");
                });
            }                            
       
        $count = $invoices->count();

        $records["recordsTotal"] = $count;
        $records["recordsFiltered"] = $count;
        $records['data'] = array();
        //$invoices = Invoice::offset($offset)->limit($limit)->orderBy($sort_column, $sort_order)->with('user');
        $invoices = $invoices->orderBy($sort_column, $sort_order)->offset($offset)->limit($limit)->get();

        foreach ($invoices as $invoice) {
            $records['data'][] = [
                'checkbox'=>view('shared.checkbox')->with('id', $invoice->id)->render(),
                'updated_at'=>$invoice->updated_at,
                'first_name'=>$invoice->user->first_name.' '.$invoice->user->last_name,
                'address'=>$invoice->property ? $invoice->property->address : '-',
                'reference'=>$invoice->reference,
                'value'=>$invoice->value ? $invoice->value : '-',
                'status'=>$invoice->status,
                'payment_type'=>$invoice->payment_type,
                'action'=> view('pages.invoice.actions')->with('invoice', $invoice)->render()

            ];
        }
        return $records;
    }

    public function getuserdata(Request $request)
    {
        $option = "<option value=''>Select Property</option>";
        if ($request->id) {
            $properties = Property::where('user_id', $request->id)->get();
            foreach ($properties as $property) {
                $option .= '<option value="'.$property->id.'">'.$property->address.'</option>';
            }
        }
        return array('data' => $option);
    }
}
