<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Document;
use App\User;
use Illuminate\Http\Request;
use Storage;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.document.list')->with('headTitle', 'Documents');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('user_type', 'user')->where('active', 1)->get();
        return view('pages.document.add',compact('users'))->with('headTitle', 'Document');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'user_id' => 'required',
            'document' => 'sometimes|mimes:jpg,jpeg,bmp,png,doc,docx,csv,rtf,xlsx,xls,txt,pdf,zip',
        ];
        $this->validateForm($request->all(), $rules);
        $user_id = null;
        if(!empty($request->user_id)){
            $user_id = implode(',',$request->user_id);
        }
        $document = new Document;
        $document->user_id = $user_id ? $user_id : null;
        $document->title = $request->title;
        $document->path = $request->file('document')->store('document');
        $document->size = $request->file('document')->getSize();
        $document->save();
        
        flash('Document added successfully.')->success();
        return redirect()->route('document.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        return view('pages.document.view',compact('document'))->with('headTitle', 'View Documents');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        $users = User::where('user_type', 'user')->where('active', 1)->get();
        return view('pages.document.edit',compact('document','users'))->with('headTitle', 'Edit Documents');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (!empty($request->action) && $request->action == 'change_status') {
            $content  = ['status' => 204, 'message' => "something went wrong"];
            $doc = Document::find($id);
            if ($doc) {
                $doc->active = $request->value;
                if ($doc->save()) {
                    $content['status']  = 200;
                    $content['message'] = "Status updated successfully.";
                }
            }
            return response()->json($content);
        } else {
            $rules = [
                'title'    => 'required',
                'user_id'    => 'required',

                'document' => 'sometimes|mimes:jpg,jpeg,bmp,png,doc,docx,csv,rtf,xlsx,xls,txt,pdf,zip',
            ];
            $this->validateForm($request->all(), $rules);

            $document = Document::find($id);

            if ($request->hasFile('document')) {
                Storage::delete($document->path);
                $document->path = $request->file('document')->store('document');
                $document->size = $request->file('document')->getSize();
            }

            if ($document) {
                $user_id = null;
                if(!empty($request->user_id)){
                    $user_id = implode(',',$request->user_id);
                }
                $document->user_id = $user_id ? $user_id : null;
                $document->title = $request->title;
                $document->save();

                flash('Document updated successfully.')->success();
                return redirect()->route('document.index');
            } else {
                abort(404);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!empty($request->action) && $request->action == 'delete_all') {
            $content = ['status' => 204, 'message' => "something went wrong"];
            
            $documents = Document::whereIn('id',explode(',',$request->ids))->get();

            foreach ($documents as $key => $doc) {
                Storage::delete($doc->path);
                $doc->delete();
            }
            
            $content['status']  = 200;
            $content['message'] = "Documents deleted successfully.";
            return response()->json($content);
        } else {
            $document = Document::find($id);
            Storage::delete($document->path);
            $document->delete();

            if (request()->ajax()) {
                $content = array('status' => 200, 'message' => "Document deleted successfully.");
                return response()->json($content);
            } else {
                flash('Document deleted successfully.')->success();
                return redirect()->route('document.index');
            }
        }
    }

    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $documents = Document::where('id','<>',0);

            if ($search != '') {
                $documents->where(function ($query) use ($search) {
                    $query->where("reference", "like", "%{$search}%");
                });
            }                            
       
        $count = $documents->count();

        $records["recordsTotal"] = $count;
        $records["recordsFiltered"] = $count;
        $records['data'] = array();
        $documents = $documents->orderBy($sort_column, $sort_order)->offset($offset)->limit($limit)->get();

        foreach ($documents as $doc) {

            $params = array(
                'url'       => route('document.update', $doc->id),
                'checked'   => ($doc->active == 1) ? "checked" : "",
                'getaction' => '',
                'class'     => '',
            );

            $records['data'][] = [
                'checkbox'=>view('shared.checkbox')->with('id', $doc->id)->render(),
                'title'=>$doc->title,
                'size'=>formatBytes($doc->size),
                'active' => view('shared.switch')->with(['params' => $params, 'id' => $doc->id])->render(),
                'action'=>view('shared.actions')->with('id', $doc->id)->render(),
            ];
        }
        return $records;
    }
}
