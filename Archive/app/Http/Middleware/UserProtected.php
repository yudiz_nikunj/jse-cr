<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

class UserProtected
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check() && in_array($request->path(), ['dashboard','properties','invoices','notifications','settings','membership','documents'])) {
            return redirect('/login');
        }

        if (Auth::check()) {
            if ($request->user()->active == 0) {
                flash('Your Account is inactive.')->error();
                Auth::logout();
                return redirect('/login');
            }
        }

        if (Auth::check() && Auth::user()->isAdmin()) {
            return redirect('admin/dashboard');
        }

        return $next($request);
    }
}
