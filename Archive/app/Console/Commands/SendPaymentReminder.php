<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Invoice;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\PaymentReminder;

class SendPaymentReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Due Payemnt Reminder Email and SMS to Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $invoices = Invoice::with(['user', 'property'])
                    ->whereDate('duedate', Carbon::today()->addDays(7))
                    ->where('status', "=", "pending")
                    ->get();

        foreach ($invoices as $invoice) {
            $message = "Your Last date for Payment of £" . $invoice->value . " is " . $invoice->duedate->format("d-m-Y") . ".";
            $message .= " For " . $invoice->reference . " On Your Property " . $invoice->property->address;
            try {
                $invoice->user->notify(new PaymentReminder($message));
                $this->info($message);
                Log::info("Payment Reminder Sent to ".$invoice->user->first_name." ".$invoice->user->last_name, ['User'=>$invoice->user->toArray()]);
                $this->info("Payment Reminder Sent Successfully to ".$invoice->user->first_name." ".$invoice->user->last_name);
            } catch (\Exception $e) {
                Log::error("Failed to Send Payment Reminder to ".$invoice->user->first_name." ".$invoice->user->last_name, ['Message'=>$e->getMessage(),'error'=>$e]);
                $this->info("Can't Send Payment Reminder to ".$invoice->user->first_name." ".$invoice->user->last_name);
            }
        }
    }
}
