<?php

use App\Models\NewProperty;
use Illuminate\Support\Str;

function checkImage($id = 0, $file = '', $zc = 0, $ql = 100)
{
    $base_url = '/public/';
    //$base_url = '/public/';
    $src = $base_url . 'images/no_image_available.jpg';

    $q = DB::table('imagethumb')->where('id', '=', $id);
    if ($q->count() > 0) {
        $res = $q->first();
        //if(is_file(public_path('storage/'.$file))){
        if (file_exists(public_path('storage/' . $file))) {
            $src = $base_url . 'storage/' . $file;
        } else {
            if (is_file(public_path($res->default_image))) {
                $src = $base_url . $res->default_image;
            }
        }

        //echo 'sf'.env('APP_NAME');exit;
        $src = env('APP_URL') . "/image-thumb.php?w=" . $res->width . "&h=" . $res->height . "&zc=" . $zc . "&q=" . $ql . "&src=" . $src;
    }
    return $src;
}

function getPermissions($user_type = '')
{
    $permissions = array();

    if ($user_type != '') {

        if ($user_type == 'admin') {
            $permissions = [
                1 => [
                    'permissions' => 'access',
                ],
                2 => [
                    'permissions' => 'access,edit',
                ],
                3 => [
                    'permissions' => 'access,delete',
                ],
                4 => [
                    'permissions' => 'access,delete',
                ],
            ];

        }
    }
    return $permissions;
}

function unMaskPhoneNumber($string = '')
{
    if ($string != '') {
        preg_match_all('!\d+!', $string, $match);
        $string = implode('', $match[0]);
    }
    return $string;
}

function formatBytes($size, $precision = 2)
{
    if ($size > 0) {
        $size = (int) $size;
        $base = log($size) / log(1024);
        $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');

        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    } else {
        return $size;
    }
}

function createSlug($title){
    $slug = Str::slug($title, '-');
    $property = NewProperty::where('slug',$slug)->first();
    if(isset($property->id)){
        return $slug.'-'.rand(1111,9999);
    }else{
        return $slug;
    }
}

/*function formatPhoneNumber($number = '')
{
    if ($number != '') {
        return "(" . substr($number, 0, 5) . ") " . substr($number, 6, 6);
    } else {
        return 'NA';
    }
}*/


function getDeviceName(){

    $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
    $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
    $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
    
    if($iPod  ||  $iPhone || $iPad){
        return 'safari';
    }
    return '';
    
}