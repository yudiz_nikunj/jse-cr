<?php

namespace App;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable,SoftDeletes,Billable;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','trial_ends_at'];
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','user_type','consumer_type', 'email', 'password','profile_photo','phone_number','city','address','postcode','monthly','yearly'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','deleted_at','user_type'
    ];


    public function isAdmin()
    {
        return $this->user_type == "admin";
    }

    /**
     * check if Logged in User's type is User or Not
     * @return boolean
     */
    public function routeNotificationFor($driver, $notification = null)
    {
        if (method_exists($this, $method = 'routeNotificationFor'.Str::studly($driver))) {
            return $this->{$method}($notification);
        }

        switch ($driver) {
            case 'database':
                return $this->notifications();
            case 'mail':
                return $this->email;
            case 'nexmo':
                return $this->phone_number;
            case 'unifonic':
                return $this->phone_number;
        }

    }
    public function isUser()
    {
        return $this->user_type == "user";
    }

    public function roles()
    {
        return $this->belongsToMany(App\Models\Role::class)->withPivot('permissions')->withTimeStamps();
    }

    public function hasPermission($routeName, $permission)
    {
        $isPermitted = Role::where('route', 'like', $routeName.'%')->whereHas('permissions', function ($query) use ($permission) {
            $query->where('user_id', Auth::id())->whereRaw('find_in_set("'.$permission.'", permissions)');
        })->first();
        if (!$isPermitted) {
            return false;
        } else {
            return true;
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\PasswordReset($token, $this->user_type));
    }

    public function properties()
    {
        return $this->hasMany(\App\Models\Property::class);
    }

    public function invoices()
    {
        return $this->hasMany(\App\Models\Invoice::class);
    }

    public function userinvoices()
    {
        return $this->hasMany(\App\Models\UserInvoice::class);
    }

    public function notification()
    {
        return $this->hasMany(\App\Models\Notification::class);
    }

    public function delete()
    {
        $this->properties()->delete();
        $this->invoices()->delete();
        parent::delete();
    }
}



