<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sitesettings extends Model
{
    protected $table = 'site_settings';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label', 'type', 'constant', 'options', 'class', 'required', 'value', 'hint', 'editable'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
