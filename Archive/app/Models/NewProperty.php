<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class NewProperty extends Model
{
   
    
    protected $fillable = ['name','price','bedroom','bathroom','livingroom','location','property_type','propery_description','latitude','longtitude','address','active','slug'];

    protected $table ='new_properties';

    public function images(){
    	return $this->hasMany(NewPropertyImage::class,'property_id','id');
    }

    public function amenities(){
    	return $this->belongsToMany(Amenities::class, 'new_properties_amenities','property_id','amenity_id');
    	
    }

    public function propertyimg(){
    	$img = $this->images->first();
    	if(isset($img->name) && Storage::exists($img->name)){
    		return Storage::url($img->name);
    	}else{
    		return asset('images/no-property-image.png');
    	}
    }

    

}
