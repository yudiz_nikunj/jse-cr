<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInvoice extends Model
{
    use SoftDeletes;
    
    protected $table= "user_invoices";

    protected $fillable = ['reference', 'value', 'status', 'created_at', 'duedate', 'user_id', 'payment_type', 'notes', 'path'];

    protected $dates = ['duedate'];


    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function setDueDateAttribute($value)
    {
        $this->attributes['duedate'] = \Carbon\Carbon::parse($value);
    }

    public function isUploadInvoice()
    {
        if(!empty($this->path)){
            return true;
        }else{
            return false;
        }
    }
}
