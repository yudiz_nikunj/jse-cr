<?php

Route::group(['prefix' => 'admin',  'namespace' => 'Admin'], function () {
    Auth::routes();
    Route::get('', 'Auth\LoginController@showLoginForm');
});

/* Required auth and permission middleware */
Route::group(['middleware' => ['auth', 'permission'],'prefix'=>'admin','namespace'=>"Admin"], function () {

    /*General settings */
    Route::get('settings', 'UserController@showSetting')->name('settings.showSetting');
    Route::post('change-setting', 'UserController@changeSetting')->name('settings.changesetting');
    Route::get('dashboard', 'HomeController@index')->name('home.index');

    /* CMS */
    Route::get('cms/listing', 'CMSController@listing')->name('cms.listing');
    Route::get('cms', 'CMSController@index')->name('cms.index');
    Route::get('cms/{page}/edit', 'CMSController@edit')->name('cms.edit');
    Route::put('cms/{page}/edit', 'CMSController@update')->name('cms.update');

    Route::get('consumer/listing', 'ConsumerController@listing')->name('consumer.listing');
    Route::resource('consumer', 'ConsumerController');

    Route::get('enquiry/listing', 'EnquiryController@listing')->name('enquiry.listing');
    Route::resource('enquiry', 'EnquiryController');

    Route::get('property/listing', 'PropertyController@listing')->name('property.listing');
    Route::resource('property', 'PropertyController');

    Route::get('invoice/upload', 'InvoiceController@createInvoice')->name('invoice.createinvoice');
    Route::post('invoice/upload', 'InvoiceController@uploadInvoice')->name('invoice.uploadinvoice');
    Route::get('invoice/listing', 'InvoiceController@listing')->name('invoice.listing');
    Route::resource('invoice', 'InvoiceController');

    Route::get('userinvoice/upload', 'UserInvoiceController@createInvoice')->name('userinvoice.createinvoice');
    Route::post('userinvoice/upload', 'UserInvoiceController@uploadInvoice')->name('userinvoice.uploadinvoice');
    Route::get('userinvoice/listing', 'UserInvoiceController@listing')->name('userinvoice.listing');
    Route::resource('userinvoice', 'UserInvoiceController');

    Route::get('document/listing', 'DocumentController@listing')->name('document.listing');
    Route::resource('document', 'DocumentController');

    Route::post('newproperty-uploadimage', 'NewPropertyController@uploadItemImage')->name('new-property.uploadimage');
    Route::post('newproperty-removeimage', 'NewPropertyController@removeItemImage')->name('new-property.removeimage');
    Route::get('newproperty-getimage/{property_id}', 'NewPropertyController@getItemImage')->name('new-property.getimage');
    Route::get('new-property/listing', 'NewPropertyController@listing')->name('new-property.listing');
    Route::resource('new-property', 'NewPropertyController');
});


/* Unique E-mail */
Route::post('checkUniqueEmail', 'UserController@check_unique_email');

/* Required auth middleware */
Route::group(['middleware' => 'auth', 'prefix' => 'admin' ,'namespace'=> "Admin"], function () {
    Route::get('change-password', 'UserController@showChangePassword')->name('showChangePass');
    Route::post('change-password', 'UserController@changePassword')->name('changepass');

    Route::get('myprofile', 'UserController@showProfile')->name('showProfile');
    Route::post('myprofile', 'UserController@editProfile')->name('editProfile');

    Route::get('logout', 'Auth\LoginController@logout')->name('admin_logout');

    Route::post('consumer/notificationForm', 'ConsumerController@notificationForm')->name('notificationForm');

    Route::post('getuserdata', 'InvoiceController@getuserdata')->name('getuserdata');
});
