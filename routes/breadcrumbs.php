<?php

// Dashboard
Breadcrumbs::register('dashboard', function($breadcrumbs)
{
    $breadcrumbs->push('Dashboard', route('home.index'));
});

// Edit Profile
Breadcrumbs::register('my_profile', function($breadcrumbs)
{
	$breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Profile', route('showProfile'));
});

// Change Password
Breadcrumbs::register('change_pass', function($breadcrumbs)
{
	$breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Change Password', route('showChangePass'));
});

// General settings
Breadcrumbs::register('general_setting', function($breadcrumbs)
{
	$breadcrumbs->parent('dashboard');
    $breadcrumbs->push('General Settings', route('settings.showSetting'));
});

/* CMS Breadcrumbs Starts (List, Edit) */
Breadcrumbs::register('cms', function($breadcrumbs)
{
	$breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Pages', route('cms.index'));
});
Breadcrumbs::register('edit_cms', function($breadcrumbs)
{
    $breadcrumbs->parent('cms');
    $breadcrumbs->push('Edit Pages', route('cms.index'));
});
/* CMS Breadcrumbs Ends */

/* Consumers Breadcrumbs Starts (List,View, Delete, Edit) */
Breadcrumbs::register('consumer', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Consumer', route('consumer.index'));
});
Breadcrumbs::register('add_consumer', function($breadcrumbs)
{
    $breadcrumbs->parent('consumer');
    $breadcrumbs->push('Add Consumer', route('consumer.create'));
});
Breadcrumbs::register('edit_consumer', function($breadcrumbs, $consumer)
{
    $breadcrumbs->parent('consumer');
    $breadcrumbs->push('Edit Consumer', route('consumer.edit', $consumer->id));
});
/* Consumers Breadcrumbs Ends */

/* Enquiry Breadcrumbs Starts (List, Delete) */
Breadcrumbs::register('enquiry', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Enquiry', route('enquiry.index'));
});
/* Enquiry Breadcrumbs Ends */

/* Property Breadcrumbs Starts (List, Delete) */
Breadcrumbs::register('property', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Property', route('property.index'));
});

Breadcrumbs::register('add_property', function($breadcrumbs)
{
    $breadcrumbs->parent('property');
    $breadcrumbs->push('Add Property', route('property.create'));
});

Breadcrumbs::register('edit_property', function($breadcrumbs, $property)
{
    $breadcrumbs->parent('property');
    $breadcrumbs->push('Edit Property', route('property.edit', $property->id));
});
/* Property Breadcrumbs Ends */

/* Invoice Breadcrumbs Starts (List, Delete) */

Breadcrumbs::register('invoice', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Invoice', route('invoice.index'));
});

Breadcrumbs::register('add_invoice', function($breadcrumbs)
{
    $breadcrumbs->parent('invoice');
    $breadcrumbs->push('Add Invoice', route('invoice.index'));
});

Breadcrumbs::register('edit_invoice', function($breadcrumbs)
{
    $breadcrumbs->parent('invoice');
    $breadcrumbs->push('Edit Invoice', route('invoice.index'));
});

Breadcrumbs::register('view_invoice', function($breadcrumbs)
{
    $breadcrumbs->parent('invoice');
    $breadcrumbs->push('View Invoice', route('invoice.index'));
});

Breadcrumbs::register('upload_invoice', function($breadcrumbs)
{
    $breadcrumbs->parent('invoice');
    $breadcrumbs->push('Upload Invoice', route('invoice.index'));
});
/* Invoice Breadcrumbs Ends */


/* user Invoice Breadcrumbs Starts (List, Delete) */

Breadcrumbs::register('userinvoice', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('User Invoice', route('userinvoice.index'));
});

Breadcrumbs::register('add_userinvoice', function($breadcrumbs)
{
    $breadcrumbs->parent('userinvoice');
    $breadcrumbs->push('Add User Invoice', route('userinvoice.index'));
});

Breadcrumbs::register('edit_userinvoice', function($breadcrumbs)
{
    $breadcrumbs->parent('userinvoice');
    $breadcrumbs->push('Edit User Invoice', route('userinvoice.index'));
});

Breadcrumbs::register('view_userinvoice', function($breadcrumbs)
{
    $breadcrumbs->parent('userinvoice');
    $breadcrumbs->push('View User Invoice', route('userinvoice.index'));
});

Breadcrumbs::register('upload_userinvoice', function($breadcrumbs)
{
    $breadcrumbs->parent('userinvoice');
    $breadcrumbs->push('Upload User Invoice', route('userinvoice.index'));
});
/* Invoice Breadcrumbs Ends */


Breadcrumbs::register('document', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Document', route('document.index'));
});

Breadcrumbs::register('add_document', function($breadcrumbs)
{
    $breadcrumbs->parent('document');
    $breadcrumbs->push('Add Document', route('document.index'));
});

Breadcrumbs::register('edit_document', function($breadcrumbs)
{
    $breadcrumbs->parent('document');
    $breadcrumbs->push('Edit Document', route('document.index'));
});

Breadcrumbs::register('view_document', function($breadcrumbs)
{
    $breadcrumbs->parent('document');
    $breadcrumbs->push('View Document', route('document.index'));
});


Breadcrumbs::register('new_property', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('New Property', route('new-property.index'));
});

Breadcrumbs::register('add_new_property', function($breadcrumbs)
{
    $breadcrumbs->parent('new_property');
    $breadcrumbs->push('Add New Property', route('new-property.index'));
});

Breadcrumbs::register('edit_new_property', function($breadcrumbs)
{
    $breadcrumbs->parent('new_property');
    $breadcrumbs->push('Edit New Property', route('new-property.index'));
});

Breadcrumbs::register('view_new_property', function($breadcrumbs)
{
    $breadcrumbs->parent('new_property');
    $breadcrumbs->push('View New Property', route('new-property.index'));
});



