<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

//use App\Notifications\PaymentReminder;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


Route::get('nocookie', 'UserController@enableCookie')->name('nocookie');
Route::get('noscript', 'UserController@noscript')->name('noscript');

// Webhook Route For Stripe Payment
Route::post(
    'stripe/webhook',
    '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook'
);

// Front End Routes
Auth::routes();

Route::get('/', 'PagesController@index')->name('home');
Route::get('new-property', 'PagesController@newproperty')->name('newproperty');
// Route::get('property-detail-page', 'PagesController@propertydetailpage')->name('property-detail-page');
Route::get('property-detail/{slug}', 'PagesController@propertydetailpage')->name('property-detail');
Route::get('about', 'PagesController@about')->name('about');
Route::get('service-overview', 'PagesController@serviceOverview')->name('overview');
Route::get('coverage', 'PagesController@coverage')->name('coverage');
Route::get('management', 'PagesController@management')->name('management');
Route::get('development', 'PagesController@development')->name('development');
Route::get('sourcing', 'PagesController@sourcing')->name('sourcing');
Route::get('contact', 'PagesController@contact')->name('contact');
Route::post('get_in_touch', 'PagesController@enquiry')->name('get_in_touch');
Route::post('property-inquiry', 'PagesController@propertyinquiry')->name('propertyinquiry');
Route::post('checkAvailableEmail', 'UserController@checkAvailableEmail');

// User Protected Routes (Portal Routes)
Route::group(['middleware' => ['user.protected','stripe.active'] ], function () {
    Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::get('properties', 'PropertyController@index')->name('properties');
    Route::post('properties', 'PropertyController@store');
    Route::get('properties/{property}', 'PropertyController@destroy');
    Route::get('invoices', 'HomeController@invoices')->name('invoices');
    
    Route::get('notifications', 'HomeController@notifications')->name('notifications');
    Route::get('settings', 'HomeController@settings')->name('settings');
    Route::post('settings', 'UserController@profile');
    Route::post('changeuserpass', 'UserController@changeuserpass')->name('changeuserpass');
    Route::get('request_callback', 'HomeController@request_callback')->name('request_callback');

    Route::get('invoices/{invoice}/pay', 'HomeController@pay')->name('invoice.pay')->where(['invoice' => '[0-9]+']);
    Route::get('invoices/{invoice}/download', 'HomeController@download')->name('download_invoice')->where(['invoice' => '[0-9]+']);
    Route::get('invoices/{invoice}/view', 'HomeController@viewPDF')->name('view_invoice')->where(['invoice' => '[0-9]+']);

    Route::get('userinvoices/{invoice}/pay', 'HomeController@payinvoice')->name('userinvoice.pay')->where(['invoice' => '[0-9]+']);
    Route::get('userinvoices/{userinvoice}/download', 'HomeController@userinvoicedownload')->name('sourcing_download_invoice')->where(['invoice' => '[0-9]+']);
    Route::get('userinvoices/{userinvoice}/view', 'HomeController@userinvoiceviewPDF')->name('sourcing_view_invoice')->where(['invoice' => '[0-9]+']);

    Route::get('documents', 'HomeController@documents')->name('documents');
});

Route::group(['middleware' => ['user.protected'] ], function () {
    /*Route::get('test', function (Request $request) {
        $request->user()->notify(new PaymentReminder());
    });*/

    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('membership', 'HomeController@membership')->name('membership');
    Route::post('membership', 'HomeController@payment');
    Route::post('subscribe', 'StripeController@subscribe')->name('subscribe');

    Route::get('cards', 'CardsController@index')->name('cards');
    Route::post('cards', 'CardsController@store');
    Route::delete('cards/{id}', 'CardsController@destroy');
    Route::post('cards/default', 'CardsController@defaultCard')->name('cards.default');
});

Route::get('sms', function () {
    //dd($schedule);
    // $result = $schedule->command('reminder:send')->emailOutputTo('viral@ping2world.com');
    return Artisan::call('reminder:send');
});

// Backend Routes are stored in admin.php file
require_once 'admin.php';