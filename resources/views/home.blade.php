@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('dashboard') !!}
@endsection

@section('content')
<div class="row margin-top-10">
    <div class="row widget-row">

      <div class="col-md-3">
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <a class="dashboard-stat-v2" href="{{ route('consumer.index') }}">
              <h4 class="widget-thumb-heading">Total Consumers</h4>
              <div class="widget-thumb-wrap">
                  <i class="widget-thumb-icon bg-green fa fa-users"></i>
                  <div class="widget-thumb-body">
                      <span class="widget-thumb-subtitle"></span>
                      <span class="widget-thumb-body-stat" data-counter="counterup" >{{ $counts['consumer'] }}</span>
                  </div>
              </div>
            </a>
        </div>
      </div>

      <div class="col-md-3">
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <a class="dashboard-stat-v2" href="{{ route('invoice.index') }}">
              <h4 class="widget-thumb-heading">Total Invoices</h4>
              <div class="widget-thumb-wrap">
                  <i class="widget-thumb-icon bg-red icon-calculator"></i>
                  <div class="widget-thumb-body">
                      <span class="widget-thumb-subtitle"></span>
                      <span class="widget-thumb-body-stat" data-counter="counterup" >{{ $counts['invoice'] }}</span>
                  </div>
              </div>
            </a>
        </div>
      </div>

      <div class="col-md-3">
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <a class="dashboard-stat-v2" href="{{ route('enquiry.index') }}">
              <h4 class="widget-thumb-heading">Total Enquiries</h4>
              <div class="widget-thumb-wrap">
                  <i class="widget-thumb-icon bg-blue fa fa-phone"></i>
                  <div class="widget-thumb-body">
                      <span class="widget-thumb-subtitle"></span>
                      <span class="widget-thumb-body-stat" data-counter="counterup" >{{ $counts['enquiry'] }}</span>
                  </div>
              </div>
            </a>
        </div>
      </div>

    <div class="col-md-3">
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <a class="dashboard-stat-v2" href="{{ route('property.index') }}">
              <h4 class="widget-thumb-heading">Total Property</h4>
              <div class="widget-thumb-wrap">
                  <i class="widget-thumb-icon bg-blue fa fa-building-o"></i>
                  <div class="widget-thumb-body">
                      <span class="widget-thumb-subtitle"></span>
                      <span class="widget-thumb-body-stat" data-counter="counterup" >{{ $counts['property'] }}</span>
                  </div>
              </div>
            </a>
        </div>
    </div>

    </div>
</div>
@endsection
