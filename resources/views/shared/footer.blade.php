<div class="page-footer">
    <div class="page-footer-inner"> 
    	<?php echo str_replace('{{YEAR}}', date('Y'),Config::get('settings.footer_text')); ?>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>