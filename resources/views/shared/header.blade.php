<div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{ route('home.index') }}" style="text-decoration: none;">
                <h4 style="font-size: 40px;" id="logo_name"><img src="{{ asset('images/logo_black.png') }}" alt="logo" class="logo-default logo-header" alt="JSE" /></h4>
                {{-- <img src="{{ asset('images/'.Config::get('settings.site_logo_after')) }}" alt="logo" class="logo-default logo-header" /> --}}
            </a>
            <div class="menu-toggler sidebar-toggler" id="toggle">
                @push('scripts')
                    <script type="text/javascript">
                        $("#toggle").click(function(){
                            $("#logo_name").toggle();
                        });
                    </script>
                @endpush
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-on-mobile"> {{ Auth::user()->name }} </span>
                            <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                            <img alt="" class="img-circle" src="{{ !empty(Auth::user()->profile_photo) ? asset('storage/'.Auth::user()->profile_photo) : asset('images/default_profile.jpg') }}" />
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{ route('showProfile') }}">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>
                            <li>
                                <a href="{{ route('showChangePass') }}">
                                    <i class="icon-lock"></i> Change Password
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin_logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                            <form id="logout-form" action="{{ route('admin_logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
