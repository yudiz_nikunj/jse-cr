@if (in_array('view', $permissions) || in_array('edit', $permissions) || in_array('delete', $permissions))
	@if (in_array('view', $permissions))
		<a href="{{ route($routeName.'.show', $id) }}" title="View" class="btn btn-success btn-xs">View</a>
	@endif
	@if (in_array('edit', $permissions))
		<a href="{{ route($routeName.'.edit', $id) }}" title="Edit" class="btn btn-warning btn-xs">Edit</a>
	@endif
	@if (in_array('delete', $permissions))
		<a title="Delete" href="{{ route($routeName.'.destroy', $id) }}" class="btn btn-danger btn-xs act-delete" data-id="{{$id}}">Delete</a>
	@endif
	@if (in_array('edit', $permissions) && $routeName == 'consumer')
		<a title="Send PushNotification" href="javascript:;" data-id="{{ $id }}" class="btn purple btn-xs send-push" data-id="{{$id}}">Send Notification</a>
	@endif
@endif