<?php  
    
    $required = '';
    if($setting->required=='y'){$required = "required ";} 

    switch($setting->type){

        case 'text':
        case 'password':?>

            <div class="form-group">
                <label for="{{$setting->id}}" class="col-md-2 control-label">{!! $mend_sign !!}{{$setting->label}}</label>
                <div class="col-md-4">
                    <div class="input-icon">
                        <i class="{{$setting->icon}}"></i>
                        <input type="{{$setting->type}}" class="form-control {{$required}} {{$setting->class}}" name="{{$setting->id}}" id="{{$setting->id}}" value="{{$setting->value}}" placeholder="Enter {{$setting->label}}">  
                    </div>
                </div>
            </div>
            
        <?php break;

        case 'textarea':?>

            <div class="form-group">
                <label for="{{$setting->id}}" class="col-md-2 control-label">{{$setting->label}}</label>
                <div class="col-md-4">
                    <div class="input-icon">
                        <i class="{{$setting->icon}}"></i>
                        <textarea placeholder="Enter {{$setting->label}}" class="form-control {{$required}} name="{{$setting->id}}" id="{{$setting->id}}">{{$setting->value}}</textarea>
                    </div>
                </div>
            </div>

        <?php break;

        case 'radio':
            
            $options = explode(',',$setting->options);?>
            
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label" for="form_control_1" style="color: #333;">{!! $mend_sign !!}{{$setting->label}}</label>
                <div class="col-md-10">
                    <div class="md-radio-inline">
                        @foreach($options as $key=>$value)
                        <div class="md-radio">
                            <input type="radio" <?php echo $value == $setting->value ? 'checked' : '' ?> id="radio53_{{$key}}" data-error-container="#privacy_error_{{$setting->id}}" value="{{$value}}" name="{{$setting->id}}" class="md-radiobtn {{$required}}">
                            <label for="radio53_{{$key}}">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span> {{$value}} </label>
                        </div>
                        @endforeach
                        <span id="privacy_error_{{$setting->id}}"></span>
                    </div>
                </div>
            </div>

                    
        <?php break;

        case 'file':?>
            
            <div class="form-group">
                <label class="col-md-2 control-label" for="{{$setting->id}}">{{$setting->label}}</label>
                <div class="col-md-4">
                    <div class="input-icon">
                        <i class="{{$setting->icon}}"></i>
                        <input type="{{$setting->type}}" name="{{$setting->id}}" id="{{$setting->id}}" class="form-control {{$required}} {{$setting->class}}">
                        <div class="clearfix margin-top-10"></div>
                        <img src="{{env('APP_URL').'/public/images/'.$setting->value}}" />
                    </div>
                </div>
            </div>  

        <?php break;
    }

?>