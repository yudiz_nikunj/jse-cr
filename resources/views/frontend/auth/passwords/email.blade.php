@extends('frontend.layout')

@section('header')

<section class="main-banner video-banner bg-parallex" data-vide-bg="{{ asset('') }}frontend/media/video">

    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="login-block">
        <div class="container">
            <span>Forget Password ?</span>
            <span><p style="color: #B8860B;">Enter your e-mail address below to reset your password. </p></span>
            {{-- @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif --}}
            <form class="forget-form" role="form" method="POST" action="{{ url('password/email') }}" id="frmsendemail">
                {{ csrf_field() }}
                <div class="col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
                    <div class="row">
                        <div class="form-group">
                            <input type="email" class="form-control" value="{{ old('email') }}" placeholder="E-mail address" name="email" id="email">
                            {{-- @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif --}}
                        </div>
                        
                        <a href="{{ url('login') }}" class="btn red btn-outline back-btn-change">Back to login</a>
                        <button type="submit" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--******************* Banner Section End ******************-->
</section>

@endsection


@push('scripts')
<script type="text/javascript">
$(document).ready(function() {

    $("#frmsendemail").validate({
        rules: {
            email: {
                required: true,
                pattern: /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
                email: true
            }
        },
        messages: {
            email: {
                required: "Email is required.",
                email: "Enter valid email address.",
                pattern: "Enter valid email address."
            }
        },
        errorClass: 'help-block',
        errorElement: 'p',
        highlight: function (element) {
           $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
           $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                  error.appendTo('.a');
            }else{
                if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            }
        }
    });
    $(document).on('submit','#frmsendemail',function(){
        if($("#frmsendemail").valid()){
            return true;
        }else{
            return false;
        }
    });
});

</script>
@endpush