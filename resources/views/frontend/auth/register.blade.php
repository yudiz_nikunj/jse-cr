<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Registration - {{Config::get('settings.sitename')}}</title>
        <noscript>
        <meta http-equiv="refresh" content="0; url={{ route('noscript') }}" />
        </noscript>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ asset('css/components-md.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{ asset('css/login-4.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
       {{--  <link rel="shortcut icon" href="{{ asset('images/'.Config::get('settings.site_logo_before')) }}" /> </head> --}}
    
        <style type="text/css">
        .back-btn-change{
            border-color: #e7505a !important;
            color: #fff !important;
            background-color: #e7505a !important;
        }
        .login .content{
            width:500px !important;
        }
        </style>
        <!-- END HEAD -->
    <body class=" login">
        @include('flash::message')
        <!-- BEGIN LOGO -->
        <div class="logo">
            {{-- <a href="{{env('APP_URL')}}">
                <img src="{{ asset('images/'.Config::get('settings.site_logo_before')) }}" alt="" style="height: 137px;"/> </a> --}}
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <form class="register-form1" id="frmregister" style="display: block;" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <h3>Sign Up</h3>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Full Name</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Enter Name" name="name" value="{{ old('name') }}" maxlength="50"/> 
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Email" id="email" name="email" value="{{ old('email') }}" maxlength="150"/> 
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" maxlength="30"/> 
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('rpassword') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
                    <div class="controls">
                        <div class="input-icon">
                            <i class="fa fa-check"></i>
                            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="rpassword" maxlength="30"/> 
                            @if ($errors->has('rpassword'))
                            <span class="help-block">
                                <strong>{{ $errors->first('rpassword') }}</strong>
                            </span>
                        @endif
                        </div>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Contact No</label>
                    <div class="input-icon">
                        <i class="fa fa-phone"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Enter Contact No" name="phone_number" value="{{ old('phone_number') }}" maxlength="15"/> 
                        @if ($errors->has('phone_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label>
                        <input type="checkbox" name="tnc" data-error-container="#register_tnc_error" /> I agree to the
                        <a href="javascript:;"> Terms of Service </a> and
                        <a href="javascript:;"> Privacy Policy </a>
                    </label>
                    <span id="register_tnc_error"> </span>
                </div>
                <div class="form-actions">
                    <a href="{{route('login')}}" class="btn red btn-outline back-btn-change"> Back </a>
                    <button type="submit" id="register-submit-btn" class="btn green pull-right"> Sign Up </button>
                </div>
            </form>                
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> <?php echo str_replace('{{YEAR}}', date('Y'),Config::get('settings.footer_text')); ?> </div>
        <script type="text/javascript">
            var BASE_PATH = "{{env('APP_URL')}}";
        </script>
        <!-- END COPYRIGHT -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset('plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ asset('plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/backstretch/jquery.backstretch.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ asset('js/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ asset('js/login-4.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
        <script>
            var oTable;
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "positionClass": "toast-top-right",
              "onclick": null,
              "showDuration": "1000",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
            }
            $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
            $(document).ready(function(){
                var cookieEnabled = (navigator.cookieEnabled) ? true : false;

                if (!cookieEnabled)
                { 
                    document.cookie="testcookie";
                    checkCookie = (document.cookie.indexOf("testcookie") != -1) ? true : false;
                    if(!checkCookie){
                        window.location.href = '{{ route('nocookie') }}'
                    }
                }  
            });
        </script>
        @stack('scripts')
    </body>

</html>
<script type="text/javascript">
$(document).ready(function() {

    $("#frmregister").validate({
        rules: {
            name:{
                required:true,
                maxlength:50
            },
            email:{
                required:true,
                email:true,
                pattern: /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
                remote: {
                    url: "{!! url('checkUniqueEmail')!!}",
                    type: "post",
                    data: {
                        _token: function() {
                            return "{{csrf_token()}}"
                        },
                        email: function(){
                            return $("#email").val();
                        }
                    }
                },
                maxlength:150,
            },
            password:{
                required:true,
                minlength:6
            },
            rpassword:{
                equalTo: "#register_password"
            },
            phone_number:{
                required:true,
                digits:true,
                minlength:9,
                maxlength:15
            },
            tnc:{
                required:true
            }
        },
        messages: {
            name:{
                required:"@lang('validation.required',['attribute'=>'name'])",
                maxlength:50
            },
            email:{
                required:"@lang('validation.required',['attribute'=>'email'])",
                email: "@lang('validation.email', ['attribute'=>'email address'])",
                pattern: "@lang('validation.email', ['attribute'=>'email address'])",
                remote:"@lang('validation.exists',['attribute'=>'email'])",
                maxlength:"@lang('validation.max.string',['attribute'=>'email','max'=>150])"
            },
            password:{
                required:"@lang('validation.required',['attribute'=>'password'])",
                minlength:"@lang('validation.min.string',['attribute'=>'password','min'=>6])"               
            },
            rpassword:{
                equalTo:"@lang('validation.same',['attribute'=>'password','other'=>'confirm password'])"
            },
            phone_number:{
                required:"@lang('validation.required',['attribute'=>'contact number'])",
                minlength:"@lang('validation.min.string',['attribute'=>'contact number','min'=>9])",
                maxlength:"@lang('validation.max.string',['attribute'=>'contact number','max'=>15])"
            },
            tnc:{
                required:"Please agree our terms and condition to proceed."
            }
        },
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function (element) {
           $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
           $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                  error.appendTo('.a');
            }else{
                if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            }
        }
    });
    $(document).on('submit','#frmregister',function(){
        if($("#frmregister").valid()){
            return true;
        }else{
            return false;
        }
    });
});

</script>