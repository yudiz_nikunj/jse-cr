
@extends('frontend.layout')

@section('header')

    @include('frontend.partials.user_menu')

@endsection

@section('main')

<!--******************* Middle Section Start ******************-->
<section class="common-section paddingb-none">
    <div class="container">
        <div class="section-title wow">
            <h1>ACCOUNT SETTINGS</h1>
        </div>
        <div class="height-saperator"></div>
        <p>Here is where you can update your account details and personal information. You can also change your password.</p>
        <div class="height-saperator"></div>
        <div class="portal-data-wrap">
            <div class="row">
                <div class="col-sm-6">
                    <div class="ac-detail-block">
                        <div class="portal-heading-bar">
                            <h2><img src="{{ asset('frontend/images/JSE-Icon-vector.svg') }}" alt="jse logo" class="wow animatedslow flipInY"/> CHANGE PASSWORD</h2>
                        </div>
                        <div class="settings-form">
                            <div class="row">
                                <form class="password-form" id="frmchangepass" method="post" action="{{ route('changeuserpass') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-5"><label>ENTER PASSWORD</label></div>
                                        <div class="col-md-7"><input type="password" class="form-control" id="opass" name="opass" /></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-5"><label>NEW PASSWORD</label></div>
                                        <div class="col-md-7"><input type="password" class="form-control" id="npass" name="npass" /></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-5"><label>RETYPE PASSWORD</label></div>
                                        <div class="col-md-7"><input type="password" class="form-control" id="cpass" name="cpass" /></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 submit-setting"><button class="btn btn-default" type="submit">change</button></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="pr-detail-block">
                        <div class="portal-heading-bar lite">
                            <h2><img src="{{ asset('frontend/images/JSE-Icon-vector.svg') }}" alt="jse logo" class="wow animatedslow flipInY"/> PERSONAL DETAILS</h2>
                        </div>
                        <div class="settings-form">
                            <div class="row">
                                <form class="username-form" method="post" id="frmprofile" action="{{ route('settings') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-5"><label>NAME</label></div>
                                        <div class="col-md-7"><input type="text" class="form-control" id="first_name" name="first_name" value="{{ old('first_name',Auth::user()->first_name) }}" /></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-5"><label>SURNAME</label></div>
                                        <div class="col-md-7"><input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name',Auth::user()->last_name) }}" /></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-5"><label>TELEPHONE</label></div>
                                        <div class="col-md-7"><input type="text" class="form-control" id="phone_number" name="phone_number" value="{{ old('phone_number',Auth::user()->phone_number) }}" /></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-5"><label>POSTCODE</label></div>
                                        <div class="col-md-7"><input type="text" class="form-control" id="postcode" name="postcode" value="{{ old('postcode',Auth::user()->postcode) }}" /></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-5"><label>ADDRESS</label></div>
                                        <div class="col-md-7">
                                            {{-- <select class="custom-select" id="address" name="address">
                                                <option>1</option>
                                            </select> --}}
                                            <textarea class="form-control" id="address" name="address" rows="4">{{ old('address',Auth::user()->address) }}</textarea>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div><div class="form-group">
                                        <div class="col-md-5"><label>CITY</label></div>
                                        <div class="col-md-7"><input type="text" class="form-control" id="city" name="city" value="{{ old('city',Auth::user()->city) }}" /></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="col-sm-12 submit-setting"><button class="btn btn-default" type="submit">Update</button></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
<!--******************* Middle Section End ******************-->

@endsection

@push('scripts')

    <script type="text/javascript">
        $(function(){
            $('#frmprofile').validate({
                errorElement: 'span',
                errorClass: 'help-block',
                focusInvalid: false,
                rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true,
                    },
                    phone_number:{
                        required: true,
                        number: true,
                        rangelength: [9, 12]
                    },
                    postcode:{
                        required: true
                    },
                    address:{
                        required: true
                    },
                    city:{
                        required: true
                    }
                },
                messages: {
                    first_name: {
                        required: "@lang('validation.required',['attribute'=>'first name'])"
                    },
                    last_name: {
                        required: "@lang('validation.required',['attribute'=>'last name'])",
                    },
                    phone_number:{
                        required: "@lang('validation.required',['attribute'=>'telephone'])",
                        number: "@lang('validation.numeric',['attribute'=>'telephone'])",
                        rangelength: "The telephone must between 9 to 12.",
                    },
                    postcode:{
                        required: "@lang('validation.required',['attribute'=>'postcode'])"
                    },
                    address:{
                        required: "@lang('validation.required',['attribute'=>'address'])"
                    },
                    city:{
                        required: "@lang('validation.required',['attribute'=>'city'])"
                    }
                },
                errorClass: 'help-block',
                errorElement: 'span',
                highlight: function (element) {
                   $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                   $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "radio") {
                          error.appendTo('.a');
                    }else{
                        if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                },
                submitHandler: function(form) {
                    if($("#frmprofile").valid()) {
                        $(form)[0].submit();
                    }
                }
            });
        });
    </script>


    <script type="text/javascript">
        $(function(){
            $('#frmchangepass').validate({
                errorElement: 'span',
                errorClass: 'help-block',
                focusInvalid: false,
                rules: {
                    opass: {
                        required: true,minlength:6
                    },
                    npass: {
                        required: true,minlength:6
                    },
                    cpass:{
                        required: true,minlength:6,
                        equalTo:"#npass"
                    }
                },
                messages: {
                    opass: {
                        required: "@lang('validation.required',['attribute'=>'old password'])",
                        minlength:"@lang('validation.min.string',['attribute'=>'old password','min'=>6])"
                    },
                    npass: {
                        required: "@lang('validation.required',['attribute'=>'new password'])",
                        minlength:"@lang('validation.min.string',['attribute'=>'new password','min'=>6])"
                    },
                    cpass:{
                        required: "@lang('validation.required',['attribute'=>'confirm password'])",
                        equalTo:"@lang('validation.same',['attribute'=>'new password','other'=>'confirm password'])"
                    }
                },
                errorClass: 'help-block',
                errorElement: 'span',
                highlight: function (element) {
                   $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                   $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "radio") {
                          error.appendTo('.a');
                    }else{
                        if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                },
                submitHandler: function(form) {
                    if($("#frmchangepass").valid()) {
                        $(form)[0].submit();
                    }
                }
            });
        });
    </script>

@endpush
