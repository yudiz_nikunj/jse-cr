
@extends('frontend.layout')

@section('header')

    @include('frontend.partials.user_menu')

@endsection

@section('main')

<!--******************* Middle Section Start ******************-->
<section class="common-section paddingb-none">
    <div class="container">
        <div class="section-title wow">
            <h1>MANAGE CARDS</h1>
        </div>
        <div class="height-saperator"></div>
        <p>Here is where you can update your cards details and add new cards. You can also delete your cards.</p>
        <div class="portal-data-wrap">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="pr-detail-block">
                        <div class="portal-heading-bar lite">
                            <h2><img src="{{ asset('frontend/images/JSE-Icon-vector.svg') }}" alt="jse logo" class="wow animatedslow flipInY"/> Add New Card</h2>
                        </div>
                        <div class="settings-form">
                            <div class="row">
                                 <form class="username-form" id="frmpayment" method="post" action="{{ route('cards') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="package" value="">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>NAME on card</label>
                                            <input type="text" class="form-control" name="card_name" id="card_name" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>credit card no.</label>
                                            <input type="text" class="form-control" name="card_no" id="card_no" />
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>cvv</label>
                                            <input type="text" class="form-control" name="cvv" id="cvv" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>expiration date</label>
                                            <input type="text" class="form-control" placeholder="MM/YY" name="expiration" id="expiration" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>postcode</label>
                                            <input type="text" class="form-control" name="postcode" id="postcode" />
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12 submit-setting"><button class="btn btn-default" type="submit">Save Card</button></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>

        @if (count($customer->sources->data))

        <div class="portal-data-wrap" id="card-list">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="pr-detail-block">
                        <div class="portal-heading-bar lite">
                            <h2><img src="{{ asset('frontend/images/JSE-Icon-vector.svg') }}" alt="jse logo" class="wow animatedslow flipInY"/> Make Card Default</h2>
                        </div>
                        <div class="settings-form">
                            <div class="row">
                                <form id="form-default-card" method="post" action="{{ route('cards.default') }}">
                                    {{ csrf_field() }}
                                    @foreach ($customer->sources->data as $card)
                                        <div class="form-group">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="card_number" id="inputCard_number" value="{{ $card->id }}" {{ ($customer->default_source == $card->id) ? "checked" : "" }}>
                                                    Brand : {{ $card->brand }} ,
                                                    Number : **** **** **** {{$card->last4}} ,
                                                    Expires : {{ str_pad($card->exp_month,2,'0',STR_PAD_LEFT) }} / {{ $card->exp_year }}
                                                </label>

                                                @if ($customer->default_source != $card->id)
                                                    <a href="{{ route('cards').'/'.$card->id }}" class="btn btn-xs btn-danger pull-right delete-card"><i class="fa fa-trash"></i> Delete</a>
                                                @endif
                                             </div>
                                        </div>
                                    @endforeach
                                    <div class="form-group">
                                        <div class="col-md-12 submit-setting"><button class="btn btn-default" type="submit">Make Default</button></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        @endif

    </div>
</section>
<!--******************* Middle Section End ******************-->

@endsection

@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $(document).on('click', '.delete-card', function(event) {
                event.preventDefault();
                $(this).html("<i class='fa fa-spinner fa-spin'></i>");
                var self = $(this);
                var url = $(this).attr('href');
                axios.delete(url)
                .then(function (response) {
                    if(response.data.success){
                        self.closest(".form-group").fadeOut();
                        self.closest(".form-group").remove();

                        showMessage("success", "Card Deleted Successfully.");

                        if ( $("#form-default-card :input[type='radio']").length == 0){
                            $("#card-list").remove();
                        }
                    }else{
                        $(self).html("<i class='fa fa-trash'></i> Delete");
                        showMessage("error", "Can't Delete Card.");
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
            });
        });

        $(function(){

            jQuery.validator.addMethod('date', function (value) {
                return /^((\d{2})\/(\d{2}))$/.test(value);
            }, 'Please Enter valid date formate MM/YY.');

            $('#frmpayment').validate({
                errorElement: 'span',
                errorClass: 'help-block',
                focusInvalid: false,
                rules: {
                    card_name: {
                        required: true
                    },
                    card_no: {
                        required: true,
                        minlength: 16,
                        maxlength: 16
                    },
                    cvv:{
                        required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 3

                    },
                    expiration:{
                        required: true,
                        date: true
                    }
                },
                messages: {
                    card_name: {
                        required: "@lang('validation.required',['attribute'=>'card on name'])"
                    },
                    card_no: {
                        required: "@lang('validation.required',['attribute'=>'card no'])",
                        minlength: "@lang('validation.min.string',['attribute'=>'card no','min'=>16])",
                        maxlength: "@lang('validation.max.string',['attribute'=>'card no','max'=>16])"
                    },
                    cvv:{
                        required: "@lang('validation.required',['attribute'=>'cvv'])",
                        minlength: "@lang('validation.min.string',['attribute'=>'cvv','min'=>3])",
                        maxlength: "@lang('validation.max.string',['attribute'=>'cvv','max'=>3])"
                    },
                    expiration:{
                        required: "@lang('validation.required',['attribute'=>'date'])",
                        number: "@lang('validation.numeric',['attribute'=>'date'])"
                    }
                },
                errorClass: 'help-block',
                errorElement: 'span',
                highlight: function (element) {
                   $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                   $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "radio") {
                          error.appendTo('.a');
                    }else{
                        if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                },
                submitHandler: function(form) {
                    if($("#frmpayment").valid()) {
                        $(form)[0].submit();
                    }
                }
            });
        });
    </script>
@endpush
