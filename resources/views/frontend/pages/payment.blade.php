
@extends('frontend.layout')

@section('header')

    @include('frontend.partials.user_menu')

@endsection

@section('main')

<!--******************* Middle Section Start ******************-->

<section class="common-section">
    <div class="container">
        <div class="section-title wow">
            <h1>PAYMENTS</h1>
        </div>
        <div class="height-saperator"></div>
        <div class="saperator wow animatedslow fadeIn"></div>
        <p><b>£{{ $amount }} Per {{ ucfirst($package) }}</b></p>
        <div class="contact-list-block">
            <h2>Total: £{{ $amount }}</h2>
            <div class="col-md-10 col-md-offset-1">
                <div class="pr-detail-block">
                    <div class="settings-form">
                        <div class="row">
                            <form class="username-form" id="frmpayment" method="post" action="{{ route('subscribe') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="package" value="{{ $package }}">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>NAME on card</label>
                                        <input type="text" class="form-control" name="card_name" id="card_name" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>credit card no. <b><i>[VISA]</i></b></label>
                                        <input type="text" class="form-control" name="card_no" id="card_no" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>cvv</label>
                                        <input type="text" class="form-control" name="cvv" id="cvv" />
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>expiration date</label>
                                        <input type="text" class="form-control" placeholder="MM/YY" name="expiration" id="expiration" />
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>postcode</label>
                                        <input type="text" class="form-control" name="postcode" id="postcode" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="clearfix"></div>
                                <div class="col-sm-12 submit-setting"><button class="btn btn-default" type="submit">make payment</button></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<!--******************* Middle Section End ******************-->

@endsection


@push('scripts')

    <script type="text/javascript">

        $(function(){

            jQuery.validator.addMethod('date', function (value) {
                return /^((\d{2})\/(\d{2}))$/.test(value);
            }, 'Please Enter valid date formate MM/YY.');

            $('#frmpayment').validate({
                errorElement: 'span',
                errorClass: 'help-block',
                focusInvalid: false,
                rules: {
                    card_name: {
                        required: true
                    },
                    card_no: {
                        required: true,
                        minlength: 16,
                        maxlength: 16
                    },
                    cvv:{
                        required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 3

                    },
                    expiration:{
                        required: true,
                        date: true
                    }
                },
                messages: {
                    card_name: {
                        required: "@lang('validation.required',['attribute'=>'card on name'])"
                    },
                    card_no: {
                        required: "@lang('validation.required',['attribute'=>'card no'])",
                        minlength: "@lang('validation.min.string',['attribute'=>'card no','min'=>16])",
                        maxlength: "@lang('validation.max.string',['attribute'=>'card no','max'=>16])"
                    },
                    cvv:{
                        required: "@lang('validation.required',['attribute'=>'cvv'])",
                        minlength: "@lang('validation.min.string',['attribute'=>'cvv','min'=>3])",
                        maxlength: "@lang('validation.max.string',['attribute'=>'cvv','max'=>3])"
                    },
                    expiration:{
                        required: "@lang('validation.required',['attribute'=>'date'])",
                        number: "@lang('validation.numeric',['attribute'=>'date'])"
                    }
                },
                errorClass: 'help-block',
                errorElement: 'span',
                highlight: function (element) {
                   $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                   $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "radio") {
                          error.appendTo('.a');
                    }else{
                        if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                },
                submitHandler: function(form) {
                    if($("#frmpayment").valid()) {
                        $(form)[0].submit();
                    }
                }
            });
        });
    </script>

@endpush
