@extends('frontend.layout')

@section('header')
<section class="main-banner page-banner about-banner">
        
    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="banner-desc">
        <h4 class="wow animatedslow fadeIn">New Property</h4>
    </div>
    <!--******************* Banner Section End ******************-->
    
</section>
@endsection


@push('styles')
   <style type="text/css">
    .selected-item {
        color: #CA9401 !important;
    }
   </style>
    <!--Plugin CSS file with desired skin-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    
@endpush

@section('main')

    <html>
        <section class="common-section property-page-wrapper">
            <div class="row">
                <div class="col-sm-3">
                    <div class="filter-wrapper">
                        <p class="filter-title title"> Filters <i class="fa fa-filter" aria-hidden="true"></i> </p>
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    Location
                                    </a>
                                </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li>
                                                <a href="javascript:" data-location="North London" class="location" > North London </a>                                                
                                            </li>
                                            <li>
                                                <a href="javascript:" data-location="East London" class="location" > East London </a>                                                
                                            </li>
                                            <li>
                                                <a href="javascript:" data-location="Central London" class="location" > Central London </a>                                                
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        Property type
                                    </a>
                                </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                      <li><label><input name="property_type[]" onchange="filter()" type="checkbox" value="Apartment"> Apartment</label> </li>
                                      <li><label><input name="property_type[]" onchange="filter()" type="checkbox" value="Flat"> Flat</label> </li>
                                      <li><label><input name="property_type[]" onchange="filter()" type="checkbox" value="Townhouse"> Townhouse</label> </li>
                                    </ul>
                                </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                    No. of Rooms
                                    </a>
                                </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                      <li><label><input name="bedroom[]" onchange="filter()" type="checkbox" value="1"> 1</label> </li>
                                      <li><label><input name="bedroom[]" onchange="filter()" type="checkbox" value="2"> 2</label> </li>
                                      <li><label><input name="bedroom[]" onchange="filter()" type="checkbox" value="3"> 3</label> </li>
                                      <li><label><input name="bedroom[]" onchange="filter()" type="checkbox" value="4"> 4</label> </li>
                                      <li><label><input name="bedroom[]" onchange="filter()" type="checkbox" value="5"> 5</label> </li>
                                    </ul>
                                </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                        Price Range
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="slidecontainer">
                                            <input type="text" class="js-range-slider" name="my_range" value="" id="myRange" />
                                            <!-- <input type="range" id="demo_5"> -->
                                            <!-- <input id="demo_5" type="text" name="" value="" class="irs-hidden-input" tabindex="-1" readonly=""> -->
                                            <!-- <datalist id="tickmarks">
                                                <option value="0"></option>
                                                <option value="10"></option>
                                                <option value="20"></option>
                                                <option value="30"></option>
                                                <option value="40"></option>
                                                <option value="50"></option>
                                                <option value="60"></option>
                                                <option value="70"></option>
                                                <option value="80"></option>
                                                <option value="90"></option>
                                                <option value="100"></option>
                                            </datalist> -->
                                            <!-- <input type="text" class="js-range-slider" name="my_range" value=""/> -->
                                            <!-- <p>Range Value (in £): <span id="demo"></span></p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                     Amenities
                                    </a>
                                </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        @foreach($amenities as $amn)
                                            <li><label><input onchange="filter()" name="amenities[]"  type="checkbox" value="{{ $amn->id }}">  {{ $amn->title }}</label> </li>
                                        @endforeach
                                    </ul>
                                </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="latest-properties-wrapper">                
                        <div class="row d-flex property-block">
                            @forelse($property as $prop)
                                <div class="col-sm-4 prop-block">
                                    <div class="card mb-4 box-shadow">
                                        <a class="box-link" href="{{ route('property-detail',$prop->slug) }}"></a>
                                        <img class="card-img-top" alt="card-image" style="height: 225px; width: 100%; display: block;" src="{{ $prop->propertyimg() }}" data-holder-rendered="true">
                                        <div class="card-body">
                                            <div class="card-text">
                                                <p><span>{{ str_limit($prop->name, 55, '...')  }}</span></p>
                                                <p>£{{ number_format($prop->price,2) }}</p>
                                            </div>
                                            <div class="property-type-box">
                                                <p class="text-capitalize">{{ $prop->location }}</p>
                                                <p class="text-capitalize">{{ $prop->property_type }}</p>
                                            </div>
                                            <div class="card-icon-box">
                                                <div class="row">
                                                    @if($prop->bathroom>0)
                                                    <div class="col-sm-4 col-xs-4 text-left">
                                                        <div class="icon-holder"><figure><img src="{{ asset('frontend/images/shower-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure> <span> {{ $prop->bathroom }} </span> </div>
                                                    </div>
                                                    @endif
                                                    @if($prop->bedroom>0)
                                                    <div class="col-sm-4 col-xs-4 text-center">
                                                        <div class="icon-holder"><figure><img src="{{ asset('frontend/images/bed-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure> <span> {{ $prop->bedroom }}</span></div>
                                                    </div>
                                                    @endif
                                                    @if($prop->livingroom>0)
                                                    <div class="col-sm-4 col-xs-4 text-right">
                                                        <div class="icon-holder"><figure><img src="{{ asset('frontend/images/sofa-icon.svg') }}" alt="jse management icon" class="wow flipInY"></figure> <span>  {{ $prop->livingroom }}</span></div>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <h1 class="col-xs-12 text-center">No property found.</h1>
                            @endforelse
                            @if($property->total() > $paginate)
                            <div class="property-btn text-center col-xs-12 loadmore">
                                <a class="theme-btn wow slow fadeIn animated" onclick="loadmore()" href="javascript:" style="visibility: visible; animation-name: fadeIn;">Load more</a>
                            </div>
                            @endif
                        </div> <!-- row ends -->
                         <div class="loader-wrapper">
                            <div class="loader">Loading...</div>
                        </div>
                    </div> <!-- latest properties wrapper -->
                </div>
            </div>
            
        </section>
    </html>
    @include('frontend.partials.get_in_touch')
    @include('frontend.partials.map')

@endsection

@push('scripts')
 <script src="{{ asset('') }}frontend/js/jcf.checkbox.js" ></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!--Plugin JavaScript file-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
<script>
    var ENDPOINT = "{{ url('/') }}";
    var page = 2;
    var totalPage = {{ $property->lastPage()}};
    var loc = '';
    function loadmore() {        

        $('.loadmore').remove();
        $.ajax({
            url: ENDPOINT + "/new-property?page=" + page,
            datatype: "html",
            type: "get",
            beforeSend: function () {
                $('.layer').show();
            }
        })
        .done(function (response) {     
            
            $('.layer').hide();
            $(".prop-block").last().after(response);

            if(page >= totalPage){
                $(".loadmore").hide();
            }
            page++;

            $('html, body').animate({
                scrollTop: $(".prop-block").last().offset().top
            }, 1000);
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });


    }

    

    function filter(){
        $('.loadmore').remove();
        var property_type = $.map($('input[name="property_type[]"]:checked'), function(c){return c.value; })     
        var bedroom = $.map($('input[name="bedroom[]"]:checked'), function(c){return c.value; })     
        var amenities = $.map($('input[name="amenities[]"]:checked'), function(c){return c.value; });       
        var price = $("#myRange").val();       
        page = 1

        $.ajax({
            url: ENDPOINT + "/new-property?page=" + page + "&amenities="+amenities+ "&bedroom="+bedroom+ "&property_type="+property_type+ "&location="+loc+ "&price="+price,
            datatype: "html",
            type: "get",
            beforeSend: function () {
                $('.layer').show();
            }
        })
        .done(function (response) {            
            $('.layer').hide();
            $(".property-block").html(response);
            page = 2;
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

    $(document).on('click','.location',function(){
        $('.location').removeClass('selected-item')
        if($(this).attr('data-location') == loc){
            loc = '';
            $(this).removeClass('selected-item');
        }else{
            loc = $(this).attr('data-location');
            $(this).addClass('selected-item');
        }        
        filter()
    })

    // toggle filter 
    if(jQuery(window).width() < 768){
        // $(document).ready(function(){
           
        //     $('.filter-title').click(function() {
        //         $(".panel-group").addClass('active');
        //     });
        //     $('.filter-title').click(function() {
        //         $(".panel-group").removeClass('active');
        //     });
        // });
        jQuery('.filter-wrapper').on('click', function() {
            if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
            }
            else
            {
            $(this).addClass('active');
            }

        });

         jQuery('.accordion-toggle').on('click', function() {
            if (jQuery('.filter-wrapper').hasClass('active')) {
            $('.filter-wrapper').removeClass('active');
            }
            else
            {
            $('.filter-wrapper').addClass('active');
            }
        });
    }

    $(document).on('change','#myRange',function(){
       filter()
    })
    $(document).ready(function(){
         var custom_values = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1250, 1500, 1750, 2000, 2250, 2500, 2750, 3000, 3250, 3500, 3750, 4000, 4250, 4500, 4750, 5000, 5500, 6000];
        
        // be careful! FROM and TO should be index of values array
        var my_from = custom_values.indexOf(100);
        var my_to = custom_values.indexOf(6000);
        
        $(".js-range-slider").ionRangeSlider({
            type: "double",
            // grid: true,
            from: my_from,
            to: my_to,
            values: custom_values,
            prefix: "£"
        });     
    });
    
</script>
@endpush

