@extends('frontend.layout')

@section('header')
<section class="main-banner page-banner contact-banner">
        
    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="banner-desc">
        <h4 class="wow animatedslow fadeIn">{!! $page->title !!}</h4>
    </div>
    <!--******************* Banner Section End ******************-->
    
</section>
@endsection


@section('main')

<!--******************* Middle Section Start ******************-->

    {!! $page->content !!}

	@include('frontend.partials.write_to_us')

<!--******************* Middle Section End ******************-->

	@include('frontend.partials.map')


@endsection