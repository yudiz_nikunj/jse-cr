@extends('frontend.layout')

@section('header')

<section class="video-banner main-banner" data-vide-bg="{{ asset('') }}frontend/media/video">

    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="banner-desc">
        <div class="container">
            <h1>{!! $page->title !!}</h1>
            <a href="about" class="wow animatedslow fadeIn">Find Out More</a>
        </div>
    </div>
    <!--******************* Banner Section End ******************-->
</section>

@endsection

@section('main')

    {!! $page->content !!}

    @include('frontend.partials.get_in_touch')

    @include('frontend.partials.map')

@endsection

