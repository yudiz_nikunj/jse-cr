
@extends('frontend.layout')

@section('header')

    @include('frontend.partials.user_menu')

@endsection

@section('main')

<!--******************* Middle Section Start ******************-->

<section class="common-section">
    <div class="container">
        <div class="section-title wow">
            <h1>MEMBERSHIP</h1>
        </div>
        <div class="height-saperator"></div>
        <p>Thank you for joining JSE. Please select from the list below the membership that most suits your needs.</p>
        <div class="height-saperator"></div>
        <div class="portal-data-wrap">
            <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                <form class="membership-selection" action="#" method="post" onsubmit="return false;">
                    {{ csrf_field() }}
                    <div class="col-sm-6">
                        <div class="membership-block portal-side {{ ($user->active_plan == 'monthly') ? 'active-package' : '' }}">
                            <h1>£{{ $user->monthly }}</h1>
                            <h3>PAY MONTHLY</h3>
                            <figure><img src="{{ asset('frontend') }}/images/property-image.png" alt="jse-logo" /></figure>
                            <p>Select to Continue</p>
                            <div>
                                <input type="radio" name="package-selection" {{ ($user->active_plan == 'monthly') ? 'checked="checked"' : '' }} id="rad-month" />
                            </div>
                            <div class="my-package-box">
                                <h2>my<br>package</h2>
                                <div class="saperator wow animated fadeIn"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="membership-block portal-side {{ ($user->active_plan == 'yearly') ? 'active-package' : '' }}">
                            <h1>£{{ $user->yearly }}</h1>
                            <h3>PAY ANNUALLY</h3>
                            <figure><img src="{{ asset('frontend') }}/images/property-image.png" alt="jse-logo" /></figure>
                            <p>Select to Continue</p>
                            <div>
                                <input type="radio" name="package-selection"  {{ ($user->active_plan == 'yearly') ? 'checked="checked"' : '' }} id="rad-annual" />
                            </div>
                            <div class="my-package-box">
                                <h2>my<br>package</h2>
                                <div class="saperator wow animated fadeIn"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <button class="wow slow fadeIn animated" type="submit">Update Membership</button>
                </form>
            </div>
        </div>
    </div>
</section>
<!--******************* Middle Section End ******************-->

@endsection
