@extends('frontend.layout')

@section('header')
<section class="main-banner page-banner service-management-banner">

    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="banner-desc">
        <h4 class="wow animatedslow fadeIn">{!! $page->title !!}</h4>
    </div>
    <!--******************* Banner Section End ******************-->

</section>
@endsection


@section('main')

<!--******************* Middle Section Start ******************-->

    {!! $page->content !!}

    <section class="common-section signup-section bg-parallex" id="our-rates">
	    <div class="container">
	        <div class="section-title wow">
	            <h1>OUR RATES</h1>
	        </div>
	        <div class="height-saperator"></div>
	        <p><b>JSE Property Management</b> offers a fixed price property management service based on the size of each individual property. Lettings and Estate Agents will generally charge you a percentage of your rental income meaning you pay more the higher your income is. We are universally lower than a lettings agent with our fees being payable in monthly instalments or with a 10% discount if paid upfront.</p>
	        <p>We operate on seven day payment terms upon completion of each job. You will be alerted by text and email once the invoice is available to pay through the JSE Customer Portal.</p>
	        <div class="signup-form">
	            <figure><img src="{{ asset('/') }}frontend/images/LogoIcon-BLACK.png" alt="jse-logo" class="wow animatedslow flipInY"/></figure>
	            <h3>SIGN UP NOW!</h3>
	            <p>Enter your details below and we'll contact you shortly to provide you with a quotation based on the size of your property</p>


	    		@include('frontend.partials.signup_form')


	    		<div class="clearfix"></div>
	        </div>
	    </div>
	</section>

<!--******************* Middle Section End ******************-->

@endsection
