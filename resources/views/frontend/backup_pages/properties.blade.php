@extends('frontend.layout')

@section('header')

    @include('frontend.partials.user_menu')

@endsection

@section('main')

<!--******************* Middle Section Start ******************-->
<section class="common-section paddingb-none">
    <div class="container">
        <div class="section-title wow">
            <h1>PROPERTIES</h1>
        </div>
        <div class="height-saperator"></div>
        <p>All your existing properties under JSE Property Management can be found here. We can add and/or take off properties when needed.</p>
        <div class="height-saperator"></div>
        <div class="portal-data-wrap">
            <div class="col-sm-8 peoperty-block">
                <div class="row">
                    @if(count($properties))
                        @foreach($properties as $property)
                            <div class="col-sm-6">
                                <div class="property-box">
                                    <a href="javascript:;"><figure><img src="{{ asset('') }}frontend/images/property-image.png" alt="jse property icon" /></figure></a>
                                    <a href="javascript:;"><h3>{{ $property->postcode }}</h3></a>
                                    <a href="javascript:;"><p>{{ $property->address }}</p></a>
                                    {{-- <div class="proprty-action"><a href="{{ route('properties').'/'.$property->id }}">DELETE PROPERTY</a></div> --}}
                                    <div class="proprty-action"><a href="{{ route('properties').'/'.$property->id }}" data-id="{{$property->id}}" class="property-delete">DELETE PROPERTY</a></div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-sm-12">
                            <div class="note note-danger">
                                <strong> There is no property available. </strong>
                            </div>
                        </div>
                    @endif
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="property-sidebar">
                    <div class="portal-heading-bar">
                        <h2>ADD PROPERTY</h2>
                    </div>
                    <figure><img src="{{ asset('') }}frontend/images/LogoIcon-YELLLOW.png" alt="jse logo" class="wow animatedslow flipInY"/></figure>
                    <form action="{{ route('properties') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>ENTER POSTCODE</label>
                            <input type="text" class="form-control" id="postcode" name="postcode" placeholder="ie: NW10 8GE" autofocus />
                        </div>
                        <div class="form-group">
                            <label>SELECT ADDRESS</label>
                            <input id="address-search" name="address-selected" class="form-control" placeholder="Select your address" disabled />
                            <input type="hidden" id="user_location" name="address"/>
                            <p><span id="msg_addSearch"></span></p>
                        </div>
                        <div class="form-group">
                            <div class="submit-setting">
                                <button class="btn btn-default wow animatedslow fadeIn" type="submit">CREATE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<!--******************* Middle Section End ******************-->

@endsection

@push('scripts')

    <script type="text/javascript">

        $(document).on('keyup',"#postcode",function () {
            var searchContext = "",
                container = "",
                key = "{{ env('PCA_KEY') }}";

            $("#address-search").val($(this).val());
            $("#address-search").trigger('keydown');

            $("#address-search").autocomplete({
                source: function(request, response) {
                    $("#user_location").val('');

                    $.getJSON("https://services.postcodeanywhere.co.uk/Capture/Interactive/Find/v1.00/json3.ws?callback=?",
                    {
                        Key: key,
                        Text: request.term,
                        Container: container,
                        Origin: 'GBR',
                        Countries: '',
                        Limit: '',
                        Language: 'en'
                    },
                    function (data) {
                        container = '';
                        response($.map(data.Items, function(suggestion) {
                            return {
                                label: suggestion.Text +' '+suggestion.Description,
                                value: "",
                                data: suggestion
                            }
                        }));
                    });
                },
                select: function(event, ui) {
                    var item = ui.item.data;
                    var field = $(this);
                    if (item.Type == 'Postcode') {
                        $('#postcode').val(item.Text);
                    }
                    if (item.Type == 'Address') {
                        
                        $('#postcode').val(item.Description.substring(Number(item.Description.lastIndexOf(","))+2), item.Description.length);
                        
                        populateAddress(item.Text +' '+item.Description)
                    }else{

                        searchContext = item.Id;
                        window.setTimeout(function() {
                            container = item.Id;
                            field.autocomplete("search", item.Text);
                        });
                    }
                },
                autoFocus: true,
                minLength: 1,
                delay: 100
            }).focus(function() {
                searchContext = "";
                container = "";
            });

            function populateAddress(address) {
                $("#user_location").val(address.replace(/\n/g, '<br/>'));
            }
        });

    </script>

@endpush
