<section class="common-section signup-section bg-parallex">
    <div class="container">
        <div class="section-title wow">
            <h1>WRITE TO US</h1>
        </div>
        <div class="height-saperator"></div>
        <p>Enter your details below and let us know about the nature of your enquiry. We'll be back in touch to assist you shortly.</p>
        <div class="signup-form">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <form id="frmwritetous" action="{{ route('get_in_touch') }}" method="post" name="frmwritetous">
                        {{ csrf_field() }}
                        <input type="hidden" name="type" value="write-to-us">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Name" name="name" id="name">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email" name="email" id="email">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Telephone" name="telephone" id="telephone">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="6" placeholder="Enquiry" name="enquiry" id="enquiry"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Send</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>


@push('scripts')

    <script type="text/javascript">
        $(function(){
            $('#frmwritetous').validate({
                errorElement: 'span',
                errorClass: 'help-block',
                focusInvalid: false,
                rules: {
                    name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    telephone:{
                        required: true,
                        number: true
                    },
                    enquiry:{
                        required: true
                    }
                },
                messages: {
                    name: {
                        required: "@lang('validation.required',['attribute'=>'Name'])"
                    },
                    email: {
                        required: "@lang('validation.required',['attribute'=>'Email'])",
                        email: "@lang('validation.email',['attribute'=>'Email'])"
                    },
                    telephone:{
                        required: "@lang('validation.required',['attribute'=>'Telephone'])",
                        number: "@lang('validation.numeric',['attribute'=>'Telephone'])",
                    },
                    enquiry:{
                        required: "@lang('validation.required',['attribute'=>'Enquiry'])"
                    }
                },
                errorClass: 'help-block',
                errorElement: 'span',
                highlight: function (element) {
                   $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                   $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "radio") {
                          error.appendTo('.a');
                    }else{
                        if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                },
                submitHandler: function(form) {  
                    if($("#frmwritetous").valid()) {
                        $(form).submit();
                    }
                }
            }); 
        });
    </script>

@endpush