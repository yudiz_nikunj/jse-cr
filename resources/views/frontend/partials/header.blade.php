<section class="video-banner main-banner" data-vide-bg="{{ asset('') }}frontend/media/video">
        
    @include('frontend.partials.menu')

    <!--******************* Banner Section Start ******************-->
    <div class="banner-desc">
        <div class="container">
            <h1>REDEFINING THE INDUSTRY<span>A PERSONABLE APPROACH YOU CAN TRUST</span></h1>
            <a href="#" class="wow animatedslow fadeIn">Find Out More</a>
        </div>
    </div>    
    <!--******************* Banner Section End ******************-->
</section>