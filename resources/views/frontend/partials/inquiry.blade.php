<section class="common-section signup-section bg-parallex " id="inquiry">
    <div class="container">
        <div class="section-title wow">
            <h1>Enquire Now</h1>
        </div>
        <div class="height-saperator"></div>
        <p>To arrange a viewing, or find out more about this property, please enter your details below and one of the members of our team will be in touch with you soon.</p>
        <div class="signup-form">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <form id="frmgetintouch" action="{{ route('propertyinquiry') }}" method="post" name="frmgetintouch">
                        {{ csrf_field() }}
                        <input type="hidden" name="property_name" value="{{ $property_name }}">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Name" name="name" id="name">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email" name="email" id="email">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Telephone" name="telephone" id="telephone">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="6" placeholder="Enquiry" name="enquiry" id="enquiry"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Send</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>


@push('scripts')

    <script type="text/javascript">
        $(function(){
            $('#frmgetintouch').validate({
                errorElement: 'span',
                errorClass: 'help-block',
                focusInvalid: false,
                rules: {
                    name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    telephone:{
                        required: true,
                        number: true
                    },
                    enquiry:{
                        required: true
                    }
                },
                messages: {
                    name: {
                        required: "@lang('validation.required',['attribute'=>'Name'])"
                    },
                    email: {
                        required: "@lang('validation.required',['attribute'=>'Email'])",
                        email: "@lang('validation.email',['attribute'=>'Email'])"
                    },
                    telephone:{
                        required: "@lang('validation.required',['attribute'=>'Telephone'])",
                        number: "@lang('validation.numeric',['attribute'=>'Telephone'])",
                    },
                    enquiry:{
                        required: "@lang('validation.required',['attribute'=>'Enquiry'])"
                    }
                },
                errorClass: 'help-block',
                errorElement: 'span',
                highlight: function (element) {
                   $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                   $(element).closest('.form-group').removeClass('has-error');
                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "radio") {
                          error.appendTo('.a');
                    }else{
                        if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else {
                            error.insertAfter(element);
                        }
                    }
                },
                submitHandler: function(form) {  
                    if($("#frmgetintouch").valid()) {
                        $(form).submit();
                    }
                }
            }); 
        });
    </script>

@endpush