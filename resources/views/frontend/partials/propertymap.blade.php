@if(isset($longitude) && isset($latitude))
<section id="map" class="location-section"></section>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-uSiExC_Ct1bzRRAaLDhC3IcD10Yh-7c&callback=initMap" type="text/javascript"></script>
<script>
    
    $(window).resize(function(){
                initMap();
           });
            function initMap() {
                var mapDiv = document.getElementById('map');
                var latlng= new google.maps.LatLng({{ $latitude }},{{ $longitude }});
                var map = new google.maps.Map(mapDiv, {
                  center: latlng,
                  zoom: 14,
                  mapTypeId: google.maps.MapTypeId.MAP,
                  disableDefaultUI: true,
                  draggable: false,
                  scrollwheel: false,
                  navigationControl: false,
                  mapTypeControl: false,
                  scaleControl: false
                });

                var marker = new google.maps.Marker({
                    position: latlng,
                    icon: '{{ asset('frontend/images/map-marker.png')}}'
                });
                marker.setMap(map);
                map.set('styles',
                    [
                        {
                          "elementType": "labels.text.fill",
                          "stylers": [
                            {
                              "color": "#2e2e2e"
                            }
                          ]
                        },
                        {
                          "featureType": "landscape",
                          "stylers": [
                            {
                              "color": "#f0f0f0"
                            }
                          ]
                        },
                        {
                          "featureType": "poi",
                          "stylers": [
                            {
                              "visibility": "off"
                            }
                          ]
                        },
                        {
                          "featureType": "road",
                          "elementType": "geometry",
                          "stylers": [
                            {
                              "color": "#ffffff"
                            }
                          ]
                        },
                        {
                          "featureType": "road",
                          "elementType": "labels.text.fill",
                          "stylers": [
                            {
                              "color": "#777777"
                            }
                          ]
                        },
                        {
                          "featureType": "road.highway",
                          "elementType": "geometry.fill",
                          "stylers": [
                            {
                              "color": "#e6e6e6"
                            }
                          ]
                        },
                        {
                          "featureType": "transit",
                          "stylers": [
                            {
                              "visibility": "off"
                            }
                          ]
                        },
                        {
                          "featureType": "water",
                          "elementType": "geometry.fill",
                          "stylers": [
                            {
                              "color": "#ffd107"
                            }
                          ]
                        },
                        {
                          "featureType": "water",
                          "elementType": "labels",
                          "stylers": [
                            {
                              "visibility": "off"
                            }
                            ]
                        }
                    ]);
            }
</script>
@endif