<header>
    <div class="container">
        <div class="row main-row">
            <div class="col-sm-4 call-block"><a href="tel:"{{ Config::get('settings.footer_contact') }}>Call  {{ Config::get('settings.footer_contact') }}</a></div>
            <div class="col-sm-4 col-xs-5 logo-block"><a href="{{ route('home') }}"><img src="{{ asset('') }}frontend/images/JSE_Logo_Trans.svg" alt="jse logo" /></a></div>
            <div class="col-sm-4 col-xs-5 menu-block desktop-menu hidden-xs">
                <ul class="header-social-icons">
                    {{-- <li><a href="https://www.facebook.com/JSEPropertymanagement"><img src="{{ asset('') }}frontend/images/facebook.svg" alt="facebook" ></a></li> --}}
                    <li><a href="https://www.google.com/search?q=jse+property+management&rlz=1C5CHFA_enGB915GB915&oq=jse+property+management&aqs=chrome..69i57j46i175i199j69i59l3j69i60l3.5344j1j9&sourceid=chrome&ie=UTF-8#lrd=0x48761d388044c4fd:0x626f8bc431649f24,1"><img src="{{ asset('') }}frontend/images/search.svg" alt="google search" ></a></li>
                </ul>
                <p class="mb-0">Reviews</p>
                <a href="#">
                    <img src="{{ asset('') }}frontend/images/Menu-Icon.png" alt="menu-icon">
                </a>
            </div>
            <div class="col-sm-4 col-xs-7 menu-block mobile-menu visible-xs">
                <ul class="header-social-icons">
                   {{--  <li><a href="https://www.facebook.com/JSEPropertymanagement"><img src="{{ asset('') }}frontend/images/facebook.svg" alt="facebook" ></a></li> --}}
                    <li><a href="https://www.google.com/search?q=jse+property+management&rlz=1C5CHFA_enGB915GB915&oq=jse+property+management&aqs=chrome..69i57j46i175i199j69i59l3j69i60l3.5344j1j9&sourceid=chrome&ie=UTF-8#lrd=0x48761d388044c4fd:0x626f8bc431649f24,1"><img src="{{ asset('') }}frontend/images/search.svg" alt="google search" ></a></li>
                </ul>
                <p class="mb-0">Reviews</p>
                <a href="#" id="my-button">
                    <img src="{{ asset('') }}frontend/images/Menu-Icon.png" alt="menu-icon">
                </a>
            </div>
            <div class="clearfix"></div>
            <div class="menu-box">
                <div class="main-menu hidden-xs">
                    <ul class="main-navigation">
                        <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{ route('home') }}">Home</a></li>
                        
                        <li class="dropdown-list">
                            <a>services</a>
                            <ul class="sub-menu">
                                <li class="{{ Request::is('service-overview') ? 'active' : '' }}"><a href="{{ route('overview') }}">overview</a></li>
                                <li class="{{ Request::is('management') ? 'active' : '' }}"><a href="{{ route('management') }}">management</a></li>
                                <li class="{{ Request::is('development') ? 'active' : '' }}"><a href="{{ route('development') }}">development</a></li>
                                <li class="{{ Request::is('sourcing') ? 'active' : '' }}"><a href="{{ route('sourcing') }}">sourcing</a></li>
                            </ul>
                        </li>
                        <li class="{{ Request::is('new-property') ? 'active' : '' }}"><a href="{{ route('newproperty') }}">PROPERTIES</a></li>
                        <li class="{{ Request::is('about') ? 'active' : '' }}"><a href="{{ route('about') }}">about</a></li>
                        <li class="{{ Request::is('coverage') ? 'active' : '' }}"><a href="{{ route('coverage') }}">coverage</a></li>
                        
                        <li class="{{ Request::is('contact') ? 'active' : '' }}"><a href="{{ route('contact') }}">contact</a></li>
                    </ul>
                    <div class="menu-desc">
                        <figure>
                            <img src="{{ asset('') }}frontend/images/JSE-Icon-vector.svg" alt="jse logo" class="wow animatedslow flipInY"/>
                        </figure>
                        <a href="{{ url('login') }}" class="wow animatedslow fadeIn">portal</a>
                    </div>
                    <a href="#" class="close-menu"></a>
                </div>
            </div>
            <nav id="menu">
                <ul>
                    <li><a href="{{ route('home') }}" class="menu-item">Home</a></li>
                   
                    <li>
                        <a>services</a>
                        <ul>
                            <li><a href="{{ route('overview') }}" class="menu-item">overview</a></li>
                            <li><a href="{{ route('management') }}" class="menu-item">management</a></li>
                            <li><a href="{{ route('development') }}" class="menu-item">development</a></li>
                            <li><a href="{{ route('sourcing') }}" class="menu-item">sourcing</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('newproperty') }}" class="menu-item">PROPERTIES</a></li>
                     <li><a href="{{ route('about') }}" class="menu-item">about</a></li>
                    <li><a href="{{ route('coverage') }}" class="menu-item">coverage</a></li>
                    <li><a href="{{ route('contact') }}" class="menu-item">contact</a></li>
                    <li><a href="{{ url('login') }}" class="menu-item">portal</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
