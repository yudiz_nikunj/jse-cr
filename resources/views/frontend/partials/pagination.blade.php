@if ($paginator->hasPages())
    <div class="portal-footer-bar">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            {{-- <li class="disabled"><span>Previous</span></li> --}}

        @else
            <a href="{{ $paginator->previousPageUrl() }}" rel="prev">Previous</a>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" rel="next">Next</a>
        @else
            {{-- <li class="disabled"><span>@lang('pagination.next')</span></li> --}}
        @endif
    </div>
@endif