@section('class')
    template-portal
@endsection

<section class="main-banner page-banner">
    <header>
        <div class="container">
            <div class="row">
                <div class="col-sm-4 call-block"><a href="{{ route('notifications') }}"><b>{{ ($notification > 0) ? $notification.' New ' : 'No '}}</b>Notification</a></div>
                <div class="col-sm-4 col-xs-6 logo-block"><a href="{{ url('/') }}"><img src="{{ asset('') }}frontend/images/JSE_Logo_Trans.svg" alt="jse logo" /></a></div>
                <div class="col-sm-4 menu-block desktop-menu hidden-xs">
                    <a href="#"><span>Welcome, </span><b>{{ Auth::user()->first_name ." ". Auth::user()->last_name  }}</b></a>
                    <div class="logout-wrap">
                        <a href="{{ url('/logout') }}"><span> </span>log out</a>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6 menu-block visible-xs">
                    <a href="javascript:void(0)" id="my-button"><span>Welcome, </span><b>{{ Auth::user()->first_name ." ". Auth::user()->last_name  }}</b></a>
                    <div class="logout-wrap">
                        <a href="{{ url('/logout') }}"><span> > </span>log out</a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="menu-box">
                    <div class="main-menu">
                        <ul class="main-navigation">
                            <li class="{{ Request::is('dashboard') ? 'active' : '' }}"><a href="{{ route('dashboard') }}">dashboard</a></li>
                            <li class="{{ Request::is('properties') ? 'active' : '' }}"><a href="{{ route('properties') }}">properties</a></li>
                            <li class="{{ Request::is('invoices') ? 'active' : '' }}"><a href="{{ route('invoices') }}">invoices</a></li>
                            <li class="{{ Request::is('notifications') ? 'active' : '' }}"><a href="{{ route('notifications') }}">notifications</a></li>
                            <li class="{{ Request::is('cards') ? 'active' : '' }}"><a href="{{ route('cards') }}">Cards</a></li>
                            <li class="{{ Request::is('membership') ? 'active' : '' }}"><a href="{{ route('membership') }}">Membership</a></li>
                            <li class="{{ Request::is('settings') ? 'active' : '' }}"><a href="{{ route('settings') }}">settings</a></li>
                            <li class="{{ Request::is('documents') ? 'active' : '' }}"><a href="{{ route('documents') }}">documents</a></li>
                        </ul>
                        <div class="menu-desc">
                            <figure>
                                <img src="{{ asset('') }}frontend/images/JSE-Icon-vector.svg" alt="jse logo" class="wow animatedslow flipInY"/>
                            </figure>
                            <a href="{{ url('/') }}" class="wow animatedslow fadeIn">home</a>
                        </div>
                        <a href="#" class="close-menu"></a>
                    </div>
                </div>
                <nav id="menu">
                    <ul>
                        <li class="{{ Request::is('dashboard') ? 'active' : '' }}"><a href="{{ route('dashboard') }}" class="menu-item">dashboard</a></li>
                        <li class="{{ Request::is('properties') ? 'active' : '' }}"><a href="{{ route('properties') }}" class="menu-item">properties</a></li>
                        <li class="{{ Request::is('invoices') ? 'active' : '' }}"><a href="{{ route('invoices') }}" class="menu-item">invoices</a></li>
                        <li class="{{ Request::is('notifications') ? 'active' : '' }}"><a href="{{ route('notifications') }}" class="menu-item">notifications</a></li>
                        <li class="{{ Request::is('cards') ? 'active' : '' }}"><a href="{{ route('cards') }}" class="menu-item">cards</a></li>
                        <li class="{{ Request::is('membership') ? 'active' : '' }}"><a href="{{ route('membership') }}">Membership</a></li>
                        <li class="{{ Request::is('settings') ? 'active' : '' }}"><a href="{{ route('settings') }}" class="menu-item">settings</a></li>
                        <li class="{{ Request::is('documents') ? 'active' : '' }}"><a href="{{ route('documents') }}" class="menu-item">documents</a></li>
                        <li class="{{ Request::is('dashboard') ? 'active' : '' }}"><a href="{{ url('/') }}" class="menu-item">Home</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <!--******************* Banner Section Start ******************-->
    <div class="banner-desc">
        <h4 class="wow animatedslow fadeIn">CLIENT PORTAL</h4>
    </div>
    <!--******************* Banner Section End ******************-->
</section>

