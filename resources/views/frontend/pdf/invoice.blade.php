<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>JSE Property Management</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
{{-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"> --}}
<link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{ asset('css/invoice.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-md page-header-fixed page-sidebar-closed-hide-logo ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE CONTENT-->
            <div class="portlet light">
                <div class="portlet-body">
                    <div class="invoice">
                        <div class="row " style="padding-bottom: 100px !important;">
                            <div class="col-xs-12 invoice-logo-space">
                                <img src="{{ asset('images/logo_black.png') }}" class="img-responsive " alt=""/>
                                <p class="pull-right">
                                     #{{ $invoice->id }} / {{ $invoice->duedate->format('d-m-Y') }}
                                </p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr/>

                        <div class="row">
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <h3>Client:</h3>
                                <ul class="list-unstyled">
                                    <li>
                                         {{ $user->first_name }} {{ $user->last_name }}
                                    </li>
                                    <li>
                                         {{ $user->email }}
                                    </li>
                                    <li>
                                         {{ $user->phone_number }}
                                    </li>
                                    <li>
                                         {{ $user->address }}
                                    </li>
                                    <li>
                                         {{ $user->city }}
                                    </li>
                                    <li>
                                         {{ $user->postcode }}
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <h3>Reference:</h3>
                                <ul class="list-unstyled">
                                    <li>
                                         {{ $invoice->reference }}
                                    </li>
                                    <li>
                                         {{ $invoice->property ? $invoice->property->address : '' }}
                                    </li>
                                    <li>
                                         {{ $invoice->property ? $invoice->property->postcode : '' }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>
                                         Postcode
                                    </th>
                                    <th class="hidden-480">
                                         Property
                                    </th>
                                    <th class="hidden-480">
                                         Reference
                                    </th>
                                    <th class="hidden-480">
                                         Cost
                                    </th>
                                    <th>
										On
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                         {{ $invoice->property ? $invoice->property->postcode : '' }}
                                    </td>
                                    <td class="hidden-480">
                                         {{ $invoice->property ? $invoice->property->address : '' }}
                                    </td>
                                    <td class="hidden-480">
                                         {{ $invoice->reference }}
                                    </td>
                                    <td class="hidden-480">
                                         £{{ $invoice->value }}
                                    </td>
                                    <td>
                                         {{ $invoice->created_at->format('d-m-Y') }}
                                    </td>
                                    <td>
                                         {{ $invoice->status }}
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

</body>
<!-- END BODY -->
</html>
