@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('userinvoice') !!}
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calculator font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{$headTitle}}</span>
                </div>
                <div class="tools">
                </div>
                <div class="actions">
                    @if (in_array('add', $permissions))
                        <a href="{{route('userinvoice.createinvoice')}}" class="btn btn-circle blue tooltips" data-container="body" data-placement="top" data-original-title="Upload Invoice"> Upload Invoice
                            <i class="fa fa-plus"></i>
                        </a>    

                        <a href="{{route('userinvoice.create')}}" class="btn btn-circle green tooltips" data-container="body" data-placement="top" data-original-title="Generate Invoice"> Generate Invoice
                            <i class="fa fa-plus"></i>
                        </a>
                    @endif
                </div>
            </div>
            @if (in_array('delete', $permissions))
                <a href="{{ route('userinvoice.destroy',0) }}" name="del_select" id="del_select" class="btn btn-sm btn-danger btn-circle delete_all_link" style="margin-left: 20px;">Delete Selected
                    <i class="fa fa-trash  fa-lg"></i>
                </a>
            @endif
            <div class="portlet-body">
                <div class="table-container">
                    <table class="table table-striped table-bordered table-hover" id="table_DT">
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(function(){
        var table = $('#table_DT');

        oTable = table.dataTable({
            "processing": true,
            "serverSide": true,
            "language": {
                "aria": {
                    "sortAscending": ": click to sort column ascending",
                    "sortDescending": ": click to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            "columns": [
                @if (in_array('delete', $permissions))
                    { "title": "<input type='checkbox' class='all_select'>" ,"data": "checkbox","width":"3%",searchble: false, sortable:false},
                @endif
                { "title": "Updated_at" ,"data": "updated_at", visible:false},
                { "title": "Name" ,"data": "first_name"},
                { "title": "Reference" ,"data": "reference"},
                { "title": "Amount" ,"data": "value"},
                { "title": "Status" ,"data": "status"},
                { "title": "Payment" ,"data": "payment_type"},
                @if (in_array('view', $permissions) || in_array('edit', $permissions) || in_array('delete', $permissions))
                    { "title": "Action" ,"data": "action", searchble: false, sortable:false }
                @endif
            ],
            responsive: false,
            "order": [
                @if (in_array('delete', $permissions))
                    [1, 'desc']
                @else
                    [0, 'desc']
                @endif
            ],
            "lengthMenu": [
                [10, 20, 50,100],
                [10, 20, 50,100]
            ],
            "pageLength": 10,
            "ajax": {
                "url": "{{route('userinvoice.listing')}}", // ajax source
            },
            drawCallback: function( oSettings ) {
                $('.make-switch').bootstrapSwitch();
                $('.make-switch').bootstrapSwitch('onColor', 'success');
                $('.make-switch').bootstrapSwitch('offColor', 'danger');
            },
            "dom": "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        });
    });
</script>
@endpush
