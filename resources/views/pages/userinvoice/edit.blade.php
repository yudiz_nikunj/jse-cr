@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('edit_userinvoice', $userinvoice) !!}
@endsection

@section('content')
 <div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-tag font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{$headTitle}}</span>
                </div>
            </div>
            <div class="portlet-body">
                <form id="frminvoice" class="form-horizontal" role="form" method="post" action="{{ route('userinvoice.update',$userinvoice->id) }}">
                    
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                   <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="status" class="col-md-2 control-label">Select Payment Status{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <select name="status" id="status" class="form-control" >
                                <option value=''>Select Status</option>
                                <option value='pending'>Pending</option>
                                <option value='paid'>Paid</option>
                            </select>
                            @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
                        <label for="notes" class="col-md-2 control-label">Notes</label>
                        <div class="col-md-4">
                            <textarea class="form-control" name="notes">{{ $userinvoice->notes }}</textarea>
                            @if ($errors->has('notes'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('notes') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn green">Submit</button>
                            <a href="{{route('userinvoice.index')}}" class="btn red">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: SAMPLE FORM PORTLET -->
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    $("#frminvoice").validate({
        rules: {
            status:{
                required:true,
            },
        },
        messages: {
            status:{
                required:"@lang('validation.required',['attribute'=>'status'])",
            }
        },
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function (element) {
           $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
           $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                  error.appendTo('.a');
            }else{
                if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            }
        }
    });
    $(document).on('submit','#frminvoice',function(){
        if($("#frminvoice").valid()){
            return true;
        }else{
            return false;
        }
    });
});

</script>
@endpush