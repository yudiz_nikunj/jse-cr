@if (in_array('view', $permissions) || in_array('edit', $permissions) || in_array('delete', $permissions))
	
	@if (in_array('view', $permissions))
		<a href="{{ route($routeName.'.show', $userinvoice->id) }}" title="View" class="btn btn-success btn-xs">View</a>
	@endif
	
	@if (in_array('edit', $permissions) && $userinvoice->payment_type == 'offline' && $userinvoice->status == 'pending')
		<a href="{{ route($routeName.'.edit', $userinvoice->id) }}" title="Edit" class="btn btn-warning btn-xs">Edit</a>
	@endif
	
	@if (in_array('delete', $permissions))
		<a title="Delete" href="{{ route($routeName.'.destroy', $userinvoice->id) }}" class="btn btn-danger btn-xs act-delete" data-id="{{$userinvoice->id}}">Delete</a>
	@endif
	
@endif