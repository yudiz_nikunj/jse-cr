@extends('layouts.app')
@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('upload_userinvoice') !!}
@endsection

@section('content')
 <div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{$headTitle}}</span>
                </div>
            </div>
            <div class="portlet-body">
                <form id="frmuploadinvoice" class="form-horizontal" role="form" method="POST" action="{{ route('userinvoice.uploadinvoice') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                        <label for="user_id" class="col-md-2 control-label">Select Counsumer{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <select name="user_id" id="user_id" class="form-control">
                                <option value="">Select a Consumer</option>
                                @foreach ($users as $user)
                                    <option value="{{$user->id}}" {{ (old('user_id') == $user->id) ? 'selected' : '' }}>{{$user->first_name .' '.$user->last_name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('user_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('user_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                        <label for="file" class="col-md-2 control-label">Upload File{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-upload"></i>
                                <input type="file" name="file" id="file" class="form-control" />
                                @if ($errors->has('file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    


                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn green">Submit</button>
                            <a href="{{route('invoice.index')}}" class="btn red">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: SAMPLE FORM PORTLET -->
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {

    

    $("#frmuploadinvoice").validate({
        rules: {
            user_id:{required:true},
            file:{
                required:true,
                accept:'pdf'
            },
        },
        messages: {
            user_id:{
                required:"@lang('validation.required',['attribute'=>'user'])"
            },
            file:{
                required:"@lang('validation.required',['attribute'=>'file'])",
                accept:"@lang('validation.mimetypes',['attribute'=>'file','value'=>'.pdf'])"
            },
        },
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function (element) {
           $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
           $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                  error.appendTo('.a');
            }else{
                if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            }
        }
    });
    $(document).on('submit','#frmuploadinvoice',function(){
        if($("#frmuploadinvoice").valid()){
            return true;
        }else{
            return false;
        }
    });
});

</script>

@endpush
