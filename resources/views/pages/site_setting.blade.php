@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('general_setting') !!}
@endsection

@section('content')

{{-- @foreach ($settings as $setting)
        {{$setting->label}}
@endforeach --}}
 <div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">General Settings</span>
                </div>
            </div>
            <div class="portlet-body">
                <form class="form-horizontal" id="form_settings" role="form" method="POST" action="{{ route('settings.changesetting') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    
                    @foreach ($settings as $setting)
                        @include('shared.setting',compact('setting'))
                    @endforeach 
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn green">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: SAMPLE FORM PORTLET -->
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function(){   
    jQuery.validator.addMethod("extension", function(value, element, param) {
            param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g";
            return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
        }, $.validator.format("Only jpg|jpeg|png allowed."));

    $.validator.addClassRules('email_class', {
        email:true
    });
    $.validator.addClassRules('img_class', {
       extension:true
    });
});
$(document).on('submit','#form_settings', function(e){
        $("#form_settings").validate({
            ignore:[],
            errorClass: 'help-block',
            errorElement: 'span',
            highlight: function (element) {
               $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorPlacement: function (error, element) {
                if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            }
        });
        if($("#form_settings").valid()){
            return true;
        }else{
            return false;
        }
    });
</script>
@endpush