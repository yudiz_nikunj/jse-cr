@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('my_profile') !!}
@endsection

@section('content')
 <div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet bordered">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="{{ !empty(Auth::user()->profile_photo) ? asset('storage/'.Auth::user()->profile_photo) : asset('images/default_profile.jpg') }}" class="img-responsive" alt="">
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> {{$user->first_name}} </div>
                    
                </div>
                <!-- END SIDEBAR USER TITLE -->
            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_1_1">
                                    <form role="form" id="frmProfile" method="POST" action="{{ route('editProfile') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                            <label class="control-label">{!! $mend_sign !!}First Name</label>
                                            <div class="input-icon">
                                                <i class="fa fa-font"></i>
                                                <input type="text" placeholder="Enter Name" name="first_name" id="first_name" class="form-control" value="{{old('first_name',$user->first_name)}}" maxlength="50" /> 
                                                @if ($errors->has('first_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                            <label class="control-label">{!! $mend_sign !!}Last Name</label>
                                            <div class="input-icon">
                                                <i class="fa fa-font"></i>
                                                <input type="text" placeholder="Enter Name" name="last_name" id="last_name" class="form-control" value="{{old('last_name',$user->last_name)}}" maxlength="50" /> 
                                                @if ($errors->has('last_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label class="control-label">{!! $mend_sign !!}Email</label>
                                            <div class="input-icon">
                                                <i class="fa fa-envelope"></i>
                                                <input type="email" placeholder="Enter E-mail Address" name="email" id="email" class="form-control" value="{{old('email',$user->email)}}" maxlength="150" /> 
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                            <label class="control-label">{!! $mend_sign !!}Contact Number</label>
                                            <div class="input-icon">
                                                <i class="fa fa-phone"></i>
                                                <input type="text" placeholder="Enter Contact No" name="phone_number" id="phone_number" class="form-control" value="{{old('phone_number',$user->phone_number)}}" maxlength="14" /> 
                                                @if ($errors->has('phone_number'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('profile_photo') ? ' has-error' : '' }}">
                                            <label class="control-label">Profile Picture</label>
                                            <div class="input-icon">
                                                <i class="fa fa-upload"></i>
                                                <input type="file" name="profile_photo" id="profile_photo" class="form-control" /> 
                                                @if ($errors->has('profile_photo'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('profile_photo') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="margiv-top-10">
                                            <button type="submit" class="btn green"> Save Changes </button>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/jquery.sparkline.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/profile.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script type="text/javascript">
    $('#phone_number').mask('(00000) 000000');

    $(function(){
        $('#frmProfile').validate({
            errorElement: 'span',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                name:{
                    required:true,
                    maxlength:50
                },
                email:{
                    required:true,
                    email:true,
                    pattern: /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
                    remote: {
                        url: "{!! url('checkUniqueEmail')!!}",
                        type: "post",
                        data: {
                            _token: function() {
                                return "{{csrf_token()}}"
                            },
                            email: function(){
                                return $("#email").val();
                            },
                            id: function(){
                                return {{$user->id}}
                            }
                        }
                    },
                    maxlength:150,
                },
                phone_number:{
                    required:true,
                    minlength:10,
                    maxlength:16
                },
                profile_photo:{
                    accept:'jpg|png|jpeg'
                }
            },
            messages: {
                name:{
                    required:"@lang('validation.required',['attribute'=>'name'])",
                    maxlength:50
                },
                email:{
                    required:"@lang('validation.required',['attribute'=>'email'])",
                    email: "@lang('validation.email', ['attribute'=>'email address'])",
                    pattern: "@lang('validation.email', ['attribute'=>'email address'])",
                    remote:"@lang('validation.exists',['attribute'=>'email'])",
                    maxlength:"@lang('validation.max.string',['attribute'=>'email','max'=>150])"
                },
                phone_number:{
                    required:"@lang('validation.required',['attribute'=>'phone number'])",
                    minlength:"@lang('validation.min.string',['attribute'=>'phone number','min'=>10])",
                    maxlength:"@lang('validation.max.string',['attribute'=>'phone number','max'=>16])"
                },
                profile_photo:{
                    accept:"@lang('validation.mimetypes',['attribute'=>'profile photo','value'=>'jpg|png|jpeg'])"
                }
            },
            errorClass: 'help-block',
            errorElement: 'span',
            highlight: function (element) {
               $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
               $(element).closest('.form-group').removeClass('has-error');
            },
            errorPlacement: function (error, element) {
                if (element.attr("type") == "radio") {
                      error.appendTo('.a');
                }else{
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            }
        });
        $(document).on('submit','#frmProfile',function(){
        if($("#frmProfile").valid()){
            return true;
        }else{
            return false;
        }
    }); 
    });
</script>
@endpush