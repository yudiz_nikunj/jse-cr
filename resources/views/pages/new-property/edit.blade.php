@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('edit_new_property', $new_property) !!}
@endsection
@push('page_css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.multiselect.css') }}">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/dropzone.css') }}" rel="stylesheet" type="text/css">    
    <style type="text/css">
        .dz-size,.dz-filename{
            display: none
        }
        #map {
          height: 400px;
        }

        .pac-card {
          background-color: #fff;
          border: 0;
          border-radius: 2px;
          box-shadow: 0 1px 4px -1px rgba(0, 0, 0, 0.3);
          margin: 10px;
          padding: 0 0.5em;
          font: 400 18px Roboto, Arial, sans-serif;
          overflow: hidden;
          font-family: Roboto;
          padding: 0;
        }

        #pac-container {
          padding-bottom: 12px;
          margin-right: 12px;
        }

        .pac-controls {
          display: inline-block;
          padding: 5px 11px;
        }

        .pac-controls label {
          font-family: Roboto;
          font-size: 13px;
          font-weight: 300;
        }

        #pac-input {
          background-color: #fff;
          font-family: Roboto;
          font-size: 15px;
          font-weight: 300;
          margin-left: 12px;
          padding: 0 11px 0 13px;
          text-overflow: ellipsis;
          width: 400px;
        }

        #pac-input:focus {
          border-color: #4d90fe;
        }
        .dz-image img{
            height: 100%;
            width: 100%;
            object-fit: cover;
        }
    </style>
@endpush
@section('content')
 <div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{$headTitle}}</span>
                </div>
            </div>
            <div class="portlet-body">
                 <form id="frmproperty" class="form-horizontal" role="form" method="post" action="{{ route('new-property.update',$new_property->id) }}" enctype="multipart/form-data">
                    
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    
                     <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-2 control-label">Name{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-header"></i>
                                <input type="text" placeholder="Enter Name" name="name" id="name" class="form-control" value="{{old('name',$new_property->name)}}"  />
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                        <label for="price" class="col-md-2 control-label">Price{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-header"></i>
                                <input type="number" placeholder="Enter Price" name="price" id="price" class="form-control" value="{{old('price',$new_property->price)}}"  />
                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('bedroom') ? ' has-error' : '' }}">
                        <label for="bedroom" class="col-md-2 control-label">No of Bedroom{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-header"></i>
                                <input type="number" placeholder="Enter No of Bedroom" name="bedroom" id="bedroom" class="form-control" value="{{old('bedroom',$new_property->bedroom)}}"  />
                                @if ($errors->has('bedroom'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bedroom') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                     <div class="form-group{{ $errors->has('bathroom') ? ' has-error' : '' }}">
                        <label for="bathroom" class="col-md-2 control-label">No of Bathroom{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-header"></i>
                                <input type="number" placeholder="Enter No of Bathroom" name="bathroom" id="bathroom" class="form-control" value="{{old('bathroom',$new_property->bathroom)}}"  />
                                @if ($errors->has('bathroom'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bathroom') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                     <div class="form-group{{ $errors->has('livingroom') ? ' has-error' : '' }}">
                        <label for="livingroom" class="col-md-2 control-label">No of Livingroom{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-header"></i>
                                <input type="number" placeholder="Enter No of Livingroom" name="livingroom" id="livingroom" class="form-control" value="{{old('livingroom',$new_property->livingroom)}}"  />
                                @if ($errors->has('livingroom'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('livingroom') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }} location">
                        <label for="location" class="col-md-2 control-label">Location{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                           
                                <select name="location" id="location"  class="form-control 3col active"  data-error-container="#location-error">
                                    <option value="">Select Location</option>
                                    <option value="North London" {{ $new_property->location =="North London" ? "selected" : '' }}>North London</option>
                                    <option value="East London" {{ $new_property->location =="East London" ? "selected" : '' }}>East London</option>
                                    <option value="Central London" {{ $new_property->location =="Central London" ? "selected" : '' }}>Central London</option>
                                </select>
                                @if ($errors->has('location'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                @endif
                                <span id="location-error"></span>
                            
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('property_type') ? ' has-error' : '' }} property_type">
                        <label for="property_type" class="col-md-2 control-label">Property Type{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            
                                <select name="property_type" id="property_type"  class="form-control 3col active"  data-error-container="#property_type-error">
                                    <option value="">Select Property Type</option>
                                    <option value="Apartment" {{ $new_property->property_type =="Apartment" ? "selected" : '' }}>Apartment</option>
                                    <option value="Flat" {{ $new_property->property_type =="Flat" ? "selected" : '' }}>Flat</option>
                                    <option value="Townhouse" {{ $new_property->property_type =="Townhouse" ? "selected" : '' }}>Townhouse</option>
                                </select>
                                @if ($errors->has('property_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('property_type') }}</strong>
                                    </span>
                                @endif
                                <span id="property_type-error"></span>
                            
                        </div>
                    </div>
                    
                    

                    <div class="form-group">
                        <label class="col-md-2 control-label">Amenities</label>
                        <div class="col-md-8">                            
                            @foreach($amenities as $amnt)
                                <input class="form-control" type="checkbox" name="amenities[]" value="{{  $amnt->id }}" 
                                {{ in_array($amnt->id,$selectedAminities) ? 'checked' : '' }}
                                > {{ $amnt->title }}
                            @endforeach
                        </div>
                    </div>

                     <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description" class="col-md-2 control-label">Description{!! $mend_sign !!}</label>
                        <div class="col-md-8">                            
                            <textarea type="text" placeholder="description" name="description" id="description" class="form-control" rows="8" />{{old('description',$new_property->description)}}</textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif                            
                        </div>
                    </div>

                    <div class="form-group">                         
                        <label for="Images" class="col-md-2 control-label">Images</label>                                         
                        <div class="col-md-8">
                            <div id="dropzone" class="dropzone">

                            </div> 
                        </div>
                    </div>

                    <div class="form-group">                         
                        <label for="Images" class="col-md-2 control-label">Select Location</label>                                         
                        <div class="col-md-8">
                            <input
                              id="pac-input"
                              class="controls form-control"
                              type="text"
                              placeholder="Search Box"
                              name="address"
                              value="{{ $new_property->address }}"
                            />
                            <div id="map"></div>
                        </div>
                    </div>

                    <input type="hidden" name="latitude" id="latitude" value="{{ $new_property->latitude }}">
                    <input type="hidden" name="longitude" id="longitude" value="{{ $new_property->longitude }}">  

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn green">Submit</button>
                            <a href="{{route('new-property.index')}}" class="btn red">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: SAMPLE FORM PORTLET -->
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    function initAutocomplete() {
  const map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: {{ config("settings.lat") }}, lng: {{ config("settings.long") }} },
    zoom: 13,
    mapTypeId: "roadmap",
  });
  // Create the search box and link it to the UI element.
  const input = document.getElementById("pac-input");
  const searchBox = new google.maps.places.SearchBox(input);

  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  // Bias the SearchBox results towards current map's viewport.
  map.addListener("bounds_changed", () => {
    searchBox.setBounds(map.getBounds());
  });

  let markers = [];

  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener("places_changed", () => {
    const places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach((marker) => {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    const bounds = new google.maps.LatLngBounds();

    places.forEach((place) => {
      if (!place.geometry || !place.geometry.location) {
        console.log("Returned place contains no geometry");
        return;
      }

      const icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25),
      };

      // Create a marker for each place.
      markers.push(
        new google.maps.Marker({
          map,
          icon,
          title: place.name,
          position: place.geometry.location,
        })
      );

      const latitude = place.geometry.location.lat();
      const longitude = place.geometry.location.lng();

      $("#latitude").val(latitude)
      $("#longitude").val(longitude)
      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
}
</script>
<script  src="{{ asset('js/jquery.multiselect.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/dropzone.js') }}" type="text/javascript"></script>
<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-uSiExC_Ct1bzRRAaLDhC3IcD10Yh-7c&callback=initAutocomplete&libraries=places&v=weekly"
      async
    ></script>
<script type="text/javascript">
$(document).ready(function() {
    
    $('body').on('change', '#user_id', function(){
        $('#user_id').valid();
    }); 
    $("#frmproperty").validate({
        ignore:[],
        rules: {
            name:{ 
                required:true 
            },
            price:{
                required:true,
            },
            bedroom:{ 
                required:true 
            },
            bathroom:{ 
                required:true 
            },
            livingroom:{ 
                required:true 
            },
            location:{ 
                required:true 
            },
            property_type:{ 
                required:true 
            },
            // description:{ 
            //     required:true 
            // },
            
        },
        messages: {
            name:{
                required:"@lang('validation.required',['attribute'=>'name'])"
            },
            price:{
                required:"@lang('validation.required',['attribute'=>'price'])"
            },
            bedroom:{
                required:"@lang('validation.required',['attribute'=>'bedroom'])"
            },
            bathroom:{
                required:"@lang('validation.required',['attribute'=>'bathroom'])"
            },
            livingroom:{
                required:"@lang('validation.required',['attribute'=>'livingroom'])"
            },
            location:{
                required:"@lang('validation.required',['attribute'=>'location'])"
            },
            property_type:{
                required:"@lang('validation.required',['attribute'=>'property type'])"
            },
            description:{
                required:"@lang('validation.required',['attribute'=>'description'])"
            },
            
        },
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function (element) {
           $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
           $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.attr("type") == "radio") {
                  error.appendTo('.a');
            }else{
                if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            }
        }
    });
    $(document).on('submit','#frmproperty',function(){
        if($("#frmproperty").valid()){
            if ($(this).valid()) {
                addOverlay();
                $("input[type=submit], input[type=button], button[type=submit]").prop("disabled", "disabled");
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    });

    $("div#dropzone").dropzone({
        url: '{{ route('new-property.uploadimage') }}',
        paramName: "item_image",
        uploadMultiple: false,
        parallelUploads: 50,
        addRemoveLinks: true,
        autoDiscover: false,
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        removedfile: function (file) {
            $('input[name="item_image[]"][value="' + file.id  + '"]').remove();
            $.post('{{ route('new-property.removeimage') }}', {removeImage: true, item_image: file.id ,_token:'{{ csrf_token() }}'}, function (data) {
            });
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function (file, responseText) {
            if (typeof (responseText) != "undefined") {
                $element = file.previewElement;
                var obj = jQuery.parseJSON(responseText);
                file.id = obj.fileID;
                $($element).find(".dz-remove").attr("id", obj.fileID);
                $('#frmproperty').append('<input type="hidden" name="item_image[]" value="' + obj.fileID + '" />');
            }
        }, init: function () {                      
            thisDropzone = this;
            $.get('{{ route('new-property.getimage',$new_property->id) }}', function (data) {
                if ($.trim(data) != "") {
                    $.each(data, function (key, value) {
                        var mockFile = {name: value.name, size: '',id:value.id};
                        thisDropzone.emit("addedfile", mockFile);
                        thisDropzone.emit("complete", mockFile);
                        thisDropzone.emit("thumbnail", mockFile, '{{ asset('storage/') }}/' + value.name);
                        thisDropzone.files.push(mockFile);
                    });
                }

            }, "json");
            
        }
    });
});

</script>

<script type='text/javascript' src='{{ asset('') }}tinymce/tinymce.min.js'></script>

<script type="text/javascript">
$(document).ready(function() {
    tinymce.init({ 
        selector:'textarea',
        plugins: [
          'advlist autolink link image lists charmap print preview hr anchor pagebreak',
          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
          'save table contextmenu directionality emoticons template paste textcolor'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
        menubar: "tools",
        height: 300,
        valid_elements : '*[*]',
        allow_html_in_named_anchor: true,
        allow_unsafe_link_target: true,
        convert_fonts_to_spans : false,
        entity_encoding : "raw",
        cleanup_on_startup: false,
        trim_span_elements: false,
        verify_html: false,
        cleanup: false,
        convert_urls: false,
        allow_script_urls: true,
        relative_urls : true,
        document_base_url : "{{ env('APP_URL') }}/jse",
        valid_children : "body[p,ol,ul]"
        + ",p[a|span|b|i|u|sup|sub|img|hr|#text]"
        + ",span[a|b|i|u|sup|sub|img|#text]"
        + ",a[p|figure|span|b|i|u|sup|sub|img|#text]"
        + ",b[span|a|i|u|sup|sub|img|#text]"
        + ",i[span|a|b|u|sup|sub|img|#text]"
        + ",sup[span|a|i|b|u|sub|img|#text]"
        + ",sub[span|a|i|b|u|sup|img|#text]"
        + ",li[span|a|b|i|u|sup|sub|img|ol|ul|#text]"
        + ",ol[li]"
        + ",ul[li]",
    });
});

</script>

@endpush
