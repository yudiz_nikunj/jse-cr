@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('view_invoice', $invoice) !!}
@endsection


@section('content')
 <div class="row">
     <div class="col-md-12">
         <div class="profile-sidebar">
         </div>
         <div class="profile-content">
             <div class="row">
                 <div class="col-md-12">
                     <div class="portlet light bordered">
                         <div class="portlet-title tabbable-line">
                             <div class="caption caption-md">
                                 <i class="icon-globe theme-font hide"></i>
                                 <span class="caption-subject font-blue-madison bold uppercase">View Profile</span>
                             </div>
                         </div>

                         <div class="portlet-body">
                          <div class="row">
                            <div class="col-md-9">
                                <div class="form-group clearfix">
                                    <label class="control-label bold col-md-3">Name :</label>
                                    <div class="col-md-9">{{ $invoice->user->first_name.' '.$invoice->user->last_name}}</div>
                                </div>

                                 <div class="form-group clearfix">
                                    <label class="control-label bold col-md-3">Address :</label>
                                    <div class="col-md-9">{{ $invoice->property ? $invoice->property->address : '-' }} 
                                    </div>
                                </div>
                               
                                <div class="form-group clearfix">
                                    <label class="control-label bold col-md-3">Reference :</label>
                                    <div class="col-md-9">{{ $invoice->reference}} 
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="control-label bold col-md-3">Amount :</label>
                                    <div class="col-md-9">{{ $invoice->value ? $invoice->value : '-'  }}</div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="control-label bold col-md-3">Status :</label>
                                    <div class="col-md-9">{{ $invoice->status }}</div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="control-label bold col-md-3">Payment Type :</label>
                                    <div class="col-md-9">{{ $invoice->payment_type }}</div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="control-label bold col-md-3">Notes :</label>
                                    <div class="col-md-9">{{ $invoice->notes ? $invoice->notes : '-'}}</div>
                                </div>

                                @if($invoice->isUploadInvoice())
                                    <div class="form-group clearfix">
                                        <label class="control-label bold col-md-3">Invoice View :</label>
                                        <div class="col-md-9"><a href="{{ asset('storage/app/public/'.$invoice->path) }}" target="_blank">Click Here</a></div>
                                    </div>
                                @endif    
                            </div>    
                          </div>  
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <!-- END PROFILE CONTENT -->
     </div>
 </div>
@endsection

@push('extra-js-scripts')
<script type="text/javascript">
  
</script>
@endpush
