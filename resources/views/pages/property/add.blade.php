@extends('layouts.app')
@section('title') {{$headTitle}} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('add_property') !!}
@endsection

@section('content')
 <div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{$headTitle}}</span>
                </div>
            </div>
            <div class="portlet-body">
                <form id="frmproperties" class="form-horizontal" role="form" method="POST" action="{{ route('property.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                        <label for="user_id" class="col-md-2 control-label">Select Counsumer{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <select name="user_id" id="user_id" class="form-control">
                                <option value="">Select a Consumer</option>
                                @foreach ($users as $user)
                                    <option value="{{$user->id}}" {{ (old('user_id') == $user->id) ? 'selected' : '' }}>{{$user->first_name.' '.$user->last_name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('user_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('user_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
                        <label for="postcode" class="col-md-2 control-label">Postcode{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-map"></i>
                                <input type="text" class="form-control" id="postcode" name="postcode" placeholder="ie: NW10 8GE" autofocus />
                                @if ($errors->has('postcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('postcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        <label for="address" class="col-md-2 control-label">Address{!! $mend_sign !!}</label>
                        <div class="col-md-4">
                            <div class="input-icon">
                                <i class="fa fa-map-marker"></i>
                                <input id="address-search" name="address" class="form-control" placeholder="Select your address" disabled />
                                <input type="hidden" id="user_location" value="" name="address"/>
                                <p><span id="msg_addSearch"></span></p>
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn green">Submit</button>
                            <a href="{{route('consumer.index')}}" class="btn red">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: SAMPLE FORM PORTLET -->
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {

        $("#frmproperties").validate({
            rules: {
                user_id:{required:true},
                postcode:{required:true},    
                address:{required:true},
            },
            messages: {
                user_id:{
                    required:"@lang('validation.required',['attribute'=>'user'])"
                },
                postcode:{
                    required:"@lang('validation.required',['attribute'=>'postcode'])"
                },
                address:{
                    required:"@lang('validation.required',['attribute'=>'address'])"
                }
            },
            errorClass: 'help-block',
            errorElement: 'span',
            highlight: function (element) {
               $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
               $(element).closest('.form-group').removeClass('has-error');
            },
            errorPlacement: function (error, element) {
                if (element.attr("type") == "radio") {
                      error.appendTo('.a');
                }else{
                    if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            }
        });
        $(document).on('submit','#frmproperties',function(){
            if($("#frmproperties").valid()){
                return true;
            }else{
                return false;
            }
        });
    });
</script>

<script type="text/javascript">
    $(document).on('keyup',"#postcode",function () {
        var searchContext = "",
            container = "",
            key = "{{ env('PCA_KEY') }}";

        $("#address-search").val($(this).val());
        $("#address-search").trigger('keydown');

        $("#address-search").autocomplete({
            source: function(request, response) {
                $("#user_location").val('');

                $.getJSON("https://services.postcodeanywhere.co.uk/Capture/Interactive/Find/v1.00/json3.ws?callback=?",
                {
                    Key: key,
                    Text: request.term,
                    Container: container,
                    Origin: 'GBR',
                    Countries: '',
                    Limit: '',
                    Language: 'en'
                },
                function (data) {
                    container = '';
                    response($.map(data.Items, function(suggestion) {
                        return {
                            label: suggestion.Text +' '+suggestion.Description,
                            value: "",
                            data: suggestion
                        }
                    }));
                });
            },
            select: function(event, ui) {
                var item = ui.item.data;
                var field = $(this);
                if (item.Type == 'Postcode') {
                    $('#postcode').val(item.Text);
                }
                if (item.Type == 'Address') {

                    $('#postcode').val(item.Description.substring(Number(item.Description.lastIndexOf(","))+2), item.Description.length);

                    populateAddress(item.Text +' '+item.Description)
                }else{

                    searchContext = item.Id;
                    window.setTimeout(function() {
                        container = item.Id;
                        field.autocomplete("search", item.Text);
                    });
                }
            },
            autoFocus: true,
            minLength: 1,
            delay: 100
        }).focus(function() {
            searchContext = "";
            container = "";
        });

        function populateAddress(address) {
            $("#user_location").val(address.replace(/\n/g, '<br/>'));
        }
    });
</script>

@endpush
