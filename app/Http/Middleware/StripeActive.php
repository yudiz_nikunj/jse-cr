<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class StripeActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->consumer_type == 'management'){
            
            if(Auth::user()->monthly == 0 && Auth::user()->yearly == 0){
                // echo "string";die;
                return $next($request);        
            }

            if (! $request->user()->subscribed('main')) {
                return redirect()->route('membership');
            }
        }else{
            if (in_array($request->path(), ['properties'])) {
                flash('Contact admin properties access.')->error();    
                return redirect('/dashboard');
            }
        } 
        return $next($request);
    }
}
