<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Invoice;
use App\Models\Enquiry;
use App\Models\Property;
use Illuminate\Support\Facades\Route;
use DB;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counts['consumer'] = User::where('user_type', 'user')->count();
        $counts['invoice']  = Invoice::count();
        $counts['enquiry']  = Enquiry::count();
        $counts['property'] = Property::count();
        return view("home", compact('counts'))->with('headTitle', 'Dashboard');
    }
}
