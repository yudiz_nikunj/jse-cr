<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;
use App\Models\Enquiry;
use Carbon\Carbon;

class EnquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.enquiry.list')->with('headTitle', 'Enquiry');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if(!empty($request->action) && $request->action == 'delete_all'){
            $content = ['status'=>204, 'message'=>"something went wrong"];
            Enquiry::destroy(explode(',',$request->ids));
            $content['status']=200;
            $content['message'] = "Enquiries deleted successfully.";
            return response()->json($content);
        }else{
            Enquiry::destroy($id);
            if(request()->ajax()){
                $content = array('status'=>200, 'message'=>"Enquiry deleted successfully.");
                return response()->json($content);
            }else{
                flash('Enquiry deleted successfully.')->success();
                return redirect()->route('enquiry.index');
            }
        }
    }

    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $count = Enquiry::where('id','>',0);

        if($search != ''){
            $count->where(function($query) use ($search){
                $query->where("name", "like", "%{$search}%");
                $query->orwhere("email", "like", "%{$search}%");
                $query->orwhere("telephone", "like", "%{$search}%");
                $query->orwhere("type", "like", "%{$search}%");
            });
        }
        $count = $count->count();

        $records["recordsTotal"] = $count;
        $records["recordsFiltered"] = $count;
        $records['data'] = array();
        $enquiries = Enquiry::offset($offset)->limit($limit)->orderBy($sort_column,$sort_order);

        if($search != ''){
            $enquiries->where(function($query) use ($search){
                $query->where("name", "like", "%{$search}%");
                $query->orwhere("email", "like", "%{$search}%");
                $query->orwhere("telephone", "like", "%{$search}%");
                $query->orwhere("type", "like", "%{$search}%");
            });
        }
        $enquiries = $enquiries->get();

        foreach ($enquiries as $enquiry) {

            $records['data'][] = [
                'checkbox'=>view('shared.checkbox')->with('id',$enquiry->id)->render(),
                'name'=>$enquiry->name,
                'email'=>$enquiry->email,
                'telephone'=>$enquiry->telephone,
                'enquiry'=>$enquiry->enquiry,
                'type'=>$enquiry->type,
                'action'=>view('shared.actions')->with('id', $enquiry->id)->render(),
            ];
        }

        return $records;
    }
}
