<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Amenities;
use App\Models\NewProperty;
use App\Models\NewPropertyImage;
use App\Models\PropertyAmenity;
use Illuminate\Http\Request;
use Storage;

class NewPropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.new-property.list')->with('headTitle', 'Property');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $amenities = Amenities::all();
        return view('pages.new-property.add',compact('amenities'))->with('headTitle', 'Property');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules = [
            'name' => 'required',
            'price' => 'required|numeric',
            'bedroom' => 'required|numeric',
            'bathroom' => 'required|numeric',
            'livingroom' => 'required|numeric',
            'location' => 'required',
            'property_type' => 'required',
            'description' => 'required'
        ];
        $this->validateForm($request->all(), $rules);
        $user_id = null;
       
        $property = new NewProperty;
        $property->name = $request->name;
        $property->price = $request->price;
        $property->bedroom = $request->bedroom;
        $property->bathroom = $request->bathroom;
        $property->livingroom = $request->livingroom;
        $property->location = $request->location;
        $property->property_type = $request->property_type;
        $property->description = $request->description;
        $property->address = $request->address;
        $property->longitude = $request->longitude;
        $property->latitude = $request->latitude;
        $property->slug = createSlug($request->name);
        $property->save();

        if(isset($request->amenities)){
            foreach ($request->amenities as $key => $value) {
                PropertyAmenity::create([
                    'amenity_id' => $value,
                    'property_id' => $property->id
                ]);
            }
        }

        if(isset($request->item_image)){
            foreach ($request->item_image as $key => $value) {
                $exists = Storage::exists($value); 
                $insArr=array();       
                if($exists){
                    $new=explode('/', $value);
                    Storage::move($value, 'item_image/'.$new[1]);
                    $insArr[]=array('property_id'=>$property->id,'name'=>'item_image/'.$new[1]);
                }  
                if(count($insArr)>0){
                    NewPropertyImage::insert($insArr);
                }
            }
        }
        
        flash('Property added successfully.')->success();
        return redirect()->route('new-property.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(NewProperty $new_property)
    {
        return view('pages.new-property.view',compact('new_property'))->with('headTitle', 'View Property');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(NewProperty $new_property)
    {   
        $amenities = Amenities::all();
        $selectedAminities =$new_property->amenities->pluck('id')->toArray();
        return view('pages.new-property.edit',compact('new_property','amenities','selectedAminities'))->with('headTitle', 'Edit Property');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (!empty($request->action) && $request->action == 'change_status') {
            $content  = ['status' => 204, 'message' => "something went wrong"];
            $property = NewProperty::find($id);
            if ($property) {
                $property->active = $request->value;
                if ($property->save()) {
                    $content['status']  = 200;
                    $content['message'] = "Status updated successfully.";
                }
            }
            return response()->json($content);
        } else {
            $rules = [
                'name' => 'required',
                'price' => 'required|numeric',
                'bedroom' => 'required|numeric',
                'bathroom' => 'required|numeric',
                'livingroom' => 'required|numeric',
                'location' => 'required',
                'property_type' => 'required',
                'description' => 'required'
            ];
            $this->validateForm($request->all(), $rules);
            $property = NewProperty::find($id);
            if ($property) {

                $property->name = $request->name;
                $property->price = $request->price;
                $property->bedroom = $request->bedroom;
                $property->bathroom = $request->bathroom;
                $property->livingroom = $request->livingroom;
                $property->location = $request->location;
                $property->property_type = $request->property_type;
                $property->description = $request->description;
                $property->address = $request->address;
                $property->longitude = $request->longitude;
                $property->latitude = $request->latitude;
                // $property->slug = createSlug($request->name,$id);
                $property->save();

                if(isset($request->amenities)){                   
                    $amtid = [];
                    foreach ($request->amenities as $key => $value) {                       
                        $amtid[] = $value;
                        $matchThese = ['amenity_id'=>$value,'property_id'=>$property->id];
                        PropertyAmenity::updateOrCreate($matchThese,['amenity_id'=>$value,'property_id'=>$property->id]);
                    }

                    PropertyAmenity::where([
                        'property_id' => $property->id
                    ])->whereNotIn('amenity_id',$amtid)->delete();

                }else{
                    PropertyAmenity::where([
                        'property_id' => $property->id
                    ])->delete();
                }

                
                if(isset($request->item_image)){
                    foreach ($request->item_image as $key => $value) {
                        $exists = Storage::exists($value); 
                        $insArr=array();       
                        if($exists){
                            $new=explode('/', $value);
                            Storage::move($value, 'item_image/'.$new[1]);
                            $insArr[]=array('property_id'=>$property->id,'name'=>'item_image/'.$new[1]);
                        }  
                        if(count($insArr)>0){
                            NewPropertyImage::insert($insArr);
                        }
                    }
                } 

                flash('Property updated successfully.')->success();
                return redirect()->route('new-property.index');
            } else {
                abort(404);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!empty($request->action) && $request->action == 'delete_all') {
            $content = ['status' => 204, 'message' => "something went wrong"];
            
            $propertys = NewProperty::whereIn('id',explode(',',$request->ids))->get();

            foreach ($propertys as $key => $prop) {
                Storage::delete($prop->path);
                $prop->delete();
            }
            
            $content['status']  = 200;
            $content['message'] = "Property deleted successfully.";
            return response()->json($content);
        } else {
            $property = NewProperty::find($id);
            $property->delete();

            if (request()->ajax()) {
                $content = array('status' => 200, 'message' => "Property deleted successfully.");
                return response()->json($content);
            } else {
                flash('Property deleted successfully.')->success();
                return redirect()->route('new-property.index');
            }
        }
    }

    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $propertys = NewProperty::where('id','<>',0);

            if ($search != '') {
                $propertys->where(function ($query) use ($search) {
                    $query->where("name", "like", "%{$search}%")
                    ->orWhere("location", "like", "%{$search}%")
                    ->orWhere("property_type", "like", "%{$search}%")
                    ->orWhere("price", "like", "%{$search}%");
                });
            }                            
       
        $count = $propertys->count();

        $records["recordsTotal"] = $count;
        $records["recordsFiltered"] = $count;
        $records['data'] = array();
        $propertys = $propertys->orderBy($sort_column, $sort_order)->offset($offset)->limit($limit)->get();

        foreach ($propertys as $prop) {

            $params = array(
                'url'       => route('new-property.update', $prop->id),
                'checked'   => ($prop->active == 1) ? "checked" : "",
                'getaction' => '',
                'class'     => '',
            );

            $records['data'][] = [
                'checkbox'=>view('shared.checkbox')->with('id', $prop->id)->render(),
                'price'=>$prop->price,
                'location'=>$prop->location,
                'name'=>$prop->name,
                'property_type'=>$prop->property_type,
                'price'=>$prop->price,
                'active' => view('shared.switch')->with(['params' => $params, 'id' => $prop->id])->render(),
                'action'=>view('shared.actions')->with('id', $prop->id)->render(),
            ];
        }
        return $records;
    }

    function uploadItemImage(Request $request){     

        $path ='';
        if( $request->hasFile('item_image') ) {
            $path = $request->file('item_image')->store('temp_item_image');
        }
        $imgpath["fileID"] =$path;
        return json_encode($imgpath);
    }

    function removeItemImage(Request $request){     
        if(is_numeric($request->item_image)){
            $itemImg=NewPropertyImage::where('id',$request->item_image)->first();

            if( $itemImg->path )
                    Storage::delete($itemImg->path);

            $itemImg->delete();
            return 1;
        }
        $item_image=$request->item_image;
        $exists = Storage::exists($item_image);        
        if($exists){
            Storage::delete($item_image);
        }   
        return 1;    
    }

    function getItemImage(Request $request){
       return  NewPropertyImage::where('property_id',$request->property_id)->get();
    }
}
