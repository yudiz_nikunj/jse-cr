<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\Signup;
use App\Mail\Consumernotify;
use App\Models\Notification;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Stripe\Stripe;

class ConsumerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.consumer.list')->with('headTitle', 'Consumers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.consumer.add')->with('headTitle', 'Consumers');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'consumer_type' => 'required',
            'first_name'    => 'required|max:50', /*|unique:category,name*/
            'last_name' => 'required|max:50',
            'email'         => 'required|max:50|unique_email',
            'password'      => 'required|max:50',
            'phone_number'    => 'required|min:9|max:15',   
            'profile_photo' => 'sometimes|image|mimes:jpeg,jpg,png',
        ];
        
        if($request->consumer_type == 'management'){
            $rules['monthly'] = 'required|numeric';
            $rules['yearly']  = 'required|numeric';
        }


        $this->validateForm($request->all(), $rules);

        if (($request->monthly == 0 && $request->yearly != 0) || 
                ($request->monthly != 0 && $request->yearly == 0))
        {        
            flash('monthly or yearly fees not allowed 0.')->error();
            return redirect()->route('consumer.create');
        }


        $consumer = new User;
        if ($request->hasFile('profile_photo')) {
            $consumer->profile_photo = $request->file('profile_photo')->store('consumers');
        }

        $consumer->first_name = Input::get('first_name');
        $consumer->last_name  = Input::get('last_name');
        $consumer->email      = Input::get('email');
        $consumer->password   = Hash::make(Input::get('password'));
        $consumer->phone_number = Input::get('phone_number');
        $consumer->consumer_type = Input::get('consumer_type');
       
        if($request->consumer_type == 'management'){
            $consumer->monthly    = Input::get('monthly');
            $consumer->yearly     = Input::get('yearly');
        }

        if ($consumer->save()) {
            try{
                Mail::to($request->email)->send(new Signup($request));

                Stripe::setApiKey(config("services.stripe.secret"));

                $customer = \Stripe\Customer::create(array(
                    "email"    => $consumer->email,
                    "metadata" => [
                        "First Name" => $consumer->first_name,
                        "Last Name"  => $consumer->last_name,
                        "JSE User ID"  => $consumer->id,
                    ],
                ));

                if ($customer) {
                    $consumer->stripe_id = $customer->id;
                    $consumer->save();

                    if($request->consumer_type == 'management'){    
                       
                       $planMonthly = \Stripe\Plan::create(
                            array(
                          "amount" => ($consumer->monthly * 100),
                          "interval" => "month",
                          "name" => $consumer->first_name." ".$consumer->last_name."'s Monthly Plan",
                          "currency" => config("services.stripe.currency"),
                          "id" => "user-".$consumer->id."-monthly")
                        );

                        $planYearly = \Stripe\Plan::create(
                            array(
                          "amount" => ($consumer->yearly * 100),
                          "interval" => "year",
                          "name" => $consumer->first_name." ".$consumer->last_name."'s Yearly Plan",
                          "currency" => config("services.stripe.currency"),
                          "id" => "user-".$consumer->id."-yearly")
                        );
                    }    

                    /*
                        \Stripe\Stripe::setApiKey(config("services.stripe.secret"));
                        $customer = \Stripe\Customer::retrieve($user->stripe_id);
                    */

                    /*$card = $customer->sources->create(
                        array(
                            "source" => [
                                'object'    => "card",
                                "exp_month" => 12,
                                "exp_year"  => 2020,
                                "number"    => 4242424242424242,
                                "cvc"       => 121,
                                "name"      =>$consumer->first_name." ".$consumer->last_name
                            ],
                        )
                    );*/

                    // YOUR CODE (LATER): When it's time to charge the customer again, retrieve the customer ID.
                    /*$charge = \Stripe\Charge::create(array(
                      "amount" => 1500, // $15.00 this time
                      "currency" => "gbp",
                      "customer" => $customer->id
                    ));

                    dd($charge);*/
                }

                flash('Consumer added successfully.')->success();
                return redirect()->route('consumer.index');
            }catch(\Exception $e){
                flash('Internal Server Error.')->error();
                return redirect()->route('consumer.index');
            }    
        }

        flash('Internal Server Error.')->error();
        return redirect()->route('consumer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $consumer = User::findOrFail($id);
            return view('pages.consumer.edit')->with('consumer', $consumer)->with('headTitle', 'Edit Consumer');
        } catch (ModelNotFoundException $e) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!empty($request->action) && $request->action == 'change_status') {
            $content  = ['status' => 204, 'message' => "something went wrong"];
            $consumer = User::find($id);
            if ($consumer) {
                $consumer->active = $request->value;
                if ($consumer->save()) {
                    $content['status']  = 200;
                    $content['message'] = "Status updated successfully.";
                }
            }
            return response()->json($content);
        } else {
            $rules = [
                'first_name'    => 'required|max:50', /*|unique:category,name*/
                'last_name' => 'required|max:50',
                /*'email' => 'required|max:50|unique_email',
                'password' => 'required|max:50',*/
                'phone_number'    => 'required|min:9|max:15',
                'profile_photo' => 'sometimes|image|mimes:jpeg,jpg,png',
            ];
            $this->validateForm($request->all(), $rules);

            $consumer = User::find($id);

            if (($request->monthly == 0 && $request->yearly != 0) || 
                ($request->monthly != 0 && $request->yearly == 0))
            {          
                flash('monthly or yearly fees not allowed 0.')->error();
                return redirect()->route('consumer.edit', $id);
            }


            if ($request->hasFile('profile_photo')) {
                Storage::delete($consumer->profile_photo);
                $consumer->profile_photo = $request->file('profile_photo')->store('consumers');
            }

            if ($consumer) {
                $consumer->first_name = Input::get('first_name');
                $consumer->last_name  = Input::get('last_name');
                /*$consumer->email  = Input::get('email');
                $consumer->password  = Hash::make(Input::get('password'));*/
                $consumer->phone_number = Input::get('phone_number');
                $consumer->monthly    = Input::get('monthly');
                $consumer->yearly     = Input::get('yearly');
                $consumer->save();

                flash('Consumer updated successfully.')->success();
                return redirect()->route('consumer.index');
            } else {
                abort(404);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!empty($request->action) && $request->action == 'delete_all') {
            $content = ['status' => 204, 'message' => "something went wrong"];
            User::destroy(explode(',', $request->ids));
            $content['status']  = 200;
            $content['message'] = "Consumers deleted successfully.";
            return response()->json($content);
        } else {
            User::destroy($id);
            if (request()->ajax()) {
                $content = array('status' => 200, 'message' => "Consumer deleted successfully.");
                return response()->json($content);
            } else {
                flash('Consumer deleted successfully.')->success();
                return redirect()->route('consumer.index');
            }
        }
    }

    public function listing(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $users = User::where('user_type', 'user');

        if ($search != '') {
            $users->where(function ($query) use ($search) {
                $query->where("first_name", "like", "%{$search}%");
                $query->orwhere("last_name", "like", "%{$search}%");
                $query->orwhere("email", "like", "%{$search}%");
                $query->orwhere("consumer_type", "like", "%{$search}%");
                $query->orwhere("monthly", "like", "%{$search}%");
                $query->orwhere("yearly", "like", "%{$search}%");
            });
        }
        $count = $users->count();

        $records["recordsTotal"]    = $count;
        $records["recordsFiltered"] = $count;
        $records['data']            = array();
        

        $consumers = $users->offset($offset)->where('user_type', 'user')->limit($limit)->orderBy($sort_column, $sort_order)->get();

        foreach ($consumers as $consumer) {
            $params = array(
                'url'       => route('consumer.update', $consumer->id),
                'checked'   => ($consumer->active == 1) ? "checked" : "",
                'getaction' => '',
                'class'     => '',
            );

            $records['data'][] = [
                'checkbox'   => view('shared.checkbox')->with('id', $consumer->id)->render(),
                'updated_at' =>$consumer->updated_at,
                'first_name' => $consumer->first_name,
                'last_name'  => $consumer->last_name,
                'consumer_type' => ucwords($consumer->consumer_type),
                'email'      => $consumer->email,
                'monthly'    => '£'.$consumer->monthly,
                'yearly'     => '£'.$consumer->yearly,
                'active'     => view('shared.switch')->with(['params' => $params, 'id' => $consumer->id])->render(),
                'action'     => view('shared.actions')->with('id', $consumer->id)->render(),
            ];
        }
        return $records;
    }

    public function notificationForm(Request $request)
    {
        //$notification = Notification::find(1)->with('user')->get();
        $notification = Notification::create($request->only(['user_id', 'title', 'description']));
        if ($notification) {
            $user = User::findOrFail($request->user_id);  
            Mail::to($user->email)->send(new Consumernotify($request));
            flash('Notification Sent Successfully')->success();
            return redirect()->back();
        }
        flash('Server Error.')->error();
        return redirect()->back();
    }
}
