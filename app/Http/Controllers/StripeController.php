<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Stripe\Customer;
use Stripe\Error\Card;
use Stripe\Stripe;

class StripeController extends Controller
{
    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'card_no' => 'required',
            'card_name' => 'required',
            'cvv' => 'required',
            'expiration' => 'required',
        ]);

        Stripe::setApiKey(config("services.stripe.secret"));
        $customer = Customer::retrieve($request->user()->stripe_id);

        $package = ($request->package == 'monthly') ? ('user-' . $request->user()->id . '-monthly') : ('user-' . $request->user()->id . '-yearly');

        $date = explode('/', $request->expiration);
        try {
            $card = $customer->sources->create(
                array(
                    "source" => [
                        'object'    => "card",
                        "exp_month" => $date[0],
                        "exp_year"  => $date[1],
                        "number"    => $request->card_no,
                        "cvc"       => $request->cvv,
                        "name"      => $request->user()->first_name . " " . $request->user()->last_name,
                    ],
                )
            );
            if ($card) {
                $request->user()->card_brand     = $card->brand;
                $request->user()->card_last_four = $card->last4;
                $request->user()->trial_ends_at  = Carbon::now();
                $request->user()->active_plan    = $request->package;
                $request->user()->save();
                $result = $request->user()->newSubscription('main', $package)->create();
                flash("Subscription Successful.")->success();
                return redirect('dashboard');
            }
        } catch (Card $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        }catch (\Exception $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        }
    }
}
