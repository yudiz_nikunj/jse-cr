<?php

namespace App\Http\Controllers;

use App\Mail\ContactUs;
use App\Mail\Propertyinquiry;
use App\Models\Amenities;
use App\Models\Enquiry;
use App\Models\NewProperty;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller
{
    public function index()
    {
        // dd('dfssdaf');
        $page = Page::where('name', "home")->first();
        $property = NewProperty::where('active', 1)->take(3)->orderBy('id','DESC')->get();

        if (!$page) {
            abort(404);
        }

        return view("frontend.pages.index", compact('page','property'))->withTitle("Home Page");
    }
    public function newproperty()
    {   
        $amenities = Amenities::all();
        $paginate = 12;
        $property = NewProperty::where('active', 1)->orderBy('id','DESC');
        
        if (request()->ajax()) {
            $amenities = explode(',',request()->amenities);
            $bedroom = explode(',',request()->bedroom);
            $property_type = explode(',',request()->property_type);

            if(count($bedroom) > 0 && request()->bedroom !=''){
                $property = $property->whereIn('bedroom',$bedroom);
            }
            if(count($property_type) > 0 && request()->property_type !=''){
                $property = $property->whereIn('property_type',$property_type);
            }
            if(request()->location !=''){
                $property = $property->where('location',request()->location);
            }
            if(count($amenities) > 0 && request()->amenities !=''){
                $property = $property->whereHas('amenities', function($query) use ($amenities) {
                    $query->whereIn('amenity_id',$amenities);
                });
            }
            if(request()->price !=''){
                $prc = explode(';',request()->price);
                $from = $prc[0] ?? 0;
                $to = $prc[1] ?? 0;
                if($to > 0 && $from> 0){
                    $property = $property->where(function($query) use($to,$from) {
                        $query->where('price','>=',$from)->where('price','<=',$to);
                    });
                }
                
            }
            $property = $property->paginate($paginate);
            $filter = 1;
            return view("frontend.pages.property-block",compact('property','paginate','filter'));
        }
        $property = $property->paginate($paginate);
        return view("frontend.pages.new-property",compact('property','amenities','paginate'))->withTitle("Property");
    }

    public function propertydetailpage($slug)
    {
        $property = NewProperty::where('slug',$slug)->first();

        if(!isset($property->id)){
            abort(404);
        }

        $previous = NewProperty::select('slug')->where('id', '<', $property->id)->orderBy('id','DESC')->first();
        $next = NewProperty::select('slug')->where('id', '>', $property->id)->orderBy('id','ASC')->first();
        return view("frontend.pages.property-detail-page",compact('property','previous','next'))->withTitle($property->name);
    }

    public function about()
    {
        $page = Page::where('name', "about")->first();

        if (!$page) {
            abort(404);
        }

        return view("frontend.pages.about", compact('page'))->withTitle("About Us");
    }

    public function serviceOverview()
    {
        $page = Page::where('name', "overview")->first();

        if (!$page) {
            abort(404);
        }

        return view("frontend.pages.overview", compact('page'))->withTitle("Service Overview");
    }

    public function coverage()
    {
        $page = Page::where('name', "coverage")->first();

        if (!$page) {
            abort(404);
        }

        return view("frontend.pages.coverage", compact('page'))->withTitle("Coverage");
    }

    public function management()
    {
        $page = Page::where('name', "management")->first();

        if (!$page) {
            abort(404);
        }

        return view("frontend.pages.management", compact('page'))->withTitle("Management");
    }

    public function development()
    {
        $page = Page::where('name', "development")->first();

        if (!$page) {
            abort(404);
        }

        return view("frontend.pages.development", compact('page'))->withTitle("Development");
    }

    public function sourcing()
    {
        $page = Page::where('name', "sourcing")->first();

        if (!$page) {
            abort(404);
        }

        return view("frontend.pages.sourcing", compact('page'))->withTitle("Sourcing");
    }

    public function contact()
    {
        $page = Page::where('name', "contact")->first();

        if (!$page) {
            abort(404);
        }

        return view("frontend.pages.contact", compact('page'))->withTitle("Contact Us");
    }

    public function enquiry(Request $request)
    {
        if ($request->type != "sign-up") {
            Mail::to(config('settings.admin_email'))->send(new ContactUs($request));
        }
        $enquiry = Enquiry::create($request->only('name', 'email', 'telephone', 'enquiry', 'type', 'property_address', 'rooms'));

        ($request->type != "sign-up") ? flash('Thank you for ContactUs.')->success() : flash('Admin will contact you soon.')->success();
        return redirect()->back();
    }

    public function propertyinquiry(Request $request)
    {
        Mail::to(config('settings.admin_email'))->send(new Propertyinquiry($request));
        flash('Thank you for property inquire, admin will contact you soon.')->success();
        return redirect()->back();
    }
}
