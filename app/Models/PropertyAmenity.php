<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyAmenity extends Model
{
   
    
    protected $fillable = ['amenity_id','property_id'];

    protected $table ='new_properties_amenities';
   
}
