<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewPropertyImage extends Model
{
   
    
    protected $fillable = ['name','property_id'];

    protected $table ='new_properties_images';
   
}
