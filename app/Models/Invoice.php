<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $fillable = ['property_id', 'reference', 'value', 'status', 'created_at', 'duedate', 'payment_type', 'notes', 'path'];

    protected $dates = ['duedate'];

    public function property()
    {
        return $this->belongsTo(\App\Models\Property::class)->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function setDueDateAttribute($value)
    {
        $this->attributes['duedate'] = \Carbon\Carbon::parse($value);
    }

    public function isUploadInvoice()
    {
        if(!empty($this->path)){
            return true;
        }else{
            return false;
        }
    }
}
