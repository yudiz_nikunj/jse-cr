<?php

namespace App\Providers;

use App\User;
use Validator;
use App\Models\Notification;
use Laravel\Cashier\Cashier;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Cashier::useCurrency('gbp', '£');
        Validator::extend('unique_email', function ($attribute, $value, $parameters) {
            $checkEmail = User::where($attribute, "$value")->where('user_type', '!=', 'social');
            if (isset($parameters[0]) && $parameters[0] > 0) {
                $checkEmail->where('id', '!=', $parameters[0]);
            }
            $checkEmail = $checkEmail->count();
            if ($checkEmail == 0) {
                return true;
            }
        });

        View::composer('frontend.partials.user_menu', function ($view) {
            $notification = Notification::where(['user_id'=>Auth::id(),'status'=>1])->count();
            $view->with(['notification' => $notification]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
