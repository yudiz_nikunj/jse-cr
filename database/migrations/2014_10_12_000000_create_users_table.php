<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('user_type', ['admin','user'])->default('user');
            $table->enum('consumer_type', ['management','sourcing'])->default('management');
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('email', 100);
            $table->string('password', 100);
            $table->string('profile_photo', 60);
            $table->string('phone_number', 30);
            $table->string('postcode', 20);
            $table->string('address', 500);
            $table->string('city', 30);
            $table->string('stripe_id', 50)->nullable();
            $table->string('card_brand', 30)->nullable();
            $table->string('card_last_four', 10)->nullable();
            $table->timestamp('trial_ends_at')->nullable();
            $table->integer('monthly');
            $table->integer('yearly');
            $table->boolean('active')->default(1);
            $table->string('active_plan', 10)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
