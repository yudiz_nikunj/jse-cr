<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewPropertiesImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('new_properties_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('property_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('property_id')->references('id')->on('new_properties')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
