<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquiries', function (Blueprint $table){
            $table->increments('id');
            $table->string('name',30);
            $table->string('email',50);
            $table->string('telephone',15);
            $table->string('enquiry', 500)->nullable();
            $table->string('type',20)->default('write-to-us');
            $table->string('property_address', 500)->nullable();
            $table->string('rooms', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enquiries');
    }
}
