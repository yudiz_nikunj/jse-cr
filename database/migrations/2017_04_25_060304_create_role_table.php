<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoleTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('role', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('section_id')->unsigned();
			$table->string('title');
			$table->string('route');
			$table->string('params');
			$table->string('image');
			$table->integer('sequence');
			$table->boolean('active')->default(1);
			$table->timestamps();

			$table->foreign('section_id')->references('id')->on('section')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('role');
	}
}
