<?php

use Illuminate\Database\Seeder;

class AminitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $thumbs = array (
        	array(
                'id'=>1,
        		'title'=>'Lift'
        	),
     		array(
                'id'=>2,
        		'title'=>'New Build'
        	),
        	array(
                'id'=>3,
        		'title'=>'Wi-Fi'
        	),
        	array(
                'id'=>4,
        		'title'=>'Balcony'
        	),
        	array(
                'id'=>5,
        		'title'=>'Air Conditioned'
        	),
        	array(
                'id'=>6,
        		'title'=>'Garden'
        	)
        );
		$db = DB::table('amenities')->insert($thumbs);
    }
}

