<?php

use Illuminate\Database\Seeder;

class permissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = array (
        	array(
        		'name'=>'Access',
                'constant'=>'access',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
        		'name'=>'Add',
                'constant'=>'add',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
        		'name'=>'Edit',
                'constant'=>'edit',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
        		'name'=>'View',
                'constant'=>'view',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
        		'name'=>'Delete',
                'constant'=>'delete',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
		);
		$db = DB::table('permissions')->insert($permissions);
    }
}
