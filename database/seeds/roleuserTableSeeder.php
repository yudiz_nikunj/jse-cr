<?php

use Illuminate\Database\Seeder;

class roleuserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array (
        	array(
                'id' => 1,
        		'user_id'=>1,
        		'role_id'=>1,
        		'permissions'=>'access',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
                'id' => 2,
        		'user_id'=>1,
        		'role_id'=>2,
        		'permissions'=>'access,edit',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
                'id' => 3,
        		'user_id'=>1,
        		'role_id'=>3,
        		'permissions'=>'access,edit',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
                'id' => 4,
                'user_id'=>1,
                'role_id'=>4,
                'permissions'=>'access,add,delete,edit',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id' => 5,
                'user_id'=>1,
                'role_id'=>5,
                'permissions'=>'access,delete',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id' => 6,
                'user_id'=>1,
                'role_id'=>6,
                'permissions'=>'access,add,delete',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id' => 7,
                'user_id'=>1,
                'role_id'=>7,
                'permissions'=>'access,add,edit,view',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id' => 8,
                'user_id'=>1,
                'role_id'=>8,
                'permissions'=>'access,add,edit,view',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id' => 9,
                'user_id'=>1,
                'role_id'=>9,
                'permissions'=>'access,add,edit,delete',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
        );
        $db = DB::table('role_user')->insert($roles);
    }
}
