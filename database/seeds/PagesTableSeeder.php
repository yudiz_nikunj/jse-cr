<?php

use Illuminate\Database\Seeder;
use App\Models\Page;
use Carbon\Carbon;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Home Page Seed Data 
		$home_page = new Page();

		$home_page->name = "home";
		$home_page->title = "REDEFINING THE INDUSTRY<span>A PERSONABLE APPROACH YOU CAN TRUST</span>";
		$home_page->content = '<section class="common-section">
						        <div class="container">
						            <div class="saperator wow animatedslow fadeIn"></div>
						            <p>After a decade of managing properties James Elkington, founder of <b>JSE Property Management,</b> became frustrated with the quality of service from lettings and estate agents and also the cost of the services being offered.. These frustrations ranged from being tied in to a contract, being overcharged for property management to third party companies adding margins on to the works their contractors have carried out.</p>
						            <p>Our company is passionate about providing a smooth, personable, customer centric and cost effective property management service. From organising trusted, insured and qualified tradesmen to carry out works to tenant management to sourcing the best value for money quotes on the market. Our invoicing, job tracking and payment systems are designed around customers needs and for ease of use. </p>
						            <p>JSE Property Management also provides a property sourcing service and project management services for renovations and refurbishments, please see ‘Services’ for more information.</p>
						            <div class="height-saperator"></div>
						            <div class="section-title wow">
						                <h1>Services</h1>
						            </div>
						        </div>
						    </section>
						    <section class="services-section">
							    <div class="row">
							        <div class="col-sm-12">
							            <a href="management" class="services-block management">
							                <p>MANAGEMENT</p>
							                <figure>
							                    <img src="frontend/images/LogoIcon-YELLLOW.png" alt="jse logo" class="wow animatedslow zoomIn"/>
							                </figure>
							            </a>
							        </div>
							        <div class="col-sm-6">
							            <a href="development" class="services-block development">
							                <p>DEVELOPMENT</p>
							                <figure>
							                    <img src="frontend/images/LogoIcon-YELLLOW.png" alt="jse logo" class="wow animatedslow zoomIn"/>
							                </figure>
							            </a>
							        </div>
							        <div class="col-sm-6">
							            <a href="sourcing" class="services-block sourcing">
							                <p>SOURCING</p>
							                <figure>
							                    <img src="frontend/images/LogoIcon-YELLLOW.png" alt="jse logo" class="wow animatedslow zoomIn"/>
							                </figure>
							            </a>
							        </div>
							        <div class="clearfix"></div>
							    </div>
							</section>
						    <section class="common-section">
							        <div class="container">
							            <div class="section-title wow">
							                <h1>MANAGEMENT</h1>
							            </div>
							            <div class="height-saperator"></div>
							            <p><b>JSE Property Management</b> aims to make your life as a landlord simpler while ensuring you do not lose money by being over charged for the day to day running of your property. We do this by charging a fixed rate and only charging you what we are charged by our contractors.</p>
							            <p>This is how we work:</p>
							            <div class="management-block">
							                <div class="row">
							                    <div class="col-sm-6 management-box">
							                        <figure>
							                            <img src="frontend/images/consultation-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY" />
							                        </figure>
							                        <h4>INITIAL CONSULTATION</h4>
							                        <p>Your initial consultation can be conducted by phone or a face-to-face meeting at a time and place to suit you. We will then visit the property, make sure to understand all of your needs and keep communications thereafter as regular and detailed as you would or would not like.</p>
							                    </div>
							                    <div class="col-sm-6 management-box">
							                        <figure>
							                            <img src="frontend/images/management-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY" />
							                        </figure>
							                        <h4>PROPERTY MANAGEMENT</h4>
							                        <p>Your individual property address/addresses will be uploaded to our secure ‘Client Services’ portal along with tenant contact details and all relevant information surrounding each property. Invoices will be uploaded to each individual property so you can see what\'s been done and when.</p>
							                    </div>
							                    <div class="col-sm-6 management-box">
							                        <figure>
							                            <img src="frontend/images/operation-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY" />
							                        </figure>
							                        <h4>SMOOTH OPERATIONS</h4>
							                        <p>We will alert you via text and email upon completion of each job and payment is then made via our secure payment portal. We can liaise directly with the tenants, agency or landlord depending on your preference. </p>
							                    </div>
							                    <div class="col-sm-6 management-box">
							                        <figure>
							                            <img src="frontend/images/approach-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY" />
							                        </figure>
							                        <h4>TRUSTWORTHY APPROACH</h4>
							                        <p>JSE Property Management and all contractors that work on our behalf are fully insured. Note that we act as a facilitator and will not add any extra charges or margin to any works completed. This is our commitment to you.</p>
							                    </div>
							                    <div class="cleafix"></div>
							                    <a class="theme-btn wow animatedslow fadeIn" href="#">find out more</a>
							                </div>
							            </div>
							        </div>
							</section>';

		$home_page->created_at = Carbon::now();
		$home_page->updated_at = Carbon::now();
		$home_page->save();


		// About Page Seed Data 
		$about_page = new Page();

		$about_page->name = "about";
		$about_page->title = "About";
		$about_page->content = '<section class="common-section">
								    <div class="container">
								        <div class="saperator wow animatedslow fadeIn"></div>
								        <p><b>Having managed properties for over ten years we have an unpatrolled knowledge and expertise in property management. From studio flats to five bedroom properties you will not find a more consistent and meticulous service on the market.</b></p>
								        <p>Over the past decade we have built close working relationships with a large number of businesses and individuals who work in partnership with us looking after the maintenance and well being of our clients properties. It is these two-way relationships that guarantee all of the services on our behalf are to the highest standard and for the best possible price.</p>
								        <p>With a redefining customer service policy we welcome all enquiries and look forward to hearing from you.</p>
								        <div class="section-title wow">
								            <h1>MANAGEMENT</h1>
								        </div>
								        <div class="height-saperator"></div>
								        <p><b>JSE Property Management</b> aims to make your life as a landlord simpler while ensuring you do not lose money by being over charged for the day to day running of your property. We do this by charging a fixed rate and only charging you what we are charged by our contractors.</p>
								        <p>This is how we work:</p>
								        <div class="management-block">
								            <div class="row">
								                <div class="col-sm-6 management-box">
								                    <figure>
								                        <img src="frontend/images/consultation-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY"/>
								                    </figure>
								                    <h4>INITIAL CONSULTATION</h4>
								                    <p>Your initial consultation can be conducted by phone or a face-to-face meeting at a time and place to suit you. We will then visit the property, make sure to understand all of your needs and keep communications thereafter as regular and detailed as you would or would not like.</p>
								                </div>
								                <div class="col-sm-6 management-box">
								                    <figure>
								                        <img src="frontend/images/management-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY"/>
								                    </figure>
								                    <h4>PROPERTY MANAGEMENT</h4>
								                    <p>Your individual property address/addresses will be uploaded to our secure ‘Client Services’ portal along with tenant contact details and all relevant information surrounding each property. Invoices will be uploaded to each individual property so you can see what\'s been done and when.</p>
								                </div>
								                <div class="col-sm-6 management-box">
								                    <figure>
								                        <img src="frontend/images/operation-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY"/>
								                    </figure>
								                    <h4>SMOOTH OPERATIONS</h4>
								                    <p>We will alert you via text and email upon completion of each job and payment is then made via our secure payment portal. We can liaise directly with the tenants, agency or landlord depending on your preference. </p>
								                </div>
								                <div class="col-sm-6 management-box">
								                    <figure>
								                        <img src="frontend/images/approach-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY"/>
								                    </figure>
								                    <h4>TRUSTWORTHY APPROACH</h4>
								                    <p>JSE Property Management and all contractors that work on our behalf are fully insured. Note that we act as a facilitator and will not add any extra charges or margin to any works completed. This is our commitment to you.</p>
								                </div>
								                <div class="cleafix"></div>
								                <a class="theme-btn wow animatedslow fadeIn" href="#">find out more</a>
								            </div>
								        </div>
								    </div>
								</section>
								<section class="signup-section more-info-section bg-parallex">
								    <div class="container">
								        <h2>FOR MORE INFORMATION CALL  JAMES TODAY ON </h2>
								        <h1>020 3612 8986</h1>
								    </div>
								</section>';

		$about_page->created_at = Carbon::now();
		$about_page->updated_at = Carbon::now();
		$about_page->save();


		// Overview Page Seed Data 
		$overview_page = new Page();

		$overview_page->name = "overview";
		$overview_page->title = "Services";
		$overview_page->content = '<section class="common-section">
								    <div class="container">
								        <div class="saperator wow animatedslow fadeIn"></div>
								        <p><b>JSE Property Management is passionate about providing a smooth, personable, customer centric and cost effective Property Management service.</b></p>
								        <p>From organising trusted, insured and qualified tradesmen to carry out works to tenant management while works are being completed. Our invoicing, job tracking and payment systems are designed to make the process effortless.</p>
								        <p>We also provide a Property Sourcing service and Project Management services for renovations and refurbishments.</p>
								        <div class="height-saperator"></div>
								        <div class="section-title wow">
								            <h1>WHAT WE DO</h1>
								        </div>
								    </div>
								</section>
								<section class="services-section">
								    <div class="row">
								        <div class="col-sm-12">
								            <a href="management" class="services-block management">
								                <p>MANAGEMENT</p>
								                <figure>
								                    <img src="frontend/images/LogoIcon-YELLLOW.png" alt="jse logo" class="wow animatedslow flipInY"/>
								                </figure>
								            </a>
								        </div>
								        <div class="col-sm-6">
								            <a href="development" class="services-block development">
								                <p>DEVELOPMENT</p>
								                <figure>
								                    <img src="frontend/images/LogoIcon-YELLLOW.png" alt="jse logo" class="wow animatedslow flipInY"/>
								                </figure>
								            </a>
								        </div>
								        <div class="col-sm-6">
								            <a href="sourcing" class="services-block sourcing">
								                <p>SOURCING</p>
								                <figure>
								                    <img src="frontend/images/LogoIcon-YELLLOW.png" alt="jse logo" class="wow animatedslow flipInY"/>
								                </figure>
								            </a>
								        </div>
								        <div class="clearfix"></div>
								    </div>
								</section>';

		$overview_page->created_at = Carbon::now();
		$overview_page->updated_at = Carbon::now();
		$overview_page->save();


		// Management Page Seed Data
		$management_page = new Page();

		$management_page->name = "management";
		$management_page->title = "Management";
		$management_page->content = '<section class="common-section">
							            <div class="container">
							                <div class="saperator wow animatedslow fadeIn"></div>
							                <p><b>JSE Property Management aims to make your life as a landlord simpler while ensuring you do not lose money by being over charged for the day to day running of your property.</b></p>
							                <p>We do this by charging a fixed rate and only charging you what we are charged by our contractors.</p>
							                <p>This is how we work:</p>
							                <div class="management-block">
							                    <div class="row">
							                        <div class="col-sm-6 management-box">
							                            <figure>
							                                <img src="frontend/images/consultation-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY"/>
							                            </figure>
							                            <h4>INITIAL CONSULTATION</h4>
							                            <p>Your initial consultation can be conducted by phone or a face-to-face meeting at a time and place to suit you. We will then visit the property, make sure to understand all of your needs and keep communications thereafter as regular and detailed as you would or would not like.</p>
							                        </div>
							                        <div class="col-sm-6 management-box">
							                            <figure>
							                                <img src="frontend/images/management-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY"/>
							                            </figure>
							                            <h4>PROPERTY MANAGEMENT</h4>
							                            <p>Your individual property address/addresses will be uploaded to our secure ‘Client Services’ portal along with tenant contact details and all relevant information surrounding each property. Invoices will be uploaded to each individual property so you can see what\'s been done and when.</p>
							                        </div>
							                        <div class="col-sm-6 management-box">
							                            <figure>
							                                <img src="frontend/images/operation-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY"/>
							                            </figure>
							                            <h4>SMOOTH OPERATIONS</h4>
							                            <p>We will alert you via text and email upon completion of each job and payment is then made via our secure payment portal. We can liaise directly with the tenants, agency or landlord depending on your preference. </p>
							                        </div>
							                        <div class="col-sm-6 management-box">
							                            <figure>
							                                <img src="frontend/images/approach-icon.jpg" alt="jse management icon" class="wow animatedslow flipInY"/>
							                            </figure>
							                            <h4>TRUSTWORTHY APPROACH</h4>
							                            <p>JSE Property Management and all contractors that work on our behalf are fully insured. Note that we act as a facilitator and will not add any extra charges or margin to any works completed. This is our commitment to you.</p>
							                        </div>
							                        <div class="cleafix"></div>
							                        <a class="theme-btn wow animatedslow fadeIn" href="#" >our rates</a>
							                    </div>
							                </div>
							            </div>
							        </section>
							        <section class="common-section dark-section">
							            <div class="container">
							                <div class="section-title wow">
							                    <h1>SERVICES</h1>
							                </div>
							                <div class="height-saperator"></div>
							                <p>Here is the list of services we can provide as part of our Property Management package. And remember, we act as a facilitator and will not add any extra charges or margin to any works completed.</p>
							                <div class="list">
							                    <div class="row">
							                        <div class="col-sm-4 col-md-3">
							                            <p><span>Repairs & Maintenance</span></p>
							                            <p><span>Check-out Management</span></p>
							                            <p><span>Building & Refurbishment Projects</span></p>
							                        </div>
							                        <div class="col-sm-4 col-md-6">
							                            <p><span>Annual gas and electrical safety inspections / certificates</span></p>
							                            <p><span>Liaison with Surveyors and other Professional Advisors</span></p>
							                            <p><span>Management of Vacant Properties (and between tenancies)</span></p>
							                            <p><span>Out of hours & 24x7 Emergency Service</span> </p>
							                        </div>
							                        <div class="col-sm-4 col-md-3">
							                            <p><span>Inventory Reports</span></p>
							                            <p><span>Management Inspections</span></p>
							                            <p><span>Cleaning Services</span></p>
							                        </div>
							                        <div class="clearfix"></div>
							                    </div>
							                </div>
							            </div>
							        </section>';
        $management_page->created_at = Carbon::now();
		$management_page->updated_at = Carbon::now();
		$management_page->save();


		// Development Page Seed Data
		$development_page = new Page();

		$development_page->name = "development";
		$development_page->title = "Development";
		$development_page->content = '<section class="common-section">
							            <div class="container">
							                <div class="saperator wow animatedslow fadeIn"></div>
							                <p><b>JSE Property Management has expertise in property development with our fees starting from £1000.</b></p>
							                <p>JSE Property Management can be involved as little or as much as you would like from an initial consultation to full project management. Please do contact us for more information.</p>
							            </div>
							        </section>';
        $development_page->created_at = Carbon::now();
		$development_page->updated_at = Carbon::now();
		$development_page->save();


		// Sourcing Page Seed Data
		$sourcing_page = new Page();

		$sourcing_page->name = "sourcing";
		$sourcing_page->title = "sourcing";
		$sourcing_page->content = '<section class="common-section">
							            <div class="container">
							                <div class="saperator wow animatedslow fadeIn"></div>
							                <p><b>Having been sourcing residential, buy to let and development properties for over ten years James has forged close relationships with carefully selected contacts which enables him access to off market and discounted deals that others outside of the industry simply do not have access to.</b></p>
							                <p>Upon your initial enquiry we will organise a face to face meeting so as to understand your requirements and how best to move forward.</p>
							                <p><b>Our fees start from £500 and will never exceed £5,000.</b> Please do contact us to discuss this further.</p>
							            </div>
							        </section>';
        $sourcing_page->created_at = Carbon::now();
		$sourcing_page->updated_at = Carbon::now();
		$sourcing_page->save();

		// Coverage Page Seed Data
		$coverage_page = new Page();

		$coverage_page->name = "coverage";
		$coverage_page->title = "coverage";
		$coverage_page->content = '<section class="common-section">
							            <div class="container">
							                <div class="saperator wow animatedslow fadeIn"></div>
							                <p><b>JSE Property Management currently operates in London and Lincolnshire. We will be expanding to other areas of the UK in due course so please do get in touch to discuss our plans for your area.</b></p>
							                <div class="coverage-map-block">
							                    <div class="coverage-image hidden-xs">
							                        <figure>
							                            <img src="frontend/images/coverage-map.jpg" alt="jse map" />
							                        </figure>
							                        <div class="coverage-location-box london">
							                            <h3 class="wow animatedslow fadeIn">london</h3>
							                            <img src="frontend/images/LogoIcon-YELLLOW.png" alt="jse logo" class="wow animatedslow flipInY"/>
							                        </div>
							                        <div class="coverage-location-box lincolnshire">
							                            <h3 class="wow animatedslow fadeIn">lincolnshire</h3>
							                            <img src="frontend/images/LogoIcon-YELLLOW.png" alt="jse logo" class="wow animatedslow flipInY"/>
							                        </div>
							                    </div>
							                    <div class="coverage-image visible-xs">
							                        <figure>
							                            <img src="frontend/images/coverage-map-Mobile.png" alt="jse map" />
							                        </figure>
							                    </div>
							                </div>
							            </div>
							        </section>
							        <section class="signup-section more-info-section bg-parallex">
							            <div class="container">
							                <h2>FOR MORE INFORMATION CALL  JAMES TODAY ON </h2>
							                <h1>020 3612 8986</h1>
							            </div>
							        </section>';
        $coverage_page->created_at = Carbon::now();
		$coverage_page->updated_at = Carbon::now();
		$coverage_page->save();

		// Contact Page Seed Data
		$contact_page = new Page();

		$contact_page->name = "contact";
		$contact_page->title = "contact";
		$contact_page->content = '<section class="common-section">
							            <div class="container">
							                <div class="saperator wow animatedslow fadeIn"></div>
							                <p><b>We love to hear from our clients. Please feel free to get in touch with us.​</b></p>
							                <div class="contact-list-block">
							                    <h2>JSE Property Management Ltd</h2>
							                    <div class="row">
							                        <div class="col-md-4">
							                            <figure><img src="frontend/images/call-icon.jpg" alt="jse icon" /></figure>
							                            <p><a href="tel:020 3612 8986">020 3612 8986</a>  /  <a href="tel:07747 305 545">07747 305 545</a></p>
							                        </div>
							                        <div class="col-md-4">
							                            <figure><img src="frontend/images/location-icon.jpg" alt="jse icon" /></figure>
							                            <p>3 Lower Thames Street, St Magnus House, London EC3R 6HE</p>
							                        </div>
							                        <div class="col-md-4">
							                            <figure><img src="frontend/images/mail-icon.jpg" alt="jse icon" /></figure>
							                            <p><a href="mailto:james@jsepropertymanagement.com">james@jsepropertymanagement.com</a></p>
							                        </div>
							                        <div class="clearfix"></div>
							                    </div>
							                </div>
							            </div>
							        </section>';
        $contact_page->created_at = Carbon::now();
		$contact_page->updated_at = Carbon::now();
		$contact_page->save();
				
    }
}
