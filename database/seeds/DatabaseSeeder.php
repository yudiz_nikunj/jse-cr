<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$this->call(userTableSeeder::class);
		$this->call(sectionTableSeeder::class);
		$this->call(roleTableSeeder::class);
		$this->call(permissionsTableSeeder::class);
		$this->call(imagethumbTableSeeder::class);
		$this->call(sitesettingTableSeeder::class);
		$this->call(roleuserTableSeeder::class);
		$this->call(PagesTableSeeder::class);
	}
}
