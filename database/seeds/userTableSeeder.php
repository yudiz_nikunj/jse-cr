<?php

use Illuminate\Database\Seeder;

class userTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array (
        	array(
                'id' => 1,
        		'user_type'=>'admin',
        		'first_name'=>'Admin',
                'last_name'=>'Admin',
        		'email'=>'admin@jse.com',
                'password'=>bcrypt('jse@123'),
        		'profile_photo'=>'',
        		'phone_number'=>'9876543210',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
    	);
        $db = DB::table('users')->insert($users);
    }
}
