<?php

use Illuminate\Database\Seeder;

class roleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array (
        	array(
                'id' => 1,
        		'section_id'=>1,
        		'title'=>'Dashboard',
        		'route'=>'home.index',
                'params'=>'',
        		'image'=>'fa fa-home',
        		'sequence'=>1,
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
                'id' => 2,
        		'section_id'=>2,
        		'title'=>'General Settings',
        		'route'=>'settings.showSetting',
                'params'=>'',
        		'image'=>'fa fa-cogs',
        		'sequence'=>1,
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
            array(
                'id' => 3,
                'section_id'=>3,
                'title'=>'CMS',
                'route'=>'cms.index',
                'params'=>'',
                'image'=>'fa fa-file-text-o',
                'sequence'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id' => 4,
                'section_id'=>4,
                'title'=>'Consumers',
                'route'=>'consumer.index',
                'params'=>'',
                'image'=>'fa fa-user',
                'sequence'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id' => 5,
                'section_id'=>5,
                'title'=>'Enquiry',
                'route'=>'enquiry.index',
                'params'=>'',
                'image'=>'fa fa-hourglass-start',
                'sequence'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id' => 6,
                'section_id'=>6,
                'title'=>'Properties    ',
                'route'=>'property.index',
                'params'=>'',
                'image'=>'fa fa-university',
                'sequence'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id' => 7,
                'section_id'=>7,
                'title'=>'Invoice',
                'route'=>'invoice.index',
                'params'=>'',
                'image'=>'fa fa-calculator',
                'sequence'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id' => 8,
                'section_id'=>8,
                'title'=>'User Invoice',
                'route'=>'userinvoice.index',
                'params'=>'',
                'image'=>'fa fa-calculator',
                'sequence'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            )
            array(
                'id' => 9,
                'section_id'=>9,
                'title'=>'Document',
                'route'=>'document.index',
                'params'=>'',
                'image'=>'fa fa-file-text-o',
                'sequence'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            )
		);
		$db = DB::table('role')->insert($roles);
    }
}
