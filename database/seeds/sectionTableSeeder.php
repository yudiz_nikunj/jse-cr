<?php

use Illuminate\Database\Seeder;

class sectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sections = array (
        	array(
                'id'=>1,
        		'name'=>'Dashboard',
        		'image'=>'fa fa-home',
        		'sequence'=>1,
                'active'=>1,
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
                'id'=>2,
        		'name'=>'General Settings',
        		'image'=>'fa fa-cogs',
        		'sequence'=>2,
                'active'=>1,
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
            array(
                'id'=>3,
                'name'=>'CMS',
                'image'=>'fa fa-file-text-o',
                'sequence'=>3,
                'active'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id'=>4,
                'name'=>'Consumers',
                'image'=>'fa fa-user',
                'sequence'=>4,
                'active'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id'=>5,
                'name'=>'Enquiry',
                'image'=>'fa fa-hourglass-start',
                'sequence'=>5,
                'active'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id'=>6,
                'name'=>'Properties',
                'image'=>'fa fa-university',
                'sequence'=>6,
                'active'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id'=>7,
                'name'=>'Invoice',
                'image'=>'fa fa-calculator',
                'sequence'=>7,
                'active'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id'=>8,
                'name'=>'User Invoice',
                'image'=>'fa fa-calculator',
                'sequence'=>8,
                'active'=>1,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
            array(
                'id'=>9,
                'name'=>'Document',
                'image'=>'fa fa-file-text-o',
                'sequence'=>9,
                'active'=>0,
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
            ),
		);
		$db = DB::table('section')->insert($sections);
    }
}
