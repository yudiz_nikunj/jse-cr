-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 01, 2020 at 02:35 PM
-- Server version: 10.2.26-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jcwebser_jse`
--

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enquiry` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'write-to-us',
  `property_address` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rooms` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `name`, `email`, `telephone`, `enquiry`, `type`, `property_address`, `rooms`, `created_at`, `updated_at`) VALUES
(31, 'Eric Jones', 'eric@talkwithcustomer.com', '416-385-3200', 'Hey,\r\n\r\nYou have a website jsepropertymanagement.co.uk, right?\r\n\r\nOf course you do. I am looking at your website now.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re ', 'write-to-us', NULL, NULL, '2019-11-06 19:49:23', '2019-11-06 19:49:23'),
(32, 'Eric Jones', 'eric@talkwithcustomer.com', '416-385-3200', 'Hi,\r\n\r\nMy name is Eric and I was looking at a few different sites online and came across your site jsepropertymanagement.co.uk.  I must say - your website is very impressive.  I am seeing your website on the first page of the Search Engine. \r\n\r\nHave you noticed that 70 percent of visitors who leave your website will never return?  In most cases, this means that 95 percent to 98 percent of your marketing efforts are going to waste, not to mention that you are losing more money in customer acquisi', 'write-to-us', NULL, NULL, '2019-11-26 06:29:46', '2019-11-26 06:29:46'),
(33, 'Lara Lawrence', 'lara@excellentcontent.top', '805-372-1751', 'I\'ve received a few of your emails recently and it looks like you could use a little help with your copywriting.  I did too.  \r\n \r\nI recently started using CopyTemplates.com for my business so I can easily copy, paste and fill in the blanks using proven email, website copy and more.  I figured you could improve your copy as I did.  They have free email copy here too: \r\nhttp://excellentcontent.top/best/?=jsepropertymanagement.co.uk\r\n \r\nHave a great day!\r\n \r\n\r\n \r\nRegards,\r\nLara\r\n\r\nP.S. - If you de', 'get-in-touch', NULL, NULL, '2019-11-30 22:20:00', '2019-11-30 22:20:00'),
(35, 'Eric Jones', 'eric@talkwithcustomer.com', '416-385-3200', 'Hello jsepropertymanagement.co.uk,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website jsepropertymanagement.co.uk.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to', 'write-to-us', NULL, NULL, '2019-12-09 14:31:21', '2019-12-09 14:31:21'),
(36, 'Nicole Anderson', 'nicole@finestwebtraffic.top', '326-856-8918', 'Hey, I know this up coming time is going to be slammed with the holidays, but are you missing potential customers?\r\n\r\nHow about even more customers, could you handle them?\r\n\r\nDiscover how we can drive more visitors for you:\r\nhttps://finestwebtraffic.top/up/?=jsepropertymanagement.co.uk\r\n\r\nHappy Holidays!\r\n\r\nRegards,\r\nNicole\r\n\r\nFinest Web Traffic\r\n30025 Alicia Pkwy #20-75\r\nLaguna Niguel, CA 92677\r\n\r\nIf you are unintersted in this type of marketing:\r\nhttps://finestwebtraffic.top/unsubscribe.php?si', 'get-in-touch', NULL, NULL, '2019-12-18 01:24:52', '2019-12-18 01:24:52'),
(37, 'Joe', 'joedhdsxc56754@gmail.com', '7289202073', NULL, 'get-in-touch', NULL, NULL, '2019-12-18 03:29:25', '2019-12-18 03:29:25'),
(38, 'Lauren Plant', 'lauren@socialcontentexpansion.top', '719-809-9008', 'Hey, I just found your website. But how come you\'re not on all the social media sites?\r\n\r\nIf you\'re too busy, here\'s an easier way to do it:\r\n\r\nhttps://www.socialcontentexpansion.top/win/?=jsepropertymanagement.co.uk\r\n\r\nIt gets your business seen everywhere. And the best part is -- it requires little to no effort on your part.\r\n\r\nRegards,\r\nLauren\r\n\r\n\r\n\r\n\r\n\r\n\r\n137 E. Elliot Rd. #1579\r\nGilbert, AZ 85299\r\n\r\n \r\nNo more content expansion marketing:\r\nhttps://socialcontentexpansion.top/out.php/?site=js', 'get-in-touch', NULL, NULL, '2020-01-06 22:30:10', '2020-01-06 22:30:10'),
(39, 'Roxie Wall', 'wall.roxie@hotmail.com', '469 3996', 'Are You interested in an advertising service that charges less than $39 per month and sends hundreds of people who are ready to buy directly to your website? Have a look at: http://www.moreleadsandsales.xyz', 'write-to-us', NULL, NULL, '2020-01-18 16:42:09', '2020-01-18 16:42:09'),
(40, 'Magdalena Abend', 'magdalena.abend@googlemail.com', '06-16266237', 'Do you want to find out how to make huge commissions every day without any product or inventory and with zero experience with sales and marketing? You will get  free online training showing exactly how I do this in just 3 easy steps! You read correctly, the training is completely free and you\'ll be shown exactly how this is done, no strings attached. Visit: http://www.commissionsonsteroids.xyz', 'write-to-us', NULL, NULL, '2020-01-19 20:02:48', '2020-01-19 20:02:48'),
(42, 'Charles P.', 'adi@ndmails.com', '8102440753', 'Just wanted to ask if you would be interested in getting external help with graphic design? We do all design work like banners, advertisements, photo edits, logos, flyers, etc. for a fixed monthly fee. \r\n\r\nWe don\'t charge for each task. What kind of work do you need on a regular basis? Let me know and I\'ll share my portfolio with you.', 'write-to-us', NULL, NULL, '2020-01-20 05:06:52', '2020-01-20 05:06:52'),
(43, 'Charles P.', 'adi@ndmails.com', '8102440753', 'Just wanted to ask if you would be interested in getting external help with graphic design? We do all design work like banners, advertisements, photo edits, logos, flyers, etc. for a fixed monthly fee. \r\n\r\nWe don\'t charge for each task. What kind of work do you need on a regular basis? Let me know and I\'ll share my portfolio with you.', 'write-to-us', NULL, NULL, '2020-01-20 05:07:19', '2020-01-20 05:07:19'),
(44, 'Megan Roth', 'megan.roth@webtraffic.top', '213-558-2100', 'Hi,\r\n  Did you know your website missed out on potential customers today? Even though you are doing a good job with your website.\r\n\r\nThe problem is there are more people out there that want to do business with you but they just don\'t know about you. \r\n\r\nNot having that visibility is losing you money every month. \r\n\r\nIts no mystery that in order for someone to do business with you online, they must visit your website. For most companies if you increase your visitors by 10% that directly translate', 'get-in-touch', NULL, NULL, '2020-01-21 01:01:28', '2020-01-21 01:01:28'),
(45, 'Megan Rodriguez', 'megan@instagrambiz.top', '213-558-1751', 'Hi,\r\nI had a look at your Instagram Instagram and it looks pretty good.  \r\n \r\nThe question I have is this, is your Instagram giving you the traffic and engagement that you deserve?  \r\n \r\nWe help people organically increase their followers and engagement.  Discover how we can help you get the genuine followers you deserve:\r\n\r\nhttps://instgrambiz.top/wal/?=jsepropertymanagement.co.uk\r\n\r\nRegards,\r\nMegan\r\nInstagram Growth Specialist\r\nInstagramBiz\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n964 E. Badillo Street #2055\r\nCovin', 'get-in-touch', NULL, NULL, '2020-01-26 22:52:16', '2020-01-26 22:52:16'),
(46, 'Mariel Horniman', 'mariel.horniman@gmail.com', '06-62854855', 'Have you had enough of expensive PPC advertising? Now you can post your ad on thousands of ad sites and it\'ll cost you less than $40. Get unlimited traffic forever! Get more info by visiting: http://www.automaticadposting.xyz', 'write-to-us', NULL, NULL, '2020-01-27 10:02:35', '2020-01-27 10:02:35'),
(47, 'Eric Jones', 'eric@talkwithcustomer.com', '416-385-3200', 'Hello jsepropertymanagement.co.uk,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website jsepropertymanagement.co.uk.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to', 'write-to-us', NULL, NULL, '2020-01-28 00:12:35', '2020-01-28 00:12:35'),
(48, 'Eusebia Vancouver', 'vancouver.eusebia@gmail.com', '626-475-2963', 'Sick of paying big bucks for ads that suck? Now you can post your ad on 5000 ad sites and it\'ll cost you less than $40. These ads stay up forever, this is a continual supply of organic visitors! For more information just visit: http://www.automaticadposting.xyz', 'write-to-us', NULL, NULL, '2020-02-01 22:18:33', '2020-02-01 22:18:33'),
(49, 'Eric Jones', 'eric@talkwithcustomer.com', '416-385-3200', 'Hello jsepropertymanagement.co.uk,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website jsepropertymanagement.co.uk.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to', 'write-to-us', NULL, NULL, '2020-02-07 16:29:08', '2020-02-07 16:29:08'),
(50, 'Yetta Barcenas', 'barcenas.yetta@gmail.com', '0394 7838707', 'UNLIMITED fresh and high PR .edu, do-follow and other extensions ready to be your backlink\r\nperfect for ranking your site in any niche! Completely exclusive links and never spammed to death http://www.backlinkmagic.xyz', 'write-to-us', NULL, NULL, '2020-02-08 07:01:02', '2020-02-08 07:01:02'),
(52, 'Monserrate Naugle', 'monserrate.naugle@hotmail.com', '06-40948232', 'Looking for CBD products? We\'ve got you covered! We have tinctures, topicals, bath bombs, coffee and more. Shop now --> http://bit.ly/hempworx-drodarte\r\n\r\nWant to make money selling CBD products? We offer one of the most lucrative compensation plans in the industry. No need to stock inventory, have a store or build a website. We have a turnkey solution you\'re going to love. Check out --> http://bit.ly/winwithmdc-drodarte', 'write-to-us', NULL, NULL, '2020-02-11 18:26:51', '2020-02-11 18:26:51'),
(53, 'Eric Jones', 'eric@talkwithcustomer.com', '416-385-3200', 'Hi,\r\n\r\nYou know it’s true…\r\n\r\nYour competition just can’t hold a candle to the way you DELIVER real solutions to your customers on your website jsepropertymanagement.co.uk.\r\n\r\nBut it’s a shame when good people who need what you have to offer wind up settling for second best or even worse.\r\n\r\nNot only do they deserve better, you deserve to be at the top of their list.\r\n \r\nTalkWithCustomer can reliably turn your website jsepropertymanagement.co.uk into a serious, lead generating machine.\r\n\r\nWith T', 'write-to-us', NULL, NULL, '2020-02-20 02:38:48', '2020-02-20 02:38:48'),
(54, 'Lyn Levering', 'lyn.levering@msn.com', '0483 26 51 53', 'Would you like to promote your ad on thousands of online ad websites every month? Pay one low monthly fee and get almost unlimited traffic to your site forever!To find out more check out our site here: http://www.instantadsposted.xyz', 'write-to-us', NULL, NULL, '2020-02-21 16:19:24', '2020-02-21 16:19:24'),
(56, 'Erika Houser', 'houser.erika@gmail.com', '(21) 9168-9686', 'Do you want to submit your business on 1000\'s of Advertising sites monthly? Pay one flat rate and get virtually unlimited traffic to your site forever! To find out more check out our site here: http://www.highvolumeads.xyz', 'write-to-us', NULL, NULL, '2020-02-27 15:56:58', '2020-02-27 15:56:58'),
(58, 'Eric Jones', 'eric@talkwithwebvisitor.com', '416-385-3200', 'Hey there, I just found your site, quick question…\r\n\r\nMy name’s Eric, I found jsepropertymanagement.co.uk after doing a quick search – you showed up near the top of the rankings, so whatever you’re doing for SEO, looks like it’s working well.\r\n\r\nSo here’s my question – what happens AFTER someone lands on your site?  Anything?\r\n\r\nResearch tells us at least 70% of the people who find your site, after a quick once-over, they disappear… forever.\r\n\r\nThat means that all the work and effort you put int', 'write-to-us', NULL, NULL, '2020-03-11 17:45:25', '2020-03-11 17:45:25'),
(59, 'Eric Jones', 'eric@talkwithwebvisitor.com', '416-385-3200', 'Cool website!\r\n\r\nMy name’s Eric, and I just found your site - jsepropertymanagement.co.uk - while surfing the net. You showed up at the top of the search results, so I checked you out. Looks like what you’re doing is pretty cool.\r\n \r\nBut if you don’t mind me asking – after someone like me stumbles across jsepropertymanagement.co.uk, what usually happens?\r\n\r\nIs your site generating leads for your business? \r\n \r\nI’m guessing some, but I also bet you’d like more… studies show that 7 out 10 who land', 'write-to-us', NULL, NULL, '2020-03-13 10:02:22', '2020-03-13 10:02:22'),
(60, 'Julian Prowse', 'julian.prowse97@msn.com', '079 4462 0943', '1 Cup of this tomorrow morning will burn 3lbs of belly fat\r\n\r\nIf you still haven\'t tried this, you’re going to want to add this to your morning routine\r\nConsuming 1 cup of this delicious hot beverage in the morning sets up your metabolism to burn more fat than 55 exhausting minutes on the treadmill.\r\n\r\nIn fact, some folks are losing up to 25 Lbs of fat in just 19 days by drinking it every morning.\r\n\r\nNot to mention, it’s really simple to make right in your own kitchen. \r\n\r\nTake a look for yourse', 'write-to-us', NULL, NULL, '2020-03-13 14:19:53', '2020-03-13 14:19:53'),
(61, 'Shelli Tooth', 'tooth.shelli@gmail.com', '022 552 24 50', 'Do you want to promote your website for free? Have a look at this: http://bit.ly/submityourfreeads', 'write-to-us', NULL, NULL, '2020-03-17 14:43:12', '2020-03-17 14:43:12'),
(62, 'Eric Jones', 'eric@talkwithwebvisitor.com', '416-385-3200', 'Hey, this is Eric and I ran across jsepropertymanagement.co.uk a few minutes ago.\r\n\r\nLooks great… but now what?\r\n\r\nBy that I mean, when someone like me finds your website – either through Search or just bouncing around – what happens next?  Do you get a lot of leads from your site, or at least enough to make you happy?\r\n\r\nHonestly, most business websites fall a bit short when it comes to generating paying customers. Studies show that 70% of a site’s visitors disappear and are gone forever after ', 'write-to-us', NULL, NULL, '2020-03-19 22:04:30', '2020-03-19 22:04:30'),
(63, 'Lydia Pinkney', 'lydia.pinkney@msn.com', '0681 837 68 65', 'Looking for fresh buyers? Receive hundreds of keyword targeted visitors directly to your site. Boost revenues quick. Start seeing results in as little as 48 hours. To get details Visit: http://bit.ly/trafficmasters2020', 'write-to-us', NULL, NULL, '2020-03-26 03:12:52', '2020-03-26 03:12:52'),
(64, 'Elton Maness', 'maness.elton63@gmail.com', '705-455-7592', 'Want more visitors for your website? Receive tons of people who are ready to buy sent directly to your website. Boost your profits super fast. Start seeing results in as little as 48 hours. For more info Have a look at: http://www.trafficmasters.xyz', 'write-to-us', NULL, NULL, '2020-03-27 15:48:34', '2020-03-27 15:48:34'),
(69, 'Nereida Powers', 'nereida.powers@googlemail.com', '06825 31 51 68', 'Tired of paying for visitors for your web page? Now you can get visitors for no cost at all check out: https://bit.ly/unlimited-traffic-source', 'write-to-us', NULL, NULL, '2020-04-04 01:52:19', '2020-04-04 01:52:19'),
(71, 'Claudia Clement', 'claudiauclement@yahoo.com', '0151-8122894', 'Hi, We are wondering if you would be interested in our service, where we can provide you with a dofollow link from Amazon (DA 96) back to jsepropertymanagement.co.uk?\r\n\r\nThe price is just $57 per link, via Paypal.\r\n\r\nTo explain backlinks, DA and the benefit they have for your website, along with a sample of an existing link, please read here: https://textuploader.com/16jn8\r\n\r\nIf you\'d be interested in learning more, reply to this email but please make sure you include the word INTERESTED in the ', 'write-to-us', NULL, NULL, '2020-04-08 10:24:33', '2020-04-08 10:24:33'),
(72, 'Wilmer Carmody', 'carmody.wilmer@gmail.com', '079 3242 1434', 'Hi,\r\n\r\nWe\'re wondering if you\'ve ever considered taking the content from jsepropertymanagement.co.uk and converting it into videos to promote on Youtube using Content Samurai? You simply add the text and it converts it into scenes that make up a full video. No special skills are needed, and there\'s access to over 1 million images/clips that can be used.\r\n\r\nYou can read more about the software here: https://turntextintovideo.com - there\'s also a link to a totally free guide called the \'Youtube SE', 'write-to-us', NULL, NULL, '2020-04-08 20:19:04', '2020-04-08 20:19:04'),
(73, 'Nicole Roth', 'nicole.roth@morebusinesstraffic.top', '213-558-5117', 'Hey, \r\n\r\nI just found jsepropertymanagement.co.uk and I\'m very impressed -- great site.\r\n\r\nAre you getting enough customers? \r\n\r\nCould you handle more? A lot more?\r\n\r\nWe can hugely increase the visitors to your website in just 1-2 days. \r\n\r\nHere\'s how it works:\r\nhttps://morebusinesstraffic.top/wal/?=jsepropertymanagement.co.uk\r\n\r\nTo your success,\r\nNicole\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n964 Eas. Badillo Street Unit 2055\r\nCovina, CA 91724\r\n\r\nIf you prefer not to receive commercial messages regarding traf', 'get-in-touch', NULL, NULL, '2020-04-09 21:11:34', '2020-04-09 21:11:34'),
(74, 'Consuelo Parer', 'consuelo.parer@gmail.com', '428 8020', 'Hi\r\n\r\nMy Name is Mark my company specialises in making low-cost short hard-hitting simple social\r\nmedia videos promoting and helping clients be top frontpage with youtube and google videos.\r\n\r\nIt doesn\'t matter what business you are in you need to be seen on social media, youtube, and\r\nGoogle videos.\r\n\r\nEnclosed a company promotional YouTube video sample https://bit.ly/3cl8M5r\r\n\r\n                                     75% DISCOUNT ON NEW CLIENTS\r\n\r\nShould you wish to make an enquiry or see samples', 'write-to-us', NULL, NULL, '2020-04-14 03:38:37', '2020-04-14 03:38:37'),
(75, 'Eric Jones', 'eric@talkwithwebvisitor.com', '416-385-3200', 'Hey, this is Eric and I ran across jsepropertymanagement.co.uk a few minutes ago.\r\n\r\nLooks great… but now what?\r\n\r\nBy that I mean, when someone like me finds your website – either through Search or just bouncing around – what happens next?  Do you get a lot of leads from your site, or at least enough to make you happy?\r\n\r\nHonestly, most business websites fall a bit short when it comes to generating paying customers. Studies show that 70% of a site’s visitors disappear and are gone forever after ', 'write-to-us', NULL, NULL, '2020-04-14 06:18:44', '2020-04-14 06:18:44'),
(76, 'Judy', 'judy999887@gmail.com', '485 41 874', 'Hi there,\r\n\r\nI hope you’re doing well, I handle influencer relations for LeggingsHut. Great to meet you!\r\n\r\nI stumbled across your account and thought your content would be perfect for us. If you feel we’d make a good fit, I’d love to invite you to our referral program.\r\n\r\nUse this link to visit, http://lotless.co\r\n\r\nIf you have any questions, just reply to this email.\r\njudy999887@gmail.com\r\n\r\n\r\nSincerely,\r\n\r\nLeggingsHut\r\nJudy', 'write-to-us', NULL, NULL, '2020-04-17 07:32:16', '2020-04-17 07:32:16'),
(77, 'Merri Farkas', 'merri.farkas@gmail.com', '06-99853426', 'No more paying 1000\'s of dollars for ripoff online advertising! I have a system that costs only a small bit of money and results in an almost infinite amount of traffic to your website\r\n\r\nTake a look at: http://bit.ly/adpostingrobot', 'write-to-us', NULL, NULL, '2020-04-19 20:54:45', '2020-04-19 20:54:45'),
(78, 'Hudson Faucett', 'hudson.faucett@gmail.com', '04664 15 45 51', 'Are You interested in advertising that charges less than $49 per month and sends tons of people who are ready to buy directly to your website? Visit: http://www.trafficmasters.xyz \r\n\r\nShould you like to be removed from our database please click this: https://bit.ly/2yp4480', 'write-to-us', NULL, NULL, '2020-04-19 20:55:11', '2020-04-19 20:55:11'),
(79, 'Melissa Smith', 'melissa.smith@aaamarketing.top', '213-558-2100', 'Hey, are you still open during the COVID lockdown?\r\n\r\nCould you handle more customers? A lot more?\r\n\r\nWe can help drive targeted customers to your website during these uncertain times:\r\n\r\nhttps://aaamarketing.top/up?=jsepropertymanagement.co.uk\r\n\r\nBe safe and stay healthy,\r\nMelissa\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nYour Best Customer Club\r\n11918 SE Division Street Unit#1054\r\nPortland, OR 97266\r\n\r\nUnsub from future marketing:\r\nhttps://aaamarketing.top/unsubscribe.php/?site=jsepropertymanagement.co.uk', 'get-in-touch', NULL, NULL, '2020-04-28 03:01:55', '2020-04-28 03:01:55'),
(80, 'Joanne Ferrell', 'ferrell.joanne25@gmail.com', '08636 76 19 18', 'Hi Everyone,\r\n\r\nI want to send you good wishes & good health in your future studies.\r\nI have a couple reading assignments for you.\r\n\r\n1984 by George Orwell\r\nBrave New World by Aldous Huxley\r\n\r\nWhat is happening now is criminal. I urge you to write local and federal goverments and voice your disgust with how they are handling this situation. \r\n\r\nYou should voice your opinion everywhere you go.\r\nYou will not be manipulated by fear.\r\n\r\nI urge you to do research and to think for yourself and questio', 'write-to-us', NULL, NULL, '2020-04-28 21:21:22', '2020-04-28 21:21:22'),
(81, 'Eric Jones', 'eric@talkwithwebvisitor.com', '416-385-3200', 'Hi, Eric here with a quick thought about your website jsepropertymanagement.co.uk...\r\n\r\nI’m on the internet a lot and I look at a lot of business websites.\r\n\r\nLike yours, many of them have great content. \r\n\r\nBut all too often, they come up short when it comes to engaging and connecting with anyone who visits.\r\n\r\nI get it – it’s hard.  Studies show 7 out of 10 people who land on a site, abandon it in moments without leaving even a trace.  You got the eyeball, but nothing else.\r\n\r\nHere’s a solutio', 'write-to-us', NULL, NULL, '2020-05-01 11:19:09', '2020-05-01 11:19:09'),
(82, 'Doyle Cobbs', 'doyle.cobbs@googlemail.com', '206-234-4186', 'Would you like zero cost advertising for your website? Check out: http://bit.ly/submityourfreeads', 'write-to-us', NULL, NULL, '2020-05-03 01:05:12', '2020-05-03 01:05:12'),
(83, 'Reed Mullawirraburka', 'info@jsepropertymanagement.co.uk', '52-25-70-73', 'Hello\r\n\r\nBuy medical disposable face mask to protect your loved ones from the deadly CoronaVirus.  The price is $0.99 each.  If interested, please check our site: pharmacyusa.online\r\n\r\nRegards,\r\n\r\nJSE Property Management - Contact Us - jsepropertymanagement.co.uk', 'write-to-us', NULL, NULL, '2020-05-05 02:36:30', '2020-05-05 02:36:30'),
(84, 'Eric Jones', 'eric@talkwithwebvisitor.com', '416-385-3200', 'My name’s Eric and I just came across your website - jsepropertymanagement.co.uk - in the search results.\r\n\r\nHere’s what that means to me…\r\n\r\nYour SEO’s working.\r\n\r\nYou’re getting eyeballs – mine at least.\r\n\r\nYour content’s pretty good, wouldn’t change a thing.\r\n\r\nBUT…\r\n\r\nEyeballs don’t pay the bills.\r\n\r\nCUSTOMERS do.\r\n\r\nAnd studies show that 7 out of 10 visitors to a site like jsepropertymanagement.co.uk will drop by, take a gander, and then head for the hills without doing anything else.\r\n\r\nIt', 'write-to-us', NULL, NULL, '2020-05-05 11:17:54', '2020-05-05 11:17:54'),
(85, 'Jonathon Plummer', 'datazeon@gmail.com', '01.78.20.99.00', 'Hi,\r\n\r\nHow are you? I hope this email finds you well and healthy. \r\n\r\nJust wanted to check if you have any B2B Marketing Email List requirement? We dominate the business email list industry with a data compilation of over 40 million B2B executives and 18 million businesses compiled over the past 9 years through seminars, trade shows, exhibitions and magazine subscription offers. \r\n\r\nIndustry Covers: Technology, Networking, Telecommunication, Healthcare, Finance, Manufacturing, Education, Chemica', 'write-to-us', NULL, NULL, '2020-05-06 21:37:49', '2020-05-06 21:37:49'),
(87, 'Effie Alonzo', 'office.largeglobes.com@gmail.com', '0351 27 49 85', 'Hello,\r\nOur company makes handmade Large world globes that can be customized for your brand, company or interior design https://bit.ly/www-largeglobes-com\r\nPlease let me know if you would be interested in a custom large world globe and we can send more information.\r\n\r\nThank you.\r\nBest regards,\r\nRemus Gall\r\nGlobemaker at www.largeglobes.com\r\nProject manager at Biodomes www.biodomes.eu\r\n+40 721 448 830\r\nSkype ID office@biodomes.eu\r\nStr. Vonhaz nr 2/a Carei, Romania\r\n\r\n-----------------------------', 'write-to-us', NULL, NULL, '2020-05-13 14:37:27', '2020-05-13 14:37:27'),
(88, 'Melisa Mickle', 'mickle.melisa@msn.com', '06-71341157', 'No cost promotion for your website here: http://www.submityourfreeads.xyz', 'write-to-us', NULL, NULL, '2020-05-14 18:23:17', '2020-05-14 18:23:17'),
(89, 'Marcella Balfour', 'franck.tamdhu@gmail.com', '(07) 4577 4407', 'The analysis of the critical situation in the world may help Your business. We don\'t give advice on how to run it. We highlight key points from the flow of conflicting information. We call this situation: \"Big Brother operation\". Fact:  pandemics; Agenda:  control over the human population; Aim:  reduction of the population; Who:  a group of vested interests. Means: genetic engineering of viruses and vaccines; production of nanobots; mass-media communication satellites; big data; A.I.; global wi', 'write-to-us', NULL, NULL, '2020-05-16 18:28:21', '2020-05-16 18:28:21'),
(90, 'Tamie Tregurtha', 'info@jsepropertymanagement.co.uk', '53-45-47-73', 'Morning\r\n\r\nBuy medical disposable face mask to protect your loved ones from the deadly CoronaVirus.  The price is $0.99 each.  If interested, please visit our site: pharmacyusa.online\r\n\r\nMany Thanks,\r\n\r\nJSE Property Management - Contact Us - jsepropertymanagement.co.uk', 'write-to-us', NULL, NULL, '2020-05-17 14:12:26', '2020-05-17 14:12:26'),
(91, 'Raul Cavenagh', 'cavenagh.raul@outlook.com', '08062 61 13 47', 'Interested in advertising that charges less than $49 per month and sends thousands of people who are ready to buy directly to your website? Have a look at: http://www.trafficmasters.xyz', 'write-to-us', NULL, NULL, '2020-05-20 02:06:29', '2020-05-20 02:06:29'),
(93, 'Geneva Stewart', 'geneva.stewart@gmail.com', '937-375-6979', 'Hi, Sarah from Blessed CBD referred me here, can you please tell me more?', 'write-to-us', NULL, NULL, '2020-05-25 23:20:11', '2020-05-25 23:20:11'),
(94, 'Donny Zambrano', 'zambrano.donny@gmail.com', '79 442 92 55', 'Greetings, I was just taking a look at your site and submitted this message via your feedback form. The contact page on your site sends you these messages to your email account which is why you are reading through my message at this moment correct? That\'s half the battle with any kind of advertising, getting people to actually READ your advertisement and this is exactly what you\'re doing now! If you have an ad message you would like to blast out to millions of websites via their contact forms in', 'write-to-us', NULL, NULL, '2020-05-29 13:39:49', '2020-05-29 13:39:49'),
(95, 'Roger Reber', 'reber.roger@gmail.com', '03.58.91.50.13', 'TRIFECTA! A novel that starches your emotional – erotic itch!\r\nAgainst a background of big business, deceit, frustration, oppression drives a wide range of emotions as three generations of women from the same family, turn to the same man for emotional support and physical gratification!\r\nA wife deceives her husband while searching for her true sexuality!\r\nWhat motivates the wife’s mother and son-in-law to enter into a relationship?\r\nThe wife’s collage age daughter, with tender guidance from her ', 'write-to-us', NULL, NULL, '2020-05-30 09:07:09', '2020-05-30 09:07:09'),
(96, 'Lester Huey', 'lester.huey@gmail.com', '(07) 5376 7538', 'Hi there!\r\n\r\nAlex here, Managing Director at ensemble media - we’re a specialist SEO and Wordpress agency based in Brisbane, Australia.\r\nCan We help You ?\r\nIn these unprecedented times, working from home is becoming the norm and with many key staff members now on the Government’s Furlough Scheme plus the pressure of strained supply chains, trying to maintain a fully functioning and operational business is extremely difficult. We might not be able to solve all problems we all face but we can help', 'write-to-us', NULL, NULL, '2020-06-02 16:34:12', '2020-06-02 16:34:12'),
(97, 'Klaus Callinan', 'klaus.callinan@msn.com', '04.68.77.63.05', 'You Can DOUBLE Your Productivity For Life In Under 48 Hours\r\n\r\nAnd when it comes to changing your life, there\'s nothing more important to fixing your productivity.\r\n\r\nThink about it.\r\n\r\nIf you\'re twice as productive, then, as far as your environment supports it, you\'re going to make at least twice as much. However, the growth is almost always exponential. So expect even more income, free time, and the ability to decide what you want to do at any given moment.\r\n\r\nHere\'s the best course I\'ve seen ', 'write-to-us', NULL, NULL, '2020-06-05 23:08:49', '2020-06-05 23:08:49'),
(99, 'Lourdes Smorgon', 'smorgon.lourdes15@gmail.com', '0375 3004917', 'Greetings, I was just taking a look at your website and submitted this message via your contact form. The feedback page on your site sends you messages like this to your email account which is the reason you\'re reading my message right now correct? This is half the battle with any type of advertising, making people actually READ your message and I did that just now with you! If you have an ad message you would like to blast out to lots of websites via their contact forms in the US or to any coun', 'write-to-us', NULL, NULL, '2020-06-08 01:07:46', '2020-06-08 01:07:46'),
(100, 'Ruthie Robinson', 'robinson.ruthie77@gmail.com', '(47) 7141-5782', 'Tired of paying for clicks and getting lousy results? Now you can post your ad on 1000s of advertising sites and it\'ll cost you less than $40. Get unlimited traffic forever! \r\n\r\nGet more info by visiting: https://bit.ly/rapid-ads', 'write-to-us', NULL, NULL, '2020-06-09 02:07:33', '2020-06-09 02:07:33'),
(101, 'Les Carmichael', 'mark@iblueskyphotography.com', '077 3127 9547', 'Hi\r\n\r\nHope you don\'t mind me taking a look but your company needs a higher\r\nprofile of social media videos\r\n\r\nAll companies should be utilising social media we can OFFER you this\r\nLIMITED SALE MAKING YOU FREE promotional social media videos providing\r\nyou pay us a small weekly fee to TOP RANK you with YouTube, Google videos\r\nand Google search.\r\n\r\nFor more information please feel free to contact me my details are below\r\n\r\nBest Regards\r\nMark Henry Rood\r\niBlueSky Photography\r\nWhatsApp +44 748 322 5', 'write-to-us', NULL, NULL, '2020-06-09 16:44:57', '2020-06-09 16:44:57'),
(102, 'Jacquelyn Kerr', 'jacquelyn.kerr@outlook.com', '079 3723 9960', 'Hello,\r\n\r\n \r\n\r\nI have just browsed through your site and would like to congratulate you on the clarity of your products and the services offered. I am sending you this message via the contact form of your website to offer you the use of our personalized communication products.\r\n\r\nWe can personalize hundreds of products with your logo in order to communicate with your customers as well as potential future clients.\r\n\r\n \r\n\r\nFlags : https://bit.ly/37-flags\r\n\r\nStands : https://bit.ly/53-stand\r\n\r\nPOS ', 'write-to-us', NULL, NULL, '2020-06-11 21:39:10', '2020-06-11 21:39:10'),
(103, 'Juliet Mccord', 'mccord.juliet27@gmail.com', '031 469 12 82', 'Are You interested in an advertising service that costs less than $50 per month and delivers thousands of people who are ready to buy directly to your website? Check out: https://bit.ly/traffic-for-you', 'write-to-us', NULL, NULL, '2020-06-12 01:31:40', '2020-06-12 01:31:40'),
(104, 'Leif Witmer', 'witmer.leif81@googlemail.com', '(07) 4526 3854', 'Interested in the latest fitness , wellness, nutrition trends?\r\n\r\nCheck out my blog here: https://bit.ly/www-fitnessismystatussymbol-com\r\n\r\nAnd my Instagram page @ziptofitness', 'write-to-us', NULL, NULL, '2020-06-12 09:15:22', '2020-06-12 09:15:22'),
(105, 'Lauren Gale', 'lauren@quickfundingnow.xyz', '545-236-1566', 'IMPORTANT UPDATE\r\n\r\nMany Americans now qualify for emergency loans up to $5,000.\r\n\r\nTo find out if you qualify, visit: \r\nhttps://quickfundingnow.xyz/r1/?=jsepropertymanagement.co.uk\r\n\r\nThese loans are unrelated to any stimulus or unemployment compensation you may already be receiving from the government.\r\n\r\nDue to the COVID-19 outbreak, lenders are expecting a surge in applications. We recommend submitting your loan request as promptly as possible.\r\n\r\nRegards,\r\nLauren Gale\r\nNational Loan Advisor', 'get-in-touch', NULL, NULL, '2020-06-13 03:07:08', '2020-06-13 03:07:08'),
(106, 'Tanya Bullins', 'tanya.bullins@yahoo.com', '410-595-1517', 'Dear Business Owner / manager\r\n\r\nWith lockdown easing over the coming weeks, many sectors are re-evaluating what the workplace of the future may look like. Some have cultivated productive remote working environments during the Coronavirus (Covid-19) pandemic, while others may choose to revert to previous working practices.\r\n\r\nCloud 4 Sure offers many solutions for contemporary IT problems: \r\n\r\n•	Hosting services\r\n•	Cloud solutions\r\n•	Cyber security systems\r\n•	Secure data backup and storage\r\n•	Mi', 'write-to-us', NULL, NULL, '2020-06-16 01:34:13', '2020-06-16 01:34:13'),
(107, 'Edwina Worth', 'worth.edwina@outlook.com', '(08) 8276 5183', 'ABOLISH REACTIVE DEPRESSION AND EMERGE FROM ITS DEEP, DARK, BLACK HOLE?\r\n• Do you feel this came from the beginnings of a dysfunctional family system?\r\n• Or did this come from the loss of a beloved job or loved one?\r\n• Or did this come from dire effects from the disease of Alcoholism?\r\n• Or did this come from the brainwashing attempts of a fearful and angry world, i.e. terroristic recruitment?\r\nDo you know that whatever caused this DEEP, DARK, BLACK HOLE OF DEPRESSION which may have come from a ', 'write-to-us', NULL, NULL, '2020-06-17 15:45:25', '2020-06-17 15:45:25'),
(108, 'Verona Tipton', 'tipton.verona@yahoo.com', '02.54.17.48.93', 'FREE IT, Broadband & Phone services for up to 6 Months for all businesses who switch to us to help with COVID-19 support.\r\n\r\nThere are no catches, increase in prices or hidden T&C’s the way it works is we ask for a copy of your current bills/expenditure. We match the pricing and with the profits made over the term of the deal we pass this saving back to you in free services.\r\n\r\nAfter we have developed a good working relationship and you trust us to provide an exceptional service we believe you w', 'write-to-us', NULL, NULL, '2020-06-18 07:30:03', '2020-06-18 07:30:03'),
(109, 'Jamey Dewitt', 'dewitt.jamey@msn.com', '(02) 9132 6672', 'CHOLLEY SKINCARE SWISS MADE SINCE 1989 used by top-class beauty salons and exclusively available online!\r\n\r\nCHOLLEY PHYTOBIOTECH SYSTEM \r\nA breakthrough in Anti-Aging cosmetics\r\nbased on New Advanced Swiss Technology.\r\n\r\n Restores health, strength, and splendor of the skin \r\nusing the finest Swiss phytobiological ingredients.\r\nSWISS SUPER PREMIUM ANTI-GLYCATION PRODUCTS\r\n\r\nSwiss Anti Glycation System\r\nYour Best Anti-Aging Defense\r\nGlycation is a degenerative process caused by attachment of sugar', 'write-to-us', NULL, NULL, '2020-06-19 03:17:20', '2020-06-19 03:17:20'),
(110, 'Sherrill Benjafield', 'sherrill.benjafield@yahoo.com', '06-50509409', 'EROTICA becomes REALITY!!!\r\nStepping Stones to the ARCH De Pleasure\r\n     Men, put your feet in James Pope’s shoes, women put your feet in the women’s shoes I encounter, as I traveled the road from farm-to-MATTRESONAME!\r\nStepping Stones is my third novel in a 3, connected, series.  1- Post Hole Digger! 2-Trifecta! \r\nhttps://bit.ly/www-popejim-com “CLICK & VIEW videos.  Search: amazon.com, title - author', 'write-to-us', NULL, NULL, '2020-06-19 15:31:09', '2020-06-19 15:31:09'),
(111, 'Claudia Clement', 'claudiauclement@yahoo.com', '0161 9496 0002', 'Hi, We are wondering if you would be interested in our service, where we can provide you with a dofollow link from Amazon (DA 96) back to jsepropertymanagement.co.uk?\r\n\r\nThe price is just $87 per link, via Paypal.\r\n\r\nTo explain what DA is and the benefit for your website, along with a sample of an existing link, please read here: https://pastelink.net/1nm60\r\n\r\nIf you\'d be interested in learning more, reply to this email but please make sure you include the word INTERESTED in the subject line fie', 'write-to-us', NULL, NULL, '2020-06-20 21:28:18', '2020-06-20 21:28:18'),
(112, 'Doug Orsini', 'orsini.doug91@gmail.com', '0486 49 75 91', 'RE: Boris Hair Day??\r\nDo you feel confident and safe enough to go back and see your hairdresser, barber or beauty Salon??\r\nMany Hair & Beauty Professionals have gone a long way to making sure it\'s safe for you and them to carry out appointments.\r\nLots has changed to keep us all safe. \r\nLook out for the SaferSalon Logo on their website and in their salon window to be sure they have had the right UK Government Guidelines Training, COVID Secure Policy, COVID Secure Risk Assessment and Pre-Appointme', 'write-to-us', NULL, NULL, '2020-06-22 18:21:03', '2020-06-22 18:21:03'),
(113, 'Neva Loftis', 'loftis.neva@gmail.com', '(27) 9824-6554', 'No charge advertising, submit your site now and start getting new visitors. Visit: https://bit.ly/zero-cost-ads', 'write-to-us', NULL, NULL, '2020-06-24 03:06:59', '2020-06-24 03:06:59'),
(114, 'martyn gosling', 'martin@overnightimages.com', '07767624985', 'Hi James\r\n\r\nPlease can you tell me if you recieve this form in your email box etc', 'get-in-touch', NULL, NULL, '2020-06-29 14:34:52', '2020-06-29 14:34:52'),
(115, 'Eric Jones', 'eric@talkwithwebvisitor.com', '416-385-3200', 'Cool website!\r\n\r\nMy name’s Eric, and I just found your site - jsepropertymanagement.co.uk - while surfing the net. You showed up at the top of the search results, so I checked you out. Looks like what you’re doing is pretty cool.\r\n \r\nBut if you don’t mind me asking – after someone like me stumbles across jsepropertymanagement.co.uk, what usually happens?\r\n\r\nIs your site generating leads for your business? \r\n \r\nI’m guessing some, but I also bet you’d like more… studies show that 7 out 10 who land', 'write-to-us', NULL, NULL, '2020-06-30 15:32:25', '2020-06-30 15:32:25'),
(116, 'Cecilia Root', 'root.cecilia@outlook.com', '070 3021 4400', 'Stem cell therapy has proven itself to be one of the most effective treatments for Parkinson\'s Disease. IMC is the leader in stem cell therapies in Mexico. For more information on how we can treat Parkinson\'s Disease please visit:\r\nhttps://bit.ly/parkinson-integramedicalcenter', 'write-to-us', NULL, NULL, '2020-07-01 01:11:19', '2020-07-01 01:11:19'),
(117, 'anstype', 'e@gmail.com', '8462356979', NULL, 'sign-up', 'GHUGUBGUBEUGBERUBGIUE', '5', '2020-07-01 08:53:24', '2020-07-01 08:53:24'),
(118, 'anstype', 'e@gmail.com', '173527726289', 'djbcjbwhbdwhvbswcbbcjbjqb', 'write-to-us', NULL, NULL, '2020-07-01 08:56:06', '2020-07-01 08:56:06'),
(119, 'anstype', 'e@gmail.com', '83648846868', NULL, 'sign-up', 'scjghjvhwcvhjwdvchjwdbchjbdvwhj', '5', '2020-07-01 08:58:20', '2020-07-01 08:58:20');

-- --------------------------------------------------------

--
-- Table structure for table `imagethumb`
--

CREATE TABLE `imagethumb` (
  `id` int(10) UNSIGNED NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `default_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `imagethumb`
--

INSERT INTO `imagethumb` (`id`, `height`, `width`, `default_image`, `active`) VALUES
(1, 149, 149, 'images/default_user_image.png', 1),
(2, 39, 39, 'images/default_user_image.png', 1),
(3, 100, 150, 'images/no_image_available.jpg', 1),
(4, 50, 50, 'images/no_image_available.jpg', 1),
(5, 150, 150, 'images/no_image_available.jpg', 1),
(6, 50, 50, 'images/default_user_image.png', 1),
(7, 200, 250, 'images/default_user_image.png', 1),
(8, 757, 1242, 'images/business_cover_default.png', 1),
(9, 1500, 1500, 'images/business_cover_default.png', 1),
(10, 50, 50, 'images/business_logo_default.jpg', 1),
(11, 150, 150, 'images/business_logo_default.jpg', 1),
(12, 757, 1242, 'images/event_cover_default.png', 1),
(13, 1500, 1500, 'images/event_cover_default.png', 1),
(14, 757, 1242, 'images/no_image_available.jpg', 1),
(15, 1500, 1500, 'images/no_image_available.jpg', 1),
(16, 252, 414, 'images/no_image_available.jpg', 1),
(17, 757, 1242, 'images/no_image_available.jpg', 1),
(18, 1500, 1500, 'images/no_image_available.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `property_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'property',
  `user_id` int(10) UNSIGNED NOT NULL,
  `reference` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `status` enum('pending','paid') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `duedate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `property_id`, `type`, `user_id`, `reference`, `value`, `status`, `duedate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'property', 8, 'Test', 35, 'paid', '2018-03-23 12:48:59', '2017-11-16 07:47:53', '2017-11-16 09:33:57', '2018-03-22 23:00:00'),
(2, 2, 'property', 8, 'Chimney', 20, 'pending', '2018-03-23 12:49:02', '2018-01-30 15:40:38', '2018-01-30 15:40:38', '2018-03-22 23:00:00'),
(3, 1, 'property', 8, 'Payment', 10, 'paid', '2018-03-23 12:49:04', '2018-02-07 22:03:27', '2018-02-07 22:06:41', '2018-03-22 23:00:00'),
(4, 1, 'property', 8, 'Nexmo', 10, 'pending', '2018-03-23 12:49:08', '2018-02-09 11:02:57', '2018-02-09 11:02:57', '2018-03-22 23:00:00'),
(5, 8, 'property', 4, 'Payment', 0, 'pending', '2019-10-19 10:53:56', '2018-04-04 10:28:32', '2019-10-19 08:53:56', '2019-10-19 08:53:56'),
(6, 8, 'property', 4, 'Payment', 0, 'pending', '2019-10-19 10:53:56', '2018-04-04 10:28:33', '2019-10-19 08:53:56', '2019-10-19 08:53:56'),
(7, 8, 'property', 4, 'yo', 0, 'pending', '2019-10-19 10:53:56', '2018-04-04 10:30:42', '2019-10-19 08:53:56', '2019-10-19 08:53:56'),
(8, 9, 'property', 5, 'Tap', 86, 'pending', '2019-10-19 10:53:56', '2018-04-20 10:27:05', '2019-10-19 08:53:56', '2019-10-19 08:53:56'),
(9, 9, 'property', 5, 'Tap', 86, 'pending', '2019-10-19 10:53:56', '2018-04-20 10:27:19', '2019-10-19 08:53:56', '2019-10-19 08:53:56'),
(10, 3, 'property', 8, 'test1qa1947@gmail.com', 1, 'pending', '2019-11-04 23:00:00', '2019-11-05 05:28:59', '2019-11-05 05:28:59', NULL),
(11, 3, 'property', 8, 'test1qa1947@gmail.com', 1, 'pending', '2019-11-05 23:00:00', '2019-11-05 06:16:06', '2019-11-05 06:16:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2017_04_25_060001_create_section_table', 1),
(9, '2017_04_25_060304_create_role_table', 1),
(10, '2017_04_25_060910_create_permissions', 1),
(11, '2017_04_25_061135_create_role_user', 1),
(12, '2017_05_03_132436_create_imagethumb', 1),
(13, '2017_05_08_063607_create_site_settings_table', 1),
(14, '2017_09_25_061849_create_pages_table', 1),
(15, '2017_10_02_084704_create_enquiry_table', 1),
(16, '2017_10_04_113428_create_notification_table', 1),
(17, '2017_10_05_070315_create_properties_table', 1),
(18, '2017_10_05_114703_create_invoice_table', 1),
(19, '2017_10_09_091204_create_subscriptions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `property` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `property`, `title`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 6, NULL, 'Hi', 'hj', 1, '2017-11-05 16:45:44', '2017-11-05 16:45:44'),
(2, 8, 'N1 3GZ', 'Test', 'new invoice generated on your property Flat 9, Trafalgar Point, 137 Downham Road London, N1 3GZ for Test', 0, '2017-11-16 07:47:53', '2020-07-01 09:58:35'),
(3, 8, 'SE22 0AH', 'Chimney', 'new invoice generated on your property Flat 6, 14-16, Underhill Road London, SE22 0AH for Chimney', 0, '2018-01-30 15:40:38', '2020-07-01 09:58:35'),
(4, 8, NULL, 'Mr J', 'Test', 0, '2018-01-30 15:51:00', '2020-07-01 09:58:35'),
(5, 8, NULL, 'Money owed', 'Please pay for repair', 0, '2018-02-01 11:36:28', '2020-07-01 09:58:35'),
(6, 8, 'N1 3GZ', 'Payment', 'new invoice generated on your property Flat 9, Trafalgar Point, 137 Downham Road London, N1 3GZ for Payment', 0, '2018-02-07 22:03:27', '2020-07-01 09:58:35'),
(7, 8, 'N1 3GZ', 'Nexmo', 'new invoice generated on your property Flat 9, Trafalgar Point, 137 Downham Road London, N1 3GZ for Nexmo', 0, '2018-02-09 11:02:57', '2020-07-01 09:58:35'),
(8, 10, NULL, 'Invoice now due', 'Hi, works are now completed. Please do pay invoice', 0, '2018-02-22 15:25:44', '2018-02-22 15:26:36'),
(9, 7, NULL, 'asdasdas', 'new invoice generated for asdasdas', 0, '2018-03-23 11:07:22', '2019-10-19 12:40:37'),
(10, 7, NULL, 'dsfsgg', 'new invoice generated for dsfsgg', 0, '2018-03-23 11:09:08', '2019-10-19 12:40:37'),
(11, 8, NULL, '1211', 'new invoice generated for 1211', 0, '2018-03-23 11:09:56', '2020-07-01 09:58:40'),
(12, 8, NULL, 'asdasdas', 'new invoice generated for asdasdas', 0, '2018-03-23 11:10:58', '2020-07-01 09:58:40'),
(15, 2, NULL, 'test', 'new invoice generated for test', 1, '2018-03-23 12:02:28', '2018-03-23 12:02:28'),
(16, 2, NULL, 'Sourcing and Development of', 'new invoice generated for Sourcing and Development of of £1000', 1, '2018-03-23 12:11:47', '2018-03-23 12:11:47'),
(17, 2, NULL, 'Sourcing and Development', 'new invoice generated for Sourcing and Development of £500', 1, '2018-03-23 12:15:42', '2018-03-23 12:15:42'),
(18, 4, 'N1 3GZ', 'Payment', 'new invoice generated on your property Flat 9, Trafalgar Point, 137 Downham Road London, N1 3GZ for Payment of £0', 0, '2018-04-04 10:28:32', '2019-08-07 15:37:04'),
(19, 4, 'N1 3GZ', 'Payment', 'new invoice generated on your property Flat 9, Trafalgar Point, 137 Downham Road London, N1 3GZ for Payment of £0', 0, '2018-04-04 10:28:33', '2019-08-07 15:37:04'),
(20, 4, 'N1 3GZ', 'yo', 'new invoice generated on your property Flat 9, Trafalgar Point, 137 Downham Road London, N1 3GZ for yo of £0', 0, '2018-04-04 10:30:42', '2019-08-07 15:37:04'),
(21, 5, 'N1 2BG', 'Tap', 'new invoice generated on your property Cross Street Baptist Church, 16-18, Cross Street London, N1 2BG for Tap of £86', 1, '2018-04-20 10:27:05', '2018-04-20 10:27:05'),
(22, 5, 'N1 2BG', 'Tap', 'new invoice generated on your property Cross Street Baptist Church, 16-18, Cross Street London, N1 2BG for Tap of £86', 1, '2018-04-20 10:27:19', '2018-04-20 10:27:19'),
(23, 8, 'LN5 0PN', 'test1qa1947@gmail.com', 'new invoice generated on your property 36 High Street Leadenham, Lincoln, LN5 0PN for test1qa1947@gmail.com of £1', 0, '2019-11-05 05:28:59', '2020-07-01 09:58:40'),
(24, 8, 'LN5 0PN', 'test1qa1947@gmail.com', 'new invoice generated on your property 36 High Street Leadenham, Lincoln, LN5 0PN for test1qa1947@gmail.com of £1', 0, '2019-11-05 06:16:06', '2020-07-01 09:58:40'),
(25, 9, NULL, 'Donut chart example', 'test this', 1, '2020-06-24 13:47:53', '2020-06-24 13:47:53');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 'home', 'REDEFINING THE INDUSTRY<span>A PERSONABLE APPROACH YOU CAN TRUST</span>', '<section class=\"common-section\">\r\n<div class=\"container\">\r\n<div class=\"saperator wow animatedslow fadeIn\"></div>\r\n<p>After a decade of managing properties James Elkington, founder of <b>JSE Property Management,</b> became frustrated with the quality of service from management companies and lettings and estate agents. These frustrations ranged from extremely poor communication, sub standard customer service and a general lack of care once a deal had been struck.</p>\r\n<p>Our company is passionate about providing a smooth, personable, customer centric and cost effective 360 degree property management service. From sourcing good quality applicants to managing your property, our invoicing, job tracking and payment systems are designed around customers needs and for ease of use.</p>\r\n<p>Having worked for a large branded estate agents James understands what landlords and tenants alike, want from our serivce.</p>\r\n<p>JSE Property Management also provides a property sourcing service and project management service for renovations and refurbishments, please see ‘Services’ for more information.</p>\r\n<div class=\"height-saperator\"></div>\r\n<div class=\"section-title wow\">\r\n<h1>Services</h1>\r\n</div>\r\n</div>\r\n</section>\r\n<section class=\"services-section\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\"><a href=\"management\" class=\"services-block management\">\r\n<p>MANAGEMENT</p>\r\n<figure><img src=\"frontend/images/JSE-Icon-vector.svg\" alt=\"jse logo\" class=\"wow animatedslow zoomIn\" /></figure>\r\n</a></div>\r\n<div class=\"col-sm-6\"><a href=\"development\" class=\"services-block development\">\r\n<p>DEVELOPMENT</p>\r\n<figure><img src=\"frontend/images/JSE-Icon-vector.svg\" alt=\"jse logo\" class=\"wow animatedslow zoomIn\" /></figure>\r\n</a></div>\r\n<div class=\"col-sm-6\"><a href=\"sourcing\" class=\"services-block sourcing\">\r\n<p>SOURCING</p>\r\n<figure><img src=\"frontend/images/JSE-Icon-vector.svg\" alt=\"jse logo\" class=\"wow animatedslow zoomIn\" /></figure>\r\n</a></div>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n</section>\r\n<section class=\"common-section\">\r\n<div class=\"container\">\r\n<div class=\"section-title wow\">\r\n<h1>MANAGEMENT</h1>\r\n</div>\r\n<div class=\"height-saperator\"></div>\r\n<p><b>JSE Property Management</b> aims to make your life as a landlord simpler while ensuring you do not lose money by being over charged for the day to day running of your property. We do this by charging a fixed yearly rate, only charging you what we are charged by our contractors and no fixed term length of contract.</p>\r\n<p>This is how we work:</p>\r\n<div class=\"management-block\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/consultation-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>INITIAL CONSULTATION</h4>\r\n<p>Your initial consultation can be conducted by phone or a face-to-face meeting at a time and place to suit you. We will then visit the property, make sure to understand all of your needs and keep communications thereafter as regular and detailed as you would or would not like.</p>\r\n</div>\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/management-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>PROPERTY MANAGEMENT</h4>\r\n<p>Your individual property address/addresses will be uploaded to our secure ‘Client Services’ portal along with tenant contact details and all relevant information surrounding each property. Invoices will be uploaded to each individual property so you can see what\'s been done and when.</p>\r\n</div>\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/operation-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>SMOOTH OPERATIONS</h4>\r\n<p>We will alert you via email upon completion of each job and payment is then made via our secure payment portal. We can liaise directly with the tenants, agency or landlord depending on your preference.</p>\r\n</div>\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/approach-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>TRUSTWORTHY APPROACH</h4>\r\n<p>JSE Property Management and all contractors that work on our behalf are fully insured. Note that we act as a facilitator and will not add any extra charges or margin to any works completed. This is our commitment to you.</p>\r\n</div>\r\n<div class=\"cleafix\"></div>\r\n<a class=\"theme-btn wow animatedslow fadeIn\" href=\"management\">find out more</a></div>\r\n</div>\r\n</div>\r\n</section>', '2017-10-10 17:08:49', '2020-06-30 10:08:31'),
(2, 'about', 'About', '<section class=\"common-section\">\r\n<div class=\"container\">\r\n<div class=\"saperator wow animatedslow fadeIn\"></div>\r\n<p><b>Having managed properties for over ten years we have unparalleled knowledge and expertise in property management. From studio flats to five bedroom properties you will not find a more consistent and meticulous service on the market.</b></p>\r\n<p>Over the past decade we have built close working relationships with a large number of businesses and individuals who work in partnership with us looking after the maintenance and well being of our clients properties. It is these two-way relationships that guarantee all of the services conducted on our behalf are to the highest standard and for the best possible price.</p>\r\n<p>With a redefining customer service policy we welcome all enquiries and look forward to hearing from you.</p>\r\n<div class=\"section-title wow\">\r\n<h1>MANAGEMENT</h1>\r\n</div>\r\n<div class=\"height-saperator\"></div>\r\n<p><b>JSE Property Management</b> aims to make your life as a landlord simpler whilst ensuring we manage costs as efficiently as possible. We do this by charging a fixed rate letting and management fee, not adding margin on to any works conducted on your behalf and a redefining customer service. </p>\r\n<p>This is how we work:</p>\r\n<div class=\"management-block\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/consultation-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>INITIAL CONSULTATION</h4>\r\n<p>Your initial consultation can be conducted by phone or a face-to-face meeting at a time and place to suit you. We will then visit the property, make sure to understand all of your needs and keep communications thereafter as regular and detailed as you would or would not like.</p>\r\n</div>\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/management-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>LETTINGS AND PROPERTY MANAGEMENT</h4>\r\n<p>With access to a large database of applicants and our past experience in the industry, JSE Property Management have successfully tenanted countless properties.</p>\r\n<p>All information and documents are uploaded to your personal and secure ‘Client Services’ portal. From here, you can track expenditure, view tenancy agreements and pay invoices, if needed. The portal is also mobile friendly.</p>\r\n</div>\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/operation-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>SMOOTH OPERATIONS</h4>\r\n<p>We will alert you via email for any updates regarding your property and, if needed, any actions can be accessed via links in the email alert. </p>\r\n</div>\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/approach-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>TRUSTWORTHY APPROACH</h4>\r\n<p>JSE Property Management and all third partner companies that work on our behalf are fully insured. Note that we act as a facilitator and will not add any extra charges or margin to any works completed. This is our commitment to you.</p>\r\n</div>\r\n<div class=\"cleafix\"></div>\r\n<a class=\"theme-btn wow animatedslow fadeIn\" href=\"management\">find out more</a></div>\r\n</div>\r\n</div>\r\n</section>\r\n<section class=\"signup-section more-info-section bg-parallex\">\r\n<div class=\"container\">\r\n<h2>FOR MORE INFORMATION CALL TODAY ON</h2>\r\n<h1>07747 305545</h1>\r\n</div>\r\n</section>', '2017-10-10 17:08:49', '2019-10-19 10:02:12'),
(3, 'overview', 'Services', '<section class=\"common-section\">\r\n<div class=\"container\">\r\n<div class=\"saperator wow animatedslow fadeIn\"></div>\r\n<p><b>JSE Property Management is passionate about providing a smooth, personable, customer centric and cost effective Property Management service.</b></p>\r\n<p>From sourcing the right tenants for your property, organising trusted, insured and qualified tradesmen to carry out works to tenant management while tasks are being completed. Our invoicing, job tracking and payment systems are designed to make the process effortless.</p>\r\n<p>We also provide a Property Sourcing service and Project Management services for renovations and refurbishments.</p>\r\n<div class=\"height-saperator\"></div>\r\n<div class=\"section-title wow\">\r\n<h1>WHAT WE DO</h1>\r\n</div>\r\n</div>\r\n</section>\r\n<section class=\"services-section\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\"><a href=\"management\" class=\"services-block management\">\r\n<p>MANAGEMENT</p>\r\n<figure><img src=\"frontend/images/JSE-Icon-vector.svg\" alt=\"jse logo\" class=\"wow animatedslow flipInY\" /></figure>\r\n</a></div>\r\n<div class=\"col-sm-6\"><a href=\"development\" class=\"services-block development\">\r\n<p>DEVELOPMENT</p>\r\n<figure><img src=\"frontend/images/JSE-Icon-vector.svg\" alt=\"jse logo\" class=\"wow animatedslow flipInY\" /></figure>\r\n</a></div>\r\n<div class=\"col-sm-6\"><a href=\"sourcing\" class=\"services-block sourcing\">\r\n<p>SOURCING</p>\r\n<figure><img src=\"frontend/images/JSE-Icon-vector.svg\" alt=\"jse logo\" class=\"wow animatedslow flipInY\" /></figure>\r\n</a></div>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n</section>', '2017-10-10 17:08:49', '2018-02-06 20:12:12'),
(4, 'management', 'Management', '<section class=\"common-section\">\r\n<div class=\"container\">\r\n<div class=\"saperator wow animatedslow fadeIn\"></div>\r\n<p><b>JSE Property Management aims to make your life as a landlord simpler and more profitable. We do this by working efficiently, using known and trusted partners and by placing the right tenants in your property.</b></p>\r\n<br />\r\n<div class=\"management-block\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/consultation-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>INITIAL CONSULTATION</h4>\r\n<p>Your initial consultation can be conducted by phone or a face-to-face meeting at a time and place to suit you. We will then visit the property, make sure to understand all of your needs and keep communications thereafter as regular and detailed as you would or would not like.</p>\r\n</div>\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/management-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>PROPERTY MANAGEMENT</h4>\r\n<p>Your individual property address/addresses will be uploaded to our secure ‘Client Services’ portal along with tenant contact details and all relevant information surrounding each property. Invoices will be uploaded to each individual property so you can see what\'s been done and when.</p>\r\n</div>\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/operation-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>SMOOTH OPERATIONS</h4>\r\n<p>We will alert you via text and email upon completion of each job and payment is then made via our secure payment portal. We can liaise directly with the tenants, agency or landlord depending on your preference.</p>\r\n</div>\r\n<div class=\"col-sm-6 management-box\">\r\n<figure><img src=\"frontend/images/approach-icon.jpg\" alt=\"jse management icon\" class=\"wow animatedslow flipInY\" /></figure>\r\n<h4>TRUSTWORTHY APPROACH</h4>\r\n<p>JSE Property Management and all contractors that work on our behalf are fully insured. Note that we act as a facilitator and will not add any extra charges or margin to any works completed. This is our commitment to you.</p>\r\n</div>\r\n<div class=\"cleafix\"></div>\r\n<a class=\"theme-btn wow animatedslow fadeIn\" href=\"#our-rates\">our rates</a></div>\r\n</div>\r\n</div>\r\n</section>\r\n<section class=\"common-section dark-section\">\r\n<div class=\"container\">\r\n<div class=\"section-title wow\">\r\n<h1>SERVICES</h1>\r\n</div>\r\n<div class=\"height-saperator\"></div>\r\n<p>Here is a list of the services we provide as part of our Property Management package:</p>\r\n<p>Tenant sourcing</p>\r\n<p>Rent guarantee</p>\r\n<div class=\"list\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-4 col-md-3\">\r\n<p><span>Repairs &amp; Maintenance</span></p>\r\n<p><span>Check in and Check-out Management</span></p>\r\n<p><span>Building &amp; Refurbishment Projects</span></p>\r\n</div>\r\n<div class=\"col-sm-4 col-md-6\">\r\n<p><span>Annual gas and electrical safety inspections / certificates</span></p>\r\n<p><span>Liaison with Surveyors and other Professional Advisors</span></p>\r\n<p><span>Management of Vacant Properties (and between tenancies)</span></p>\r\n<p><span>Out of hours &amp; 24x7 Emergency Service</span></p>\r\n</div>\r\n<div class=\"col-sm-4 col-md-3\">\r\n<p><span>Inventory Reports</span></p>\r\n<p><span>Management Inspections</span></p>\r\n<p><span>Cleaning Services</span></p>\r\n</div>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>', '2017-10-10 17:08:49', '2019-10-19 10:09:28'),
(5, 'development', 'Development', '<section class=\"common-section\">\r\n<div class=\"container\">\r\n<div class=\"saperator wow animatedslow fadeIn\"></div>\r\n<p><b>JSE Property Management has expertise in property development with our fees starting from £1000.</b></p>\r\n<p>JSE Property Management can be involved as little or as much as you would like from an initial consultation to full project management. Please do contact us for more information and previous projects completed.</p>\r\n</div>\r\n</section>', '2017-10-10 17:08:49', '2019-10-19 10:03:23'),
(6, 'sourcing', 'sourcing', '<section class=\"common-section\">\r\n<div class=\"container\">\r\n<div class=\"saperator wow animatedslow fadeIn\"></div>\r\n<p><b>Having been sourcing residential, buy to let and development properties for over ten years James has forged close relationships with carefully selected contacts which enables him access to off market and discounted deals that others outside of the industry simply do not have access to.</b></p>\r\n<p>Upon your initial enquiry we will organise a face to face meeting so as to understand your requirements and how best to move forward.</p>\r\n<p><b>Our fees start from £500 and will never exceed £5,000.</b> Please do contact us to discuss this further.</p>\r\n</div>\r\n</section>', '2017-10-10 17:08:49', '2017-11-05 11:28:48'),
(7, 'coverage', 'coverage', '<section class=\"common-section\">\r\n<div class=\"container\">\r\n<div class=\"saperator wow animatedslow fadeIn\"></div>\r\n<p><b>JSE Property Management currently operates in London and Lincolnshire. We will be expanding to other areas of the UK in due course so please do get in touch to discuss our plans for your area.</b></p>\r\n<div class=\"coverage-map-block\">\r\n<div class=\"coverage-image hidden-xs\">\r\n<figure><img src=\"frontend/images/coverage-map.jpg\" alt=\"jse map\" /></figure>\r\n<div class=\"coverage-location-box london\">\r\n<h3 class=\"wow animatedslow fadeIn\">london</h3>\r\n<img src=\"frontend/images/JSE-Icon-vector.svg\" alt=\"jse logo\" class=\"wow animatedslow flipInY\" /></div>\r\n<div class=\"coverage-location-box lincolnshire\">\r\n<h3 class=\"wow animatedslow fadeIn\">lincolnshire</h3>\r\n<img src=\"frontend/images/JSE-Icon-vector.svg\" alt=\"jse logo\" class=\"wow animatedslow flipInY\" /></div>\r\n</div>\r\n<div class=\"coverage-image visible-xs\">\r\n<figure><img src=\"frontend/images/coverage-map-Mobile.png\" alt=\"jse map\" /></figure>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<section class=\"signup-section more-info-section bg-parallex\">\r\n<div class=\"container\">\r\n<h2>FOR MORE INFORMATION CALL JAMES TODAY ON</h2>\r\n<h1>07747 305 545</h1>\r\n</div>\r\n</section>', '2017-10-10 17:08:49', '2017-11-05 11:27:05'),
(8, 'contact', 'contact', '<section class=\"common-section\">\r\n<div class=\"container\">\r\n<div class=\"saperator wow animatedslow fadeIn\"></div>\r\n<p><b>We love to hear from our clients. Please feel free to get in touch with us.​</b></p>\r\n<div class=\"contact-list-block\">\r\n<h2>JSE Property Management Ltd</h2>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<figure><img src=\"frontend/images/call-icon.jpg\" alt=\"jse icon\" /></figure>\r\n<p><a href=\"tel:07747 305 545\">07747 305 545</a></p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<figure><img src=\"frontend/images/location-icon.jpg\" alt=\"jse icon\" /></figure>\r\n<p>Kemp House, City Road, London, EC1V 2PD</p>\r\n</div>\r\n<div class=\"col-md-4\">\r\n<figure><img src=\"frontend/images/mail-icon.jpg\" alt=\"jse icon\" /></figure>\r\n<p><a href=\"mailto:james@jsepropertymanagement.com\">james@jsepropertymanagement.com</a></p>\r\n</div>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>', '2017-10-10 17:08:49', '2017-11-05 10:23:49');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('rima@mailinator.com', '$2y$10$InNvRLuqnb4PQkA4Yqd78OvgHQjaCa2MSkswmoHU.4ScrIuYwRGL2', '2017-10-11 18:10:02'),
('divyesh.gangani@ping2world.com', '$2y$10$n7jp8xmM6Xe1jZ37.oWf7.1Nz3C7MCk9MW3w3jVuj2ny79b05rlEW', '2017-11-03 06:30:04');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `constant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `constant`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Access', 'access', 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(2, 'Add', 'add', 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(3, 'Edit', 'edit', 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(4, 'View', 'view', 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(5, 'Delete', 'delete', 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `postcode` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `user_id`, `postcode`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 8, 'N1 3GZ', 'Flat 9, Trafalgar Point, 137 Downham Road London, N1 3GZ', '2017-11-16 07:45:10', '2017-11-16 07:45:10', NULL),
(2, 8, 'SE22 0AH', 'Flat 6, 14-16, Underhill Road London, SE22 0AH', '2017-11-16 07:45:37', '2019-10-20 12:11:43', '2019-10-20 12:11:43'),
(3, 8, 'LN5 0PN', '36 High Street Leadenham, Lincoln, LN5 0PN', '2017-11-16 07:45:52', '2017-11-16 07:45:52', NULL),
(4, 8, 'SE13 6RH', '15A, Fordyce Road London, SE13 6RH', '2017-11-16 07:46:01', '2019-10-19 12:19:57', '2019-10-19 12:19:57'),
(5, 7, 'NW10 8GE', 'Flat 17, 2 Hillside London, NW10 8GE', '2018-01-08 12:54:27', '2019-10-19 09:03:49', '2019-10-19 09:03:49'),
(6, 8, 'N1 3NG', 'Mitchison Road Surgery, 2 Mitchison Road London, N1 3NG', '2018-02-01 11:59:40', '2018-02-01 11:59:48', '2018-02-01 11:59:48'),
(7, 11, 'N1 2BG', 'Cross Street Baptist Church, 16-18, Cross Street London, N1 2BG', '2018-02-23 09:35:16', '2018-02-23 09:35:16', NULL),
(8, 4, 'N1 3GZ', 'Flat 9, Trafalgar Point, 137 Downham Road London, N1 3GZ', '2018-04-04 10:26:43', '2019-10-19 08:53:56', '2019-10-19 08:53:56'),
(9, 5, 'N1 2BG', 'Cross Street Baptist Church, 16-18, Cross Street London, N1 2BG', '2018-04-20 10:25:08', '2019-10-19 08:53:56', '2019-10-19 08:53:56');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(10) UNSIGNED NOT NULL,
  `section_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `section_id`, `title`, `route`, `params`, `image`, `sequence`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Dashboard', 'home.index', '', 'fa fa-home', 1, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(2, 2, 'General Settings', 'settings.showSetting', '', 'fa fa-cogs', 1, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(3, 3, 'CMS', 'cms.index', '', 'fa fa-file-text-o', 1, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(4, 4, 'Consumers', 'consumer.index', '', 'fa fa-user', 1, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(5, 5, 'Enquiry', 'enquiry.index', '', 'fa fa-hourglass-start', 1, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(6, 6, 'Properties    ', 'property.index', '', 'fa fa-university', 1, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(7, 7, 'Invoice', 'invoice.index', '', 'fa fa-calculator', 1, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(8, 8, 'User Invoice', 'userinvoice.index', '', 'fa fa-calculator', 1, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'access', '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(2, 1, 2, 'access,edit', '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(3, 1, 3, 'access,edit', '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(4, 1, 4, 'access,add,delete,edit', '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(5, 1, 5, 'access,delete', '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(6, 1, 6, 'access,delete', '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(7, 1, 7, 'access,add', '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(8, 4, 1, 'access', '2017-11-02 23:00:00', '2017-11-02 23:00:00'),
(9, 4, 2, 'access,edit', '2017-11-02 23:00:00', '2017-11-02 23:00:00'),
(10, 4, 3, 'access,edit', '2017-11-02 23:00:00', '2017-11-02 23:00:00'),
(11, 4, 4, 'access,add,delete,edit', '2017-11-02 23:00:00', '2017-11-02 23:00:00'),
(12, 4, 5, 'access,delete', '2017-11-02 23:00:00', '2017-11-02 23:00:00'),
(13, 4, 6, 'access,delete', '2017-11-02 23:00:00', '2017-11-02 23:00:00'),
(14, 4, 7, 'access,add', '2017-11-02 23:00:00', '2017-11-02 23:00:00'),
(15, 1, 8, 'access,add', '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(16, 4, 8, 'access,add', '2017-11-02 23:00:00', '2017-11-02 23:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `name`, `image`, `sequence`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'fa fa-home', 1, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(2, 'General Settings', 'fa fa-cogs', 2, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(3, 'CMS', 'fa fa-file-text-o', 3, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(4, 'Consumers', 'fa fa-user', 4, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(5, 'Enquiry', 'fa fa-hourglass-start', 5, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(6, 'Properties', 'fa fa-university', 6, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(7, 'Invoice', 'fa fa-calculator', 7, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(8, 'User Invoice', 'fa fa-calculator', 8, 1, '2017-10-10 17:08:49', '2017-10-10 17:08:49');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `constant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` enum('y','n') COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hint` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `editable` enum('y','n') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `label`, `type`, `constant`, `options`, `class`, `icon`, `required`, `value`, `hint`, `editable`, `created_at`, `updated_at`) VALUES
(1, 'Site Name', 'text', 'sitename', '', '', 'fa fa-font', 'y', 'JSE', 'Site name which you want to display', 'y', '2017-10-10 17:08:49', '2017-10-12 10:22:41'),
(2, 'Site Logo', 'file', 'site_logo_after', '', 'img_class', 'fa fa-upload', 'n', 'logo_after.png', 'Site logo which you want to display after login', 'n', '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(3, 'Admin email', 'text', 'admin_email', '', 'email_class', 'fa fa-envelope', 'y', 'james@jsepropertymanagement.co.uk', '', 'y', '2017-10-10 17:08:49', '2017-11-03 13:06:56'),
(4, 'Footer Text', 'text', 'footer_text', '', '', 'fa fa-font', 'y', '© {{YEAR}} JSE Property Management System.', '', 'y', '2017-10-10 17:08:49', '2017-10-24 18:15:08'),
(5, 'Author', 'text', 'site_author', '', '', 'fa fa-user', 'y', 'JC Web Design', '', 'y', '2017-10-10 17:08:49', '2017-10-16 22:29:24'),
(6, 'Latitude', 'text', 'lat', '', '', 'fa fa-map-marker', 'y', '51.508979', '', 'y', '2017-10-10 17:08:49', '2017-10-17 15:04:23'),
(7, 'Longitude', 'text', 'long', '', '', 'fa fa-map-marker', 'y', '-0.085682', '', 'y', '2017-10-10 17:08:49', '2017-10-17 15:04:23'),
(8, 'Site Logo', 'file', 'site_logo_before', '', 'img_class', 'fa fa-upload', 'n', 'logo_before.jpg', 'Site logo which you want to display before login', 'n', '2017-10-10 17:08:49', '2017-10-10 17:08:49'),
(9, 'Footer Email', 'text', 'footer_email', '', '', 'fa fa-envelope', 'y', 'james@jsepropertymanagement.co.uk', '', 'y', '2017-10-10 17:08:49', '2017-11-03 13:06:56'),
(10, 'Footer Contact', 'text', 'footer_contact', '', '', 'fa fa-mobile', 'y', '07747 305 545', '', 'y', '2017-10-10 17:08:49', '2017-11-05 11:02:34'),
(11, 'Footer Address', 'text', 'footer_address', '', '', 'fa fa-map-marker', 'y', 'Kemp House, City Road, London, EC1V 2PD', '', 'y', '2017-10-10 17:08:49', '2017-11-05 11:02:34');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `user_id`, `name`, `stripe_id`, `stripe_plan`, `quantity`, `trial_ends_at`, `ends_at`, `created_at`, `updated_at`) VALUES
(1, 7, 'main', 'sub_BlwbNUczLLuB1G', 'user-7-monthly', 1, NULL, NULL, '2017-11-15 10:05:50', '2017-11-15 10:05:50'),
(2, 8, 'main', 'sub_BlwdK6i9zOspBg', 'user-8-yearly', 1, NULL, NULL, '2017-11-15 10:07:55', '2017-11-15 10:07:55'),
(3, 10, 'main', 'sub_CN751ZC6Y11B2n', 'user-10-monthly', 1, NULL, NULL, '2018-02-22 15:23:49', '2018-02-22 15:23:49'),
(4, 11, 'main', 'sub_CNOgWxkRRlbNwj', 'user-11-monthly', 1, NULL, NULL, '2018-02-23 09:34:21', '2018-02-23 09:34:21'),
(5, 4, 'main', 'sub_CcPWQPzGJalEfN', 'user-4-monthly', 1, NULL, NULL, '2018-04-04 10:26:26', '2018-04-04 10:26:26'),
(6, 5, 'main', 'sub_CiP6QKAVDxQR2h', 'user-5-monthly', 1, NULL, NULL, '2018-04-20 10:24:24', '2018-04-20 10:24:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_type` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `consumer_type` enum('management','sourcing') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'management',
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_photo` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `monthly` int(11) NOT NULL,
  `yearly` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `active_plan` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type`, `consumer_type`, `first_name`, `last_name`, `email`, `password`, `profile_photo`, `phone_number`, `postcode`, `address`, `city`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `monthly`, `yearly`, `active`, `active_plan`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', '', 'Admin', 'admin', 'admin@jse.com', '$2y$10$zccrF5robAgMbSOeyRd56eJWszIjf7FK6QkbtD3fUS5sGYnW94fwO', '', '', '', '', '', NULL, NULL, NULL, NULL, 0, 0, 1, NULL, 'Hx0y9vNUv8LZa3h6ZQ5rtU3C23lvGvRkwKDZYLhu2si8hj1YkoXYyDFiNylj', NULL, NULL, NULL),
(2, 'user', 'sourcing', 'test', 'qa', 'test@mailinator.com', '$2y$10$VncHnA8p3lqGfUeLOvzc0.ih3kjN8nrf6uX9RBpR4bZYzDsXA6H6q', '', '919998577529', '', '', '', 'cus_CXvKqeciszCYc2', NULL, NULL, NULL, 0, 0, 1, NULL, 'wVQC7KeALXswKHkyXSqg3PZYy9LhmfKDNIHsTG4WRTHwUQCV4mDV3ncDfRPO', '2018-03-23 11:58:37', '2019-10-19 08:53:56', '2019-10-19 08:53:56'),
(3, 'user', 'management', 'Nestor', 'Fernandez', 'csbc.islington@gmail.com', '$2y$10$.GVaE5.zc22SIX/NihkGo.Ws.uuc7Nz6y4eLFDawqgvevhgowUtPW', '', '07988911486', '', '', '', 'cus_CcP2IlYERzk4LC', NULL, NULL, NULL, 0, 0, 1, NULL, 'gBpuxU4EK1omVNCWFwf70ISmHIWbsH9FiMuwwSY6Su6xXQRMi3ypFflVxICM', '2018-04-04 09:56:38', '2018-04-20 10:17:01', '2018-04-20 10:17:01'),
(4, 'user', 'management', 'James', 'Elkington', 'elky4000@gmail.com', '$2y$10$5GEpxgeTOyIB3HJaOgjdMOrsRvOCx8n4cC8xuztx91kYJ4v6wuXfe', '', '7747305545', '', '', '', 'cus_CcPUClBPgaYRb0', 'Visa', '0320', '2018-04-04 10:26:24', 0, 0, 1, 'monthly', 'Yb9yn7KrfOZIcaX0AuvhQusS5H9PO1NZycUhKFE8UJGgSGNxGJqxvMuPEA5j', '2018-04-04 10:24:56', '2019-10-19 08:53:56', '2019-10-19 08:53:56'),
(5, 'user', 'management', 'Cross Street', 'Baptist Church', 'csbc.islington@gmail.com', '$2y$10$.3nU4o4kbbw8CJ1yZJGIIOzranoFh86gZDCy2zYz47rKlXeEtzz7m', '', '07988911486', '', '', '', 'cus_CiP1nSM9CzMtQB', 'Visa', '0320', '2018-04-20 10:24:22', 0, 0, 1, 'monthly', 'PqGlYslUwRPWVq1JwQeTjTyxjo7Q6uBKtK8kLeZ3XZwiu8d3OgQnzEoVQuA6', '2018-04-20 10:19:40', '2019-10-19 08:53:56', '2019-10-19 08:53:56'),
(6, 'user', 'management', 'Phil', 'Orme', 'philorme@hotmail.com', '$2y$10$Uf6/p9oHwLyZUpoI8zBdbuHRrnWlnNq4vMVh7e5b8ms3bxf55OIPe', '', '07826875174', '', '', '', 'cus_G1ICroVlwhRhAa', NULL, NULL, NULL, 0, 1000, 1, NULL, 'sp0LF9gZDQY8zzok9fHurggDwpJgAzwGKnXSDpawowRqTaIeUr1dcE41iFMW', '2019-10-19 08:56:46', '2019-10-19 09:02:17', '2019-10-19 09:02:17'),
(7, 'user', 'management', 'Phil', 'Orme', 'philorme@hotmail.com', '$2y$10$/jTTo8/EWqpA43JZYwYyUeR2WyqAtqoisqkXFQVWINOxol.JjtVfW', '', '07826875174', '', '', '', 'cus_G1IIAlKYE0aXjy', NULL, NULL, NULL, 0, 0, 1, NULL, 'eyqhdaG2AI5otvikm9AOqcJzbOA7NQxkdFkWZwKM9O50Q32F0irYvX7gqAlA', '2019-10-19 09:02:47', '2019-11-01 14:05:17', NULL),
(8, 'user', 'management', 'James', 'Elkington', 'elky4000@gmail.com', '$2y$10$CB2REvh63A6uQgqxw54yqO7fUC3a1Q8rTJw.kZ4/lgoDzuVGz36Na', '', '07747305545', '', '', '', 'cus_G1LR1cLgjGEzil', NULL, NULL, NULL, 0, 1, 1, NULL, '0tpL3TSfwOANVVUlfEJayCIJC4AOkRxUzRvRLK9gaQB6zB5E0lAHbULqNtGl', '2019-10-19 12:17:43', '2019-11-03 11:30:33', NULL),
(9, 'user', 'management', 'martyn', 'gosling', 'martin@overnightimages.com', '$2y$10$2eKr31gGkb8V0hsIPJLf/uyiOlTvc0JMwhLLMdUBNolHIFvb9rSR.', '', '07767624985', '', '', '', 'cus_HWe61tCrIekYtq', NULL, NULL, NULL, 10, 120, 1, NULL, NULL, '2020-06-24 13:46:57', '2020-06-24 13:58:55', '2020-06-24 13:58:55');

-- --------------------------------------------------------

--
-- Table structure for table `user_invoices`
--

CREATE TABLE `user_invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'property',
  `user_id` int(10) UNSIGNED NOT NULL,
  `reference` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `status` enum('pending','paid') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `duedate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `user_invoices`
--

INSERT INTO `user_invoices` (`id`, `type`, `user_id`, `reference`, `value`, `status`, `duedate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'property', 2, 'test', 200, 'pending', '2018-03-22 23:00:00', '2018-03-23 12:02:28', '2018-03-23 12:02:28', NULL),
(2, 'property', 2, 'Sourcing and Development of', 1000, 'pending', '2018-03-29 22:00:00', '2018-03-23 12:11:47', '2018-03-23 12:11:47', NULL),
(3, 'property', 2, 'Sourcing and Development', 500, 'pending', '2019-11-05 07:13:04', '2018-03-23 12:15:42', '2018-03-23 12:15:42', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imagethumb`
--
ALTER TABLE `imagethumb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_property_id_foreign` (`property_id`),
  ADD KEY `invoices_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id_foreign` (`user_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `properties_user_id_foreign` (`user_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_section_id_foreign` (`section_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_invoices`
--
ALTER TABLE `user_invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `imagethumb`
--
ALTER TABLE `imagethumb`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_invoices`
--
ALTER TABLE `user_invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `properties`
--
ALTER TABLE `properties`
  ADD CONSTRAINT `properties_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role`
--
ALTER TABLE `role`
  ADD CONSTRAINT `role_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
