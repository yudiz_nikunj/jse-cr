$(function () {
    $('.page-sidebar-menu li').filter(function () {
        return $(this).hasClass('active');
    }).parent('ul').parent('li').addClass('active open');

    /**
     * Delete record from database
     * @param
     * @return {[type]}      [description]
     */
    $(document).on('click', '.act-delete', function(e){
        e.preventDefault();
        var action = $(this).attr('href');

        bootbox.confirm('Are you sure! you want to delete this record?', function(res){
            if(res){
                $.ajax({
                    url: action,
                    type: 'DELETE',
                    dataType: 'json',
                    beforeSend:addOverlay,
                    data: {
                        _token: $('meta[name="csrf_token"]').attr('content')
                    },
                    success:function(r){
                        showMessage(r.status,r.message);
                        if(typeof oTable !== "undefined"){
                            if (typeof oTable.draw !== "undefined")
                            {
                                oTable.draw();
                            }
                            else if (typeof oTable.fnDraw !== "undefined")
                            {
                                oTable.fnDraw();
                            }
                        }
                        if(typeof oTable2 !== "undefined"){
                            if (typeof oTable2.draw != "undefined")
                            {
                                oTable2.draw();
                            }
                            else if (typeof oTable2.fnDraw != "undefined")
                            {
                                oTable2.fnDraw();
                            }
                        }
                    },
                    complete:removeOverlay
                });
            }
        });

    });

    // Connfirm Dialog for Delete Property
    $(document).on('click', '.property-delete', function(e){
        e.preventDefault();
        var action = $(this).attr('href');

        bootbox.confirm('Are you sure! you want to delete this property?', function(res){
            if(res){
                window.location = action;
            }
        });

    });

     // Connfirm Dialog for Paying a Invoice
    $(document).on('click', '.pay-invoice', function(e){
        e.preventDefault();
        var action = $(this).attr('href');

        bootbox.confirm('Are you sure! you want to Pay this Invoice Now? <br><br> * <small>Your Default Card will be use for payment. if you want to make payment from diffrent card then make it default <a href="'+BASE_URL+'/cards">here</a> .</small>', function(res){
            if(res){
                window.location = action;
            }
        });

    });

    //send notification
    $(document).on('click', '.send-push', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        $('#userid').html('<input type="hidden" value="'+id+'" name="user_id"/>');
        $('#ajaxModal').modal('show');
    });

    //generalized status change
    $(document).on('switchChange.bootstrapSwitch','.status-switch', function(event, state) {
        var $this = $(this);
        var customAct = typeof $(this).data('getaction') != 'undefined' ? $(this).data('getaction') : '';
        var val = state ? 1 : 0;
        var url = $(this).data('url');
        var action =  customAct != '' ? customAct : 'change_status';

        $.ajax({
            url: url,
            type: 'PUT',
            dataType: 'json',
            beforeSend:addOverlay,
            data: {
                _token: $('meta[name="csrf_token"]').attr('content'),
                action:action,
                value:val
            },
            success:function(r){
                showMessage(r.status,r.message);
                if(r.status != 200){
                    $this.trigger('click');
                }
                removeOverlay();
            },
            complete:removeOverlay
        });
    });

    //account approval
    $(document).on('switchChange.bootstrapSwitch','.approval-switch', function(event, state) {
        var $this = $(this);
        var customAct = typeof $(this).data('getaction') != 'undefined' ? $(this).data('getaction') : '';
        var val = state ? 1 : 0;
        var url = $(this).data('url');
        var action =  customAct != '' ? customAct : 'change_status';

        if($this.prop('checked')){
            bootbox.confirm('Are you sure! you want to approve this account?', function(res){
                if(res){
                    $.ajax({
                        url: url,
                        type: 'PUT',
                        dataType: 'json',
                        beforeSend:addOverlay,
                        data: {
                            _token: $('meta[name="csrf_token"]').attr('content'),
                            action:action,
                            value:val
                        },
                        success:function(r){
                            showMessage(r.status,r.message);
                            if(r.status != 200){
                                $this.trigger('click');
                            }else{
                                $this.parent().closest('td').html('Approved');
                            }
                            removeOverlay();
                        },
                        complete:removeOverlay
                    });
                }else{
                    $this.trigger('click');
                }
            });
        }
    });

    //starts select all and delete records
    $(document).on('click', '.all_select', function () {
        if ($(this).hasClass('allChecked')) {
            $('#table_DT tbody input[class="small-chk"]').prop('checked', false);
        } else {
            $('#table_DT tbody input[class="small-chk"]').prop('checked', true);
        }
        $(this).toggleClass('allChecked');
    });

    $(document).on('click', '#table_DT tbody input[class=small-chk]', function () {
        var numberOfChecked = $('#table_DT tbody input[class="small-chk"]:checked').length;
        var totalCheckboxes = $('#table_DT tbody input[class="small-chk"]').length;

        if(numberOfChecked > 0){
            if(numberOfChecked == totalCheckboxes){
                $('.all_select').prop('indeterminate',false);
                $('.all_select').prop('checked', true);
                $('.all_select').addClass('allChecked');
            }else{
                if ($('.all_select').hasClass('allChecked')) {
                    $('.all_select').removeClass('allChecked');
                }
                $('.all_select').prop('indeterminate',true);
            }
        }
        else{
            $('.all_select').prop('indeterminate',false);
            $('.all_select').prop('checked', false);
        }
    });

    $(document).on("click",".delete_all_link", function (e) {
        $(".delete_all_link").attr("disabled", "disabled");
        e.preventDefault();
        var url = $(this).attr('href');
        var searchIDs =[];
        $("#table_DT tbody input[class='small-chk']:checked").each(function() {
            searchIDs.push($(this).val());
        });
        if(searchIDs.length > 0){
            var ids = searchIDs.join();
            bootbox.confirm("Are you sure you want to delete selected records?", function(result) {
                if(result)
                {
                    $.ajax({
                        url: url,
                        type: 'DELETE',
                        beforeSend: addOverlay,
                        dataType: 'json',
                        data: {
                            action:'delete_all',
                            ids:ids,
                            _token: $('meta[name="csrf_token"]').attr('content')
                        },
                        success:function(r){
                            showMessage(r.status,r.message);
                            if (typeof oTable.draw !== "undefined"){
                                oTable.draw();
                            }
                            else if (typeof oTable.fnDraw !== "undefined"){
                                oTable.fnDraw();
                            }
                            setTimeout(function(){
                              $('.all_select').prop('indeterminate',false);$('.all_select').prop('checked', false);
                                if ($('.all_select').hasClass('allChecked')) {
                                $('.all_select').removeClass('allChecked');} }, 1000);
                        },
                        complete:removeOverlay
                    });
                }
                $(".delete_all_link").removeAttr("disabled");
            });
        }else{
            bootbox.alert('please select at-least one record',function(){ $('.all_select').prop('indeterminate',false);$(".delete_all_link").removeAttr("disabled"); });
        }
    });
    //ends select all and delete records
});

function getStatusText(code)
{
    sText = "";
    if(code !== undefined)
    {
        switch(code)
        {
            case 200:{ sText = 'Success'; break;}
            case 404:{ sText = 'Error';break;}
            case 403:{ sText = 'Error';break;}
            case 500:{ sText = 'Error';break;}
            case "success": { sText = "Success"; break;}
            case "danger":{ sText = 'Error';break;}
            case "warning":{ sText = 'Error';break;}
            default:{sText = 'Error';}

        }
    }
    return sText;
}

function showMessage(sType,sText){
    sType = getStatusText(sType);
    toastr[sType.toLowerCase()](sText);
}
function addOverlay(){$('<div id="overlayDocument"><img src="'+SITE_URL+'loading.gif" /></div>').appendTo(document.body);}
function removeOverlay(){$('#overlayDocument').remove();}

