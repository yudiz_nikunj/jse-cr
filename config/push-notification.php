<?php

return array(

    'appNameIOS'     => array(
        'environment' =>'development',
        'certificate' => public_path().'/push_certificate/dev_push.pem',
        'passPhrase'  =>'password',
        'service'     =>'apns'
    ),
    'appNameAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'AAAAL_BzUfk:APA91bHSOUB82zc0Ytdd4TjOYdm_K1V5z2VduXcyY2WLD1CXutr1ZOf868tWD1ec02TrFcBdGZx15ljN_0xY8XAYJKS3naxvVdtyYrLHS7LOx99VBQsIvZThUamD_E7lSEIJgKHx3BYc',
        'service'     =>'gcm'
    )

);