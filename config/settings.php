<?php 
use Illuminate\Support\Facades\Config;

return array (
  'sitename' => 'JSE',
  'admin_email' => 'james@jsepropertymanagement.co.uk',
  'footer_text' => '© {{YEAR}} JSE Property Management System.',
  'site_author' => 'JC Web Design',
  'lat' => '51.508979',
  'long' => '-0.085682',
  'footer_email' => 'james@jsepropertymanagement.co.uk',
  'footer_contact' => '0207 101 4582',
  'footer_address' => 'Kemp House, City Road, London, EC1V 2PD',
  'site_logo_after' => 'logo_after.png',
  'site_logo_before' => 'logo_before.jpg',
);